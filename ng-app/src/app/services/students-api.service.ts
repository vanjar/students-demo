import { Injectable } from '@angular/core';
import { Http, Response,Headers, RequestOptions,URLSearchParams } from '@angular/http';
import { Observable }  from 'rxjs/Rx';
import {StudentSearchViewModel} from '../search/search.model';
import {StudentViewModel,FacultyViewModel, CourseViewModel, DepartmentViewModel} from '../student/student.model';
import * as _ from "lodash";

@Injectable()
export class StudentsApiService {
  private baseUrl : string;

  constructor(private http: Http) { 
    this.baseUrl = "http://students.ddns.net:8080/api";
  }

  getValues() : Observable<object> 
  {
     return this.http.get(this.baseUrl+"/values")
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  search(filter: string, ascending: boolean, orderBy: number, page:number) : Observable<StudentSearchViewModel>
  {
    var params = new URLSearchParams();
    if (filter) params.set('filter', filter)
    if (page) params.set('page',String(page))
    if (ascending) params.set('ascending', String(ascending))
    if (orderBy) params.set('orderBy', String(orderBy))

    return this.http.get(this.baseUrl+"/search",{ search: params })
      .map(this.extractData)
      .catch(this.handleError);
  }

  getStudent(studentId: number) : Observable<StudentViewModel>
  {
    return this.http.get(this.baseUrl+"/student/"+String(studentId))
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteStudent(studentId: number)
  {
    return this.http.delete(this.baseUrl+"/student/"+String(studentId))
                  .map(this.extractData)
                  .catch(this.handleError);
  }

  getFaculties() : Observable<FacultyViewModel[]>
  {
  return this.http.get(this.baseUrl+"/referenceData/faculties")
      .map(this.extractData)
      .catch(this.handleError);
  }

  getDepartments(facultyId: number) : Observable<DepartmentViewModel[]>
  {
    return this.http.get(this.baseUrl+"/referenceData/departments/"+String(facultyId))
        .map(this.extractData)
        .catch(this.handleError);
  }

  getCourses(facultyId: number) : Observable<CourseViewModel[]>
  {
    return this.http.get(this.baseUrl+"/referenceData/courses/"+String(facultyId))
        .map(this.extractData)
        .catch(this.handleError);
  }


  saveStudent(studentId: number, name: string, surname:string,studentNumber:string, address:string,city: string, phoneNo: string,
    facultyId: number, departmentId: number, courseIds: number[]) : Observable<StudentViewModel>
  {
      let body =new URLSearchParams();
      body.set("studentId",String(studentId));
      body.set("name",name);
      body.set("surname",surname);
      body.set("studentNumber",studentNumber);
      body.set("address",address);
      body.set("city",city);
      body.set("phoneNo",phoneNo);
      body.set("facultyId",String(facultyId));
      body.set("departmentId",String(departmentId));
      _.forEach(courseIds, c=> body.append("courseIds",c));
      return this.http.post(this.baseUrl+"/student", body)
        .map(this.extractData)
        .catch(this.handleError);
  }
    


  private extractData(res: Response) {
    let body;
    if (res.text())
      body= res.json();
    return body || { };
  }

   private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      errMsg =error.status != 0 ? error.status+"-"+error.statusText : "No status code."
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
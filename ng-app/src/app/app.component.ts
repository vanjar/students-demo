import { Component } from '@angular/core';
import { StudentsApiService} from './services/students-api.service'
import {ToasterContainerComponent, ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Students register page demo';
  values: object;
  constructor(private studentsApiService:StudentsApiService) {

  }

  ngOnInit(){
  }

  
}

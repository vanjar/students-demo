import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {ToasterModule} from 'angular2-toaster';

import { StudentsApiService } from "./services/students-api.service";
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { StudentComponent } from './student/student.component';
import { OrderByIndicatorComponent } from './search/order-by-indicator.component';
import { TableHeaderComponent } from './search/table-header.component';
import { StudentEditComponent } from './student/student-edit.component';

const appRoutes: Routes = [
  {
    path: 'search',
    component: SearchComponent,
    data: {
      title: 'Search for students'
    }
  },
  {
    path: 'student/:id',
    component: StudentComponent,
    data: {
      title: 'Student'
    }
  },
  {
    path: 'addstudent',
    component: StudentEditComponent,
    data: {
      title: 'Add student'
    }
  },
  {
    path: 'student/:id/edit',
    component: StudentEditComponent,
    data: {
      title: 'Edit student'
    }
  },
  { path: '',   redirectTo: '/search', pathMatch: 'full'},
  { path: '**',  redirectTo: '/search' }
];

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    StudentComponent,
    OrderByIndicatorComponent, 
    TableHeaderComponent, 
    StudentEditComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    ToasterModule,
    BrowserAnimationsModule
  ],
  providers: [StudentsApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

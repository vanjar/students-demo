import { AfterViewInit, Component,ViewChild,  Input } from '@angular/core';

@Component({
  selector: 'order-by-indicator',
  templateUrl: './order-by-indicator.component.html',
})
export class OrderByIndicatorComponent {
    @Input('ascending') ascending: boolean;
    @Input('visible') visible: boolean;
    constructor()
    {    }
}
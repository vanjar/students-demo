import { Component, OnInit,OnDestroy,OnChanges,Input} from '@angular/core';
import { StudentsApiService} from '../services/students-api.service'
import { StudentSearchViewModel} from './search.model';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})

export class SearchComponent implements OnInit{

  studentSearchViewModel : StudentSearchViewModel;
  filter: string;
  page: number;
  sub: Subscription;
  isBusy: boolean;
  _ascending: boolean;
  _orderBy: number;

  set orderBy(value: number) {
    if (this._orderBy == value)
      return;
    this._orderBy = value;
    this.doSearch();
  }
  get orderBy()
  {
    return this._orderBy;
  }

  set ascending(value: boolean) {
    if (this._ascending == value)
      return;
    this._ascending = value;
    this.doSearch()
  }

  get ascending()
  {
    return this._ascending;
  }

  constructor(private studentsApiService : StudentsApiService, private router: Router) { 
    this.studentSearchViewModel = new StudentSearchViewModel();
  }

  ngOnInit() {
    this.filter = "";
    this.ascending = true;
    this.orderBy = 2;
    this.page = 1; 
  }

  createNew()
  {
    this.router.navigate(['addstudent'])
  }

  setFilter()
  {
    this.page = 1;
    this.doSearch();
  }

  private doSearch()
  {
      this.isBusy = true;
      this.studentsApiService.search(this.filter, this.ascending, this.orderBy, this.page).subscribe((result)=>{
        this.studentSearchViewModel = result;
        this.isBusy = false;
    },(error)=>{console.log(error);this.isBusy = false;}); 
  }

  private setPage(page: number)
  {
    if (page >= this.studentSearchViewModel.pages)
      this.page = this.studentSearchViewModel.pages;
    else if (page <= 0)
      this.page = 1;
    else 
      this.page = page;
    this.doSearch();
  }

  private setFirstPage()
  {
    this.page = 1;
    this.doSearch();
  }

  private setLastPage()
  {
    this.setPage(this.studentSearchViewModel.pages);
  }

  private setPreviousPage()
  {
    this.setPage(this.page-1);
  }

  private setNextPage()
  {
    this.setPage(this.page+1);
  }



  

}

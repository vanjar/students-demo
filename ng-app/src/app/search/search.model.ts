export class StudentSearchStudentViewModel
{
    studentId : number;
    name : string;
    surname : string;
    studentNo : string;
    faculty : string;
    department  : string;
}

export class StudentSearchViewModel
{
    students: StudentSearchStudentViewModel[];
    total : number;
    pageSize : number;
    page: number;
    pages : number;
    constructor (){
        
    }
}           


import { AfterViewInit, Component,ViewChild, Output, Input,EventEmitter } from '@angular/core';

@Component({
  selector: 'table-header',
  templateUrl: './table-header.component.html',
})
export class TableHeaderComponent {
    @Input('caption') caption: string;
    @Input('orderid') orderId: number;
    @Input('disabled') disabled: boolean;
    @Input('order') orderBy: number;
    @Output() orderChange: EventEmitter<number> = new EventEmitter<number>();
    @Input('ascending') ascending: boolean;
    @Output() ascendingChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor()
    {    

    }

    setOrderBy(orderBy: number)
    {
        if (this.orderBy == orderBy) 
        {
            this.ascending =!this.ascending;
            this.ascendingChange.emit(this.ascending);
            return; 
        }
        this.orderBy = orderBy;
        this.orderChange.emit(this.orderBy);
    }
}
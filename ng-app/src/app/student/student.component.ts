import { Component, OnInit } from '@angular/core';
import {StudentViewModel} from './student.model';
import { StudentsApiService} from '../services/students-api.service'
import {Router, ActivatedRoute, Params  } from '@angular/router';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
})

export class StudentComponent implements OnInit {

  studentViewModel : StudentViewModel
  studentId : number;
  isBusy: boolean;

  constructor(private studentsApiService : StudentsApiService,private router: Router,private route: ActivatedRoute,
    private toasterService: ToasterService) { 
    this.studentViewModel = new StudentViewModel();
  }
  
  ngOnInit() {
      this.route.params.subscribe( params =>
        this.studentId = params["id"]
      );
      this.getStudent();
  }

  editStudent()
  {
    this.router.navigate(['student/'+this.studentId+'/edit'])
  }
  
  deleteStudent()
  {
    this.isBusy = true;
    this.studentsApiService.deleteStudent(this.studentId).subscribe((result)=>{
      this.isBusy = false;
      this.toasterService.pop("success","Student deleted.")
      this.router.navigate(['/search']); 
    },(error)=>{console.log(error);this.isBusy = false;}); 
  }
  private getStudent()
  {
    this.isBusy = true;
    this.studentsApiService.getStudent(this.studentId).subscribe((result)=>{
        this.studentViewModel = result;
        this.isBusy = false;
    },(error)=>{console.log(error);this.isBusy = false;}); 
  }



 
}

export class StudentViewModel
{
    studentId: number;
    facultyId: number;
    facultyName:string;
    departmentId: number;
    departmentName: string;
    name: string;
    surname: string;
    studentNumber: string;
    address: string;
    city: string;
    phoneNumber: string;
    courses: CourseViewModel[];
}

export class CourseViewModel
{
    constructor(public courseId: number,public name: string,public isSelected: boolean){

    };
}

export class FacultyViewModel
{
    facultyId : number;
    name : string;
}

export class DepartmentViewModel
{
    departmentId : number;
    name: string;
}
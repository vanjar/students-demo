import { Component, OnInit,OnDestroy } from '@angular/core';
import {Router, ActivatedRoute, Params  } from '@angular/router';
import { StudentsApiService} from '../services/students-api.service'
import {ToasterService} from 'angular2-toaster';
import {StudentViewModel, FacultyViewModel, DepartmentViewModel, CourseViewModel} from './student.model';
import * as _ from "lodash";

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
})
export class StudentEditComponent implements OnInit, OnDestroy {

  studentViewModel : StudentViewModel
  studentId : number;
  isBusy: boolean;
  faculties: FacultyViewModel[];
  selectedFaculty : FacultyViewModel;
  departments: DepartmentViewModel[];
  selectedDepartment: DepartmentViewModel;
  courses: CourseViewModel[];
  defaultDepartment: DepartmentViewModel;
  defaultFaculty: FacultyViewModel;
  private sub: any;

  constructor(private studentsApiService : StudentsApiService,private router: Router,private route: ActivatedRoute,
    private toasterService: ToasterService) { 
    this.studentViewModel = new StudentViewModel();
  }

  ngOnInit() {
      this.sub=this.route.params.subscribe(params =>
          this.studentId = params["id"]
      );
      
      if (this.studentId >0)
        this.getStudent();
      else
        this.getFaculties();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  cancel()
  {
     this.router.navigate(['student',this.studentId])
  }

  onSelectedFacultyChanged()
  {
    this.isBusy = true;
    this.getDepartments();
    this.getCourses();
  }
  
  saveStudent()
  {
    this.isBusy = true;
    let courseIds = _.map(_.filter(this.courses,c=> c.isSelected),c=> c.courseId);
    this.studentsApiService.saveStudent(this.studentId,this.studentViewModel.name,this.studentViewModel.surname,
      this.studentViewModel.studentNumber, this.studentViewModel.address, this.studentViewModel.city,this.studentViewModel.phoneNumber,
      this.selectedFaculty.facultyId, this.selectedDepartment.departmentId,courseIds).subscribe((result)=>{
        this.isBusy = false;
        this.toasterService.pop("success","Student saved.")
        this.router.navigate(['/student/'+result.studentId]); 
      },(error)=>{console.log(error);this.isBusy = false;}); 
  }

  private getStudent()
  {
    this.isBusy = true;
    this.studentsApiService.getStudent(this.studentId).subscribe((result)=>{
        this.studentViewModel = result;
        this.getFaculties();
    },(error)=>{console.log(error);this.isBusy = false;}); 
  }

  private getFaculties()
  {
     this.studentsApiService.getFaculties().subscribe((fresult)=>{
            this.faculties = fresult;
            this.selectedFaculty = this.studentId>0 
              ? fresult.find(f=> f.facultyId == this.studentViewModel.facultyId) 
              : this.faculties[0];
            this.onSelectedFacultyChanged();
            this.isBusy = false;
        },(error)=>{console.log(error);this.isBusy = false;});
  }

  private getDepartments()
  {
     this.studentsApiService.getDepartments(this.selectedFaculty.facultyId).subscribe((result)=>{
            this.departments = result;
            
            this.selectedDepartment = this.studentId > 0 
            ? result.find(f=> f.departmentId == this.studentViewModel.departmentId) 
            : this.departments[0];
            this.isBusy = false;
        },(error)=>{console.log(error);this.isBusy = false;});
  }

  private getCourses()
  {
     this.studentsApiService.getCourses(this.selectedFaculty.facultyId).subscribe((result)=>{
      this.courses = _.map(result, (r)=>{
         return new CourseViewModel(r.courseId, r.name,this.studentId > 0 ?_.findIndex(this.studentViewModel.courses, (c)=>c.courseId == r.courseId)>-1 : false);
      });
        this.isBusy = false;
        },(error)=>{console.log(error);this.isBusy = false;});
  }




}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StudentsApi.DataAccess.Dtos;

namespace StudentsApi.DataAccess
{
    public enum SearchOrderByField
    {
        Name = 1,
        Surname = 2,
        StudentNo = 3,
        Faculty = 4,
        Department = 5
    }

    public class DatabaseService
    {

        private readonly IConnectionStringProvider _connectionStringProvider;
        private readonly Dictionary<int, string> _searchOrderByFieldsDictionary;

        public DatabaseService(IConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
            _searchOrderByFieldsDictionary = new Dictionary<int, string>
            {
                { (int)SearchOrderByField.Name,"Name"},
                { (int)SearchOrderByField.Surname,"Surname" },
                { (int)SearchOrderByField.StudentNo,"StudentNumber" },
                { (int)SearchOrderByField.Faculty,"FacultyName" },
                { (int)SearchOrderByField.Department,"DepartmentName" },
            };
        }


        public async Task<int> CreateFacultyAsync(string name, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                var command = new SqlCommand("insert into Faculty(Name) values (@Name);select scope_identity()", connection);
                command.Parameters.Add(new SqlParameter("Name", name));
                var result = await command.ExecuteScalarAsync(ct).ConfigureAwait(false);
                return Convert.ToInt32(result);
            }
        }

        public async Task<int> CreateDepartmentAsync(string name,int facultyId, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                var command = new SqlCommand("insert into Department(FacultyId,Name) values (@FacultyId,@Name);select scope_identity()", connection);
                command.Parameters.Add(new SqlParameter("FacultyId", facultyId));
                command.Parameters.Add(new SqlParameter("Name", name));
                var result = await command.ExecuteScalarAsync(ct).ConfigureAwait(false);
                return Convert.ToInt32(result);
            }
        }

        public async Task<int> CreateCourseAsync(string name, int facultyId, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                var command = new SqlCommand("insert into Course(FacultyId,Name) values (@FacultyId,@Name);select scope_identity()", connection);
                command.Parameters.Add(new SqlParameter("FacultyId", facultyId));
                command.Parameters.Add(new SqlParameter("Name", name));
                var result = await command.ExecuteScalarAsync(ct).ConfigureAwait(false);
                return Convert.ToInt32(result);
            }
        }

        public async Task<List<FacultyDto>> GetFacultiesAsync(CancellationToken ct)
        {
            FacultyDto Create(SqlDataReader reader) => new FacultyDto((int) reader["FacultyId"], (string) reader["Name"]);
            return await GetAsync("select * from Faculty",Create, ct).ConfigureAwait(false);
        }

        public async Task<List<DepartmentDto>> GetDepartmentsAsync(CancellationToken ct)
        {
            DepartmentDto Create(SqlDataReader reader) => new DepartmentDto((int)reader["DepartmentId"], (int)reader["FacultyId"], (string)reader["Name"]);
            return await GetAsync("select * from Department", Create, ct).ConfigureAwait(false);
        }

        public async Task<List<CourseDto>> GetCoursesAsync(CancellationToken ct)
        {
            CourseDto Create(SqlDataReader reader) => new CourseDto((int)reader["CourseId"], (int)reader["FacultyId"],(string)reader["Name"]);
            return await GetAsync("select * from Course", Create, ct).ConfigureAwait(false);
        }

        public async Task<StudentDto> GetStudentByIdAsync(int studentId, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                StudentDto CreateStudent(SqlDataReader reader) => new StudentDto((int)reader["StudentId"], (int)reader["FacultyId"], (int)reader["DepartmentId"],
                    (string)reader["Name"], (string)reader["Surname"], (string)reader["StudentNumber"], (string)reader["Address"],
                    (string)reader["City"], reader.IsDBNull(8) ? string.Empty : (string)reader["PhoneNumber"]);

                StudentCourseDto CreateStudentCourse(SqlDataReader reader) => new StudentCourseDto((int)reader["StudentCourseId"],
                    (int)reader["StudentId"], (int)reader["CourseId"]);

                var student = (await GetAsync("select * from Student where StudentId = @StudentId", CreateStudent, ct, new SqlParameter("StudentId", studentId)).ConfigureAwait(false)).FirstOrDefault();
                if (student == null)
                    return null;

                var studentCourses = await GetAsync("select * from StudentCourse where StudentId = @StudentId",
                    CreateStudentCourse, ct, new SqlParameter("StudentId", studentId)).ConfigureAwait(false);
                
                if (studentCourses?.Any() ?? false)
                    student.SetCourses(studentCourses.Select(c=> c.CourseId).ToList());

                return student;
            }
        }


        public async Task<int> CreateStudentAsync(int facultyId, int departmentId, string name, string surname,
            string studentNumber, string address, string city, string phoneNumber, int[] courseIds, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);

                var transaction = connection.BeginTransaction();
                try
                {
                   var command = new SqlCommand(
                        "insert into Student(FacultyId, DepartmentId, Name, Surname, StudentNumber, Address, City, PhoneNumber) " +
                        "values (@FacultyId, @DepartmentId, @Name, @Surname, @StudentNumber, @Address, @City, @PhoneNumber);select scope_identity()",
                        connection) {Transaction = transaction};
                    command.Parameters.Add(new SqlParameter("FacultyId", facultyId));
                    command.Parameters.Add(new SqlParameter("DepartmentId", departmentId));
                    command.Parameters.Add(new SqlParameter("Name", name));
                    command.Parameters.Add(new SqlParameter("Surname", surname));
                    command.Parameters.Add(new SqlParameter("StudentNumber", studentNumber));
                    command.Parameters.Add(new SqlParameter("Address", address));
                    command.Parameters.Add(new SqlParameter("City", city));
                    command.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber ?? string.Empty));
                    var studentId = Convert.ToInt32(await command.ExecuteScalarAsync(ct).ConfigureAwait(false));
                    await CreateStudentCoursesAsync(studentId, courseIds, connection, transaction, ct).ConfigureAwait(false);
                    transaction.Commit();
                    return studentId;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }


        public async Task UpdateStudentAsync(int studentId, int facultyId, int departmentId, string name,
            string surname,
            string studentNumber, string address, string city, string phoneNumber, int[] courseIds,
            CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);

                var transaction = connection.BeginTransaction();
                try
                {
                    var command = new SqlCommand(
                            "update Student set FacultyId = @FacultyId, DepartmentId = @DepartmentId, Name = @Name, Surname = @Surname, StudentNumber = @StudentNumber, " +
                            "Address = @Address, City = @City, PhoneNumber = @PhoneNumber " +
                            "where StudentId = @StudentId",
                            connection)
                        { Transaction = transaction };
                    command.Parameters.Add(new SqlParameter("StudentId", studentId));
                    command.Parameters.Add(new SqlParameter("FacultyId", facultyId));
                    command.Parameters.Add(new SqlParameter("DepartmentId", departmentId));
                    command.Parameters.Add(new SqlParameter("Name", name));
                    command.Parameters.Add(new SqlParameter("Surname", surname));
                    command.Parameters.Add(new SqlParameter("StudentNumber", studentNumber));
                    command.Parameters.Add(new SqlParameter("Address", address));
                    command.Parameters.Add(new SqlParameter("City", city));
                    command.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber??string.Empty));
                 
                    await command.ExecuteNonQueryAsync(ct).ConfigureAwait(false);

                    await CreateStudentCoursesAsync(studentId, courseIds, connection, transaction, ct)
                        .ConfigureAwait(false);

                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
        
        public async Task DeleteStudentAsync(int studentId, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                var deleteStudentCommand = new SqlCommand("delete from Student where StudentId = @StudentId",connection);
                deleteStudentCommand.Parameters.Add(new SqlParameter("StudentId", studentId));
                await deleteStudentCommand.ExecuteScalarAsync(ct).ConfigureAwait(false);
            }
        }


        public async Task<StudentSearchDto> SearchStudentsAsync(string filter,int pageSize, int pageNumber,SearchOrderByField orderBy,bool ascending, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                
                var searchSql = "with Students as (select distinct s.*, f.Name as FacultyName, d.Name as DepartmentName from Student s " +
                                  "join Faculty f on f.FacultyId = s.FacultyId " +
                                  "join Department d on d.DepartmentId = s.DepartmentId " +
                                  "join StudentCourse sc on sc.StudentId = s.StudentId " +
                                  "join Course c on c.CourseId = sc.CourseId " +
                                  "where c.Name like @Filter or f.Name like @Filter or d.Name like @Filter or s.Name like @Filter or s.Surname like @Filter or s.StudentNumber like @Filter), " +
                                  "StudentsCount as (select count(*) as TotalRows from Students) " +
                                  $"select * from Students,StudentsCount order by Students.{_searchOrderByFieldsDictionary[(int)orderBy]} {(ascending ? "asc" : "desc")}" +
                                  " offset(@PageNumber - 1) * @PageSize rows fetch next @PageSize rows only";


                StudentDto CreateStudent(SqlDataReader reader)
                {

                    var student = new StudentDto((int) reader["StudentId"], (int) reader["FacultyId"],
                        (int) reader["DepartmentId"],
                        (string) reader["Name"], (string) reader["Surname"], (string) reader["StudentNumber"],
                        (string) reader["Address"],
                        (string) reader["City"],reader.IsDBNull(8) ? string.Empty : (string) reader["PhoneNumber"]);
                    student.SetTotalRows((int)reader["TotalRows"]);
                    return student;
                }

                List<StudentDto> students = await GetAsync(connection,searchSql,CreateStudent,ct, new SqlParameter("PageNumber", pageNumber), new SqlParameter("PageSize", pageSize),
                    new SqlParameter("Filter", $"%{filter}%"));
                
                if (!students.Any())
                    return new StudentSearchDto(0,pageSize,pageNumber,0,new List<StudentDto>());

                var totalRows = students.First().TotalRows;
                return new StudentSearchDto(totalRows, pageSize, pageNumber,totalRows/pageSize + (totalRows%pageSize != 0 ? 1 : 0),students);
            }
        }


        public async Task ExecuteScriptAsync(string sql, CancellationToken ct)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                var command = new SqlCommand(sql,connection);
                await command.ExecuteNonQueryAsync(ct).ConfigureAwait(false);
            }
        }


        private async Task CreateStudentCoursesAsync(int studentId, int[] courseIds, SqlConnection connection, SqlTransaction transaction, CancellationToken ct)
        {
            var deleteStudentCourses = new SqlCommand("delete from StudentCourse where StudentId = @StudentId",connection);
            deleteStudentCourses.Parameters.Add(new SqlParameter("StudentId", studentId));
            deleteStudentCourses.Transaction = transaction;
            await deleteStudentCourses.ExecuteScalarAsync(ct).ConfigureAwait(false);

            if (!courseIds?.Any() ?? false)
                return;

            foreach (var courseId in courseIds)
            {
                var addStudentCourseCommand = new SqlCommand("insert into StudentCourse(StudentId, CourseId) values (@StudentId,@CourseId);" +
                                                             "select scope_identity()", connection)
                    { Transaction = transaction };
                addStudentCourseCommand.Parameters.Add(new SqlParameter("StudentId", studentId));
                addStudentCourseCommand.Parameters.Add(new SqlParameter("CourseId", courseId));
                await addStudentCourseCommand.ExecuteScalarAsync(ct).ConfigureAwait(false);
            }
        }


        private async Task<List<T>> GetAsync<T>(string sql, Func<SqlDataReader,T> create, CancellationToken ct, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionStringProvider.ConnectionString))
            {
                await connection.OpenAsync(ct).ConfigureAwait(false);
                return await GetAsync(connection, sql, create, ct, parameters);
            }
        }

        private async Task<List<T>> GetAsync<T>(SqlConnection connection,string sql, Func<SqlDataReader, T> create, 
            CancellationToken ct, params SqlParameter[] parameters)
        {
            var command = new SqlCommand(sql, connection);
            if (parameters?.Any() ?? false)
                foreach (var parameter in parameters)
                    command.Parameters.Add(parameter);

            var result = new List<T>();
            using (var reader = await command.ExecuteReaderAsync(ct).ConfigureAwait(false))
            {
                while (reader.Read())
                {
                    result.Add(create(reader));
                }
            }
            return result;
        }
    }
}

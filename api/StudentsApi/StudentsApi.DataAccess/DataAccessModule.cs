﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace StudentsApi.DataAccess
{
    public class DataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConnectionStringProvider>().As<IConnectionStringProvider>().SingleInstance();
            builder.RegisterType<DatabaseService>().SingleInstance();
        }
    }
}

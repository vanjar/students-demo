﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsApi.DataAccess
{
    public class ConnectionStringProvider : IConnectionStringProvider
    {
        public string ConnectionString { get; }

        public ConnectionStringProvider()
        {
            ConnectionString = "Server=.;Database=StudentsDemo;Trusted_Connection=True;";
        }
    }
}

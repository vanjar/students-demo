namespace StudentsApi.DataAccess.Dtos
{
    public class CourseDto
    {
        public int CourseId { get; }
        public int FacultyId { get; }
        public string Name { get; }

        public CourseDto(int courseId, int facultyId, string name)
        {
            CourseId = courseId;
            FacultyId = facultyId;
            Name = name;
        }
    }
}
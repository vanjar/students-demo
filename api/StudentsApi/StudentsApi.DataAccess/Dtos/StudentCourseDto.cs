﻿namespace StudentsApi.DataAccess.Dtos
{
    public class StudentCourseDto
    {
        public int StudentCourseId { get;  }
        public int StudentId { get;  }
        public int CourseId { get;}

        public StudentCourseDto(int studentCourseId, int studentId, int courseId)
        {
            StudentCourseId = studentCourseId;
            StudentId = studentId;
            CourseId = courseId;
        }
    }
}

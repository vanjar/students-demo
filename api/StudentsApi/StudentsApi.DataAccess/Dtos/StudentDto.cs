﻿using System.Collections.Generic;

namespace StudentsApi.DataAccess.Dtos
{
    public class StudentDto
    {
        public int StudentId { get;  }

        public int FacultyId { get;  }

        public int DepartmentId { get; }

        public string Name { get; }

        public string Surname { get; }

        public string StudentNumber { get;  }

        public string Address { get;  }
        
        public string City { get;  }

        public string PhoneNumber { get;  }

        public List<int> CourseIds { get; private set; }

        public int TotalRows { get; private set; }

        public StudentDto(int studentId, int facultyId, int departmentId, string name, string surname, string studentNumber,
            string address, string city, string phoneNumber)
        {
            StudentId = studentId;
            FacultyId = facultyId;
            DepartmentId = departmentId;
            Name = name;
            Surname = surname;
            StudentNumber = studentNumber;
            Address = address;
            City = city;
            PhoneNumber = phoneNumber;
            CourseIds = new List<int>();
        }

        public void SetCourses(List<int> courseIds)
        {
            CourseIds = courseIds;
        }

        public void SetTotalRows(int totalRows)
        {
            TotalRows = totalRows;
        }
    }
}

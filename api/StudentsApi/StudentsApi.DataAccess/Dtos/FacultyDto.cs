﻿namespace StudentsApi.DataAccess.Dtos
{
    public class FacultyDto
    {
        public int FacultyId { get; }
        public string Name { get; }

        public FacultyDto(int facultyId, string name)
        {
            FacultyId = facultyId;
            Name = name;
        }
    }
}
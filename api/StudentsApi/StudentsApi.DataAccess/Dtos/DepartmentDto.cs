namespace StudentsApi.DataAccess.Dtos
{
    public class DepartmentDto
    {
        public int DepartmentId { get;  }
        public int FacultyId { get; }
        public string Name { get; }

        public DepartmentDto(int departmentId, int facultyId, string name)
        {
            DepartmentId = departmentId;
            FacultyId = facultyId;
            Name = name;
        }
    }
}
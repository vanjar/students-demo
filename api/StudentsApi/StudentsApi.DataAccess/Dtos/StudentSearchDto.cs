﻿using System.Collections.Generic;

namespace StudentsApi.DataAccess.Dtos
{
    public class StudentSearchDto
    {
        public int Total { get;  }
        public int PageSize { get;  }
        public int Page { get;  }
        public int Pages { get;  }
        public List<StudentDto> Students { get;  }

        public StudentSearchDto(int total, int pageSize, int page, int pages, List<StudentDto> students)
        {
            Total = total;
            PageSize = pageSize;
            Page = page;
            Pages = pages;
            Students = students;
        }
    }
}
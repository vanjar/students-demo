﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentsApi.DataAccess;
using StudentsApi.Infrastructure;
using StudentsApi.Infrastructure.Models;

namespace StudentsApi.Controllers
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        private readonly ReferenceDataCache _referenceDataCache;
        private readonly DatabaseService _databaseService;

        public SearchController(ReferenceDataCache referenceDataCache, DatabaseService databaseService)
        {
            _referenceDataCache = referenceDataCache;
            _databaseService = databaseService;
        }

        [HttpGet]
        public async Task<IActionResult> Search(string filter, int page, int orderBy, bool ascending)
        {
            var pageSize = 20;
            var orderByField = (SearchOrderByField) orderBy;
            if (orderByField == 0)
                orderByField = SearchOrderByField.Surname;
            if (page == 0)
                page = 1;
            if (string.IsNullOrEmpty(filter))
                filter = string.Empty;
            
            var studentSearch = await _databaseService.SearchStudentsAsync(filter, pageSize, page, orderByField, ascending,
                CancellationToken.None).ConfigureAwait(false);

            var studentViewModels = studentSearch.Students.Select(s => new StudentSearchStudentViewModel(s.StudentId,
                s.Name, s.Surname, s.StudentNumber,
                _referenceDataCache.GetFaculty(s.FacultyId).Name,
                _referenceDataCache.GetDepartment(s.DepartmentId).Name));

            var result =  new StudentSearchViewModel(studentSearch.Total,studentSearch.PageSize, studentSearch.Page, studentSearch.Pages, studentViewModels.ToList());

            return new JsonResult(result);
        }
    }
}

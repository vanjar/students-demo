﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentsApi.Infrastructure;
using StudentsApi.Infrastructure.Models;

namespace StudentsApi.Controllers
{
    [Route("api/[controller]")]
    public class ReferenceDataController : Controller
    {
        private readonly ReferenceDataCache _referenceDataCache;

        public ReferenceDataController(ReferenceDataCache referenceDataCache)
        {
            _referenceDataCache = referenceDataCache;
        }

        [HttpGet]
        [Route("faculties")]
        public IActionResult GetFaculties()
        {
            var faculties =  _referenceDataCache.GetFaculties().Select(f => new FacultyViewModel(f.FacultyId, f.Name)).OrderBy(f=> f.Name);
            return new JsonResult(faculties);
        }

        [HttpGet]
        [Route("departments/{facultyId}")]
        public IActionResult GetDepartments(int facultyId)
        {
            var departments = _referenceDataCache.GetDepartmentsByFacultyId(facultyId);
            if (departments == null)
                return new StatusCodeResult((int)HttpStatusCode.NotFound);
            return new JsonResult(departments.Select(d=> new DepartmentViewModel(d.DepartmentId, d.Name)).OrderBy(d=> d.DepartmentId));
        }


        [HttpGet]
        [Route("courses/{facultyId}")]
        public IActionResult GetCourses(int facultyId)
        {
            var courses = _referenceDataCache.GetCoursesByFacultyId(facultyId);
            if (courses == null)
                return new StatusCodeResult((int)HttpStatusCode.NotFound);
            return new JsonResult(courses.Select(f => new CourseViewModel(f.CourseId, f.Name)).OrderBy(c=> c.Name));
        }
    }
}

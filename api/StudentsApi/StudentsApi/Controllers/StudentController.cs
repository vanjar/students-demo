﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentsApi.DataAccess;
using StudentsApi.Infrastructure;
using StudentsApi.Infrastructure.Models;

namespace StudentsApi.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private readonly DatabaseService _databaseService;
        private readonly ReferenceDataCache _referenceDataCache;

        public StudentController(DatabaseService databaseService, ReferenceDataCache referenceDataCache)
        {
            _databaseService = databaseService;
            _referenceDataCache = referenceDataCache;
        }

        [HttpGet("{studentId}")]
        public async Task<IActionResult> Get(int studentId)
        {
            var student = await _databaseService.GetStudentByIdAsync(studentId, CancellationToken.None).ConfigureAwait(false);
            if (student == null)
                return NotFound();
            
            var studentViewModel = new StudentViewModel(student.StudentId, student.FacultyId, _referenceDataCache.GetFaculty(student.FacultyId).Name,
                _referenceDataCache.GetDepartment(student.DepartmentId).Name,student.DepartmentId,student.Name, student.Surname, student.StudentNumber,
                student.Address, student.City, student.PhoneNumber, student.CourseIds.Select(c => _referenceDataCache.GetCourse(c)).Select(c=> new CourseViewModel(c.CourseId, c.Name)).ToList());

            return new JsonResult(studentViewModel);
        }

        [HttpDelete("{studentId}")]
        public async Task<IActionResult> Delete(int studentId)
        {
            var student = await _databaseService.GetStudentByIdAsync(studentId, CancellationToken.None).ConfigureAwait(false);
            if (student == null)
                return NotFound();

            await _databaseService.DeleteStudentAsync(studentId, CancellationToken.None).ConfigureAwait(false);

            return Ok();
        }


        [HttpPost]

        public async Task<IActionResult> Save(int studentId,
            string name, string surname, string address, string city, string studentNumber,
            int facultyId, string phoneNo, int departmentId, int[] courseIds)

        {
            if (studentId == 0)
                studentId = await _databaseService.CreateStudentAsync(facultyId, departmentId, name, surname,
                    studentNumber,address, city, phoneNo, courseIds, CancellationToken.None).ConfigureAwait(false);
            else
                await _databaseService.UpdateStudentAsync(studentId, facultyId, departmentId, name, surname,
                    studentNumber,address, city, phoneNo, courseIds, CancellationToken.None).ConfigureAwait(false);
            var student = await _databaseService.GetStudentByIdAsync(studentId, CancellationToken.None).ConfigureAwait(false); 
            return new JsonResult(student);
        }
    }
}

﻿using System;
using System.Threading;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StudentsApi.DataAccess;
using StudentsApi.Infrastructure;

namespace StudentsApi
{
    public class Startup
    {
        private IContainer _applicationContainer;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            _applicationContainer = CreateContainer(services);
            return new AutofacServiceProvider(_applicationContainer);
        }


        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseCors(builder =>
                builder.WithOrigins("http://192.168.100.105:4200", "http://localhost:4200", "http://students.ddns.net")
                    .AllowAnyMethod().AllowAnyHeader());
                
            app.UseMvc();
            await _applicationContainer.Resolve<ReferenceDataCache>().InitializeCacheAsync(CancellationToken.None)
                .ConfigureAwait(false);
        }


        private IContainer CreateContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<InfrastrutcureModule>();
            builder.Populate(services);
            return builder.Build();
        }

    }
}

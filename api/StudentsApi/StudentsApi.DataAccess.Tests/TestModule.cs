﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace StudentsApi.DataAccess.Tests
{
    public class TestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TestConnectionStringProvider>().As<IConnectionStringProvider>().SingleInstance();
            builder.RegisterType<DatabaseService>().SingleInstance();
        }
    }
}

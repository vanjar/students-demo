﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using Autofac;
using Autofac.Core;
using NUnit.Framework;
using Shouldly;
using StudentsApi.DataAccess.Dtos;
// ReSharper disable InconsistentNaming
namespace StudentsApi.DataAccess.Tests
{
    public class when_getting_faculties : DatabaseServiceTest
    {
        private List<FacultyDto> _faculties;

        [OneTimeSetUp]
        public void Establish()
        {
            DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Wait();
            DatabaseService.CreateFacultyAsync("FacultyTwo", CancellationToken.None).Wait();
            _faculties = DatabaseService.GetFacultiesAsync(CancellationToken.None).Result;
        }


        [Test]
        public void it_should_return_faculties() =>
            _faculties.Select(f => f.Name).ShouldContain("FacultyOne", "FacultyTwo");
    }

    public class when_getting_departments : DatabaseServiceTest
    {
        private List<DepartmentDto> _departments;

        [OneTimeSetUp]
        public void Establish()
        {
            var facultyId = DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Result;
            DatabaseService.CreateDepartmentAsync("DepartmentOne", facultyId, CancellationToken.None).Wait();
            DatabaseService.CreateDepartmentAsync("DepartmentTwo", facultyId, CancellationToken.None).Wait();
            _departments = DatabaseService.GetDepartmentsAsync(CancellationToken.None).Result;
        }

        [Test]
        public void it_should_return_departments() =>
            _departments.Select(f => f.Name).ShouldContain("DepartmentOne", "DepartmentTwo");
    }

    public class when_getting_courses : DatabaseServiceTest
    {
        private List<CourseDto> _courses;

        [OneTimeSetUp]
        public void Establish()
        {
            var facultyId = DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Result;
            DatabaseService.CreateCourseAsync("CourseOne", facultyId, CancellationToken.None).Wait();
            DatabaseService.CreateCourseAsync("CourseTwo", facultyId, CancellationToken.None).Wait();
            _courses = DatabaseService.GetCoursesAsync(CancellationToken.None).Result;
        }

        [Test]
        public void it_should_return_courses() =>
            _courses.Select(f => f.Name).ShouldContain("CourseOne", "CourseTwo");
    }


    public class when_getting_student_by_id : DatabaseServiceTest
    {
        private StudentDto _student;

        [OneTimeSetUp]
        public void Establish()
        {
            var facultyId = DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Result;
            DatabaseService.CreateDepartmentAsync("DepartmentOne", facultyId, CancellationToken.None).Wait();
            var departmentId = DatabaseService.CreateDepartmentAsync("DepartmentTwo", facultyId, CancellationToken.None)
                .Result;
            var courseIds = new[]
            {
                DatabaseService.CreateCourseAsync("CourseOne", facultyId, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseTwo", facultyId, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseThree", facultyId, CancellationToken.None).Result,
            };
            var studentId = DatabaseService.CreateStudentAsync(facultyId, departmentId, "Name", "Surname", "StudentNo",
                "Address", "City", "PhoneNo", courseIds, CancellationToken.None).Result;
            _student = DatabaseService.GetStudentByIdAsync(studentId, CancellationToken.None).Result;
        }

        [Test]
        public void it_should_return_student_with_id() =>
            _student.StudentId.ShouldBeGreaterThan(0);

        [Test]
        public void it_should_return_student_with_facultyId() =>
            _student.FacultyId.ShouldBe(1);

        [Test]
        public void it_should_return_student_with_departmentId() =>
            _student.DepartmentId.ShouldBe(2);

        [Test]
        public void it_should_return_student_with_name() =>
            _student.Name.ShouldBe("Name");

        [Test]
        public void it_should_return_student_with_surname() =>
            _student.Surname.ShouldBe("Surname");

        [Test]
        public void it_should_return_student_with_student_number() =>
            _student.StudentNumber.ShouldBe("StudentNo");

        [Test]
        public void it_should_return_student_with_address() =>
            _student.Address.ShouldBe("Address");

        [Test]
        public void it_should_return_student_with_city() =>
            _student.City.ShouldBe("City");

        [Test]
        public void it_should_return_student_with_phone_no() =>
            _student.PhoneNumber.ShouldBe("PhoneNo");

        [Test]
        public void it_should_return_student_with_courses() =>
            _student.CourseIds.ShouldBe(new[] { 1, 2, 3 });
    }

    public class when_updating_student : DatabaseServiceTest
    {
        private StudentDto _student;

        [OneTimeSetUp]
        public void Establish()
        {
            var facultyId = DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Result;
            var facultyId2 = DatabaseService.CreateFacultyAsync("FacultyTwo", CancellationToken.None).Result;

            var departmentId = DatabaseService.CreateDepartmentAsync("DepartmentTwo", facultyId, CancellationToken.None)
                .Result;
            var departmentId2 = DatabaseService
                .CreateDepartmentAsync("DepartmentTwo", facultyId2, CancellationToken.None).Result;

            var courseIds = new[]
            {
                DatabaseService.CreateCourseAsync("CourseOne", facultyId, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseTwo", facultyId, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseThree", facultyId, CancellationToken.None).Result,
            };

            var updatedCourseIds = new[]
            {
                DatabaseService.CreateCourseAsync("CourseFour", facultyId2, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseFive", facultyId2, CancellationToken.None).Result,
            };

            var studentId = DatabaseService.CreateStudentAsync(facultyId, departmentId, "Name", "Surname", "StudentNo",
                "Address", "City", "PhoneNo", courseIds, CancellationToken.None).Result;

            DatabaseService.UpdateStudentAsync(studentId, facultyId2, departmentId2, "Name2", "Surname2", "StudentNo2",
                "Address2", "City2", "PhoneNo2", updatedCourseIds, CancellationToken.None).Wait();

            _student = DatabaseService.GetStudentByIdAsync(studentId, CancellationToken.None).Result;
        }


        [Test]
        public void it_should_update_with_new_student_facultyId() =>
            _student.FacultyId.ShouldBe(2);

        [Test]
        public void it_should_update_with_new_student_departmentId() =>
            _student.DepartmentId.ShouldBe(2);

        [Test]
        public void it_should_update_with_new_student_name() =>
            _student.Name.ShouldBe("Name2");

        [Test]
        public void it_should_update_with_new_student_surname() =>
            _student.Surname.ShouldBe("Surname2");

        [Test]
        public void it_should_update_with_new_student_with_number() =>
            _student.StudentNumber.ShouldBe("StudentNo2");

        [Test]
        public void it_should_update_with_new_student_address() =>
            _student.Address.ShouldBe("Address2");

        [Test]
        public void it_should_update_with_new_student_city() =>
            _student.City.ShouldBe("City2");

        [Test]
        public void it_should_update_with_new_student_phone_no() =>
            _student.PhoneNumber.ShouldBe("PhoneNo2");

        [Test]
        public void it_should_update_with_new_student_courses() =>
            _student.CourseIds.ShouldBe(new[] { 4, 5 });
    }

    public class when_deleting_student : DatabaseServiceTest
    {

        private StudentDto _studentAfterDelete;

        [OneTimeSetUp]
        public void Establish()
        {
            var facultyId = DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Result;
            DatabaseService.CreateDepartmentAsync("DepartmentOne", facultyId, CancellationToken.None).Wait();
            var departmentId = DatabaseService.CreateDepartmentAsync("DepartmentTwo", facultyId, CancellationToken.None)
                .Result;
            var courseIds = new[]
            {
                DatabaseService.CreateCourseAsync("CourseOne", facultyId, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseTwo", facultyId, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("CourseThree", facultyId, CancellationToken.None).Result,
            };

            var studentId = DatabaseService.CreateStudentAsync(facultyId, departmentId, "Name", "Surname", "StudentNo",
                "Address", "City", "PhoneNo", courseIds, CancellationToken.None).Result;
            var student = DatabaseService.GetStudentByIdAsync(studentId, CancellationToken.None).Result;
            student.ShouldNotBeNull();
            DatabaseService.DeleteStudentAsync(studentId, CancellationToken.None).Wait();
            _studentAfterDelete = DatabaseService.GetStudentByIdAsync(studentId, CancellationToken.None).Result;
        }

        [Test]
        public void it_should_delete_student() =>
            _studentAfterDelete.ShouldBeNull();
    }


    public class when_searching_students : DatabaseServiceTest
    {
        private StudentSearchDto _studentSearch;

        [OneTimeSetUp]
        public void Establish()
        {
            var facultyId1 = DatabaseService.CreateFacultyAsync("FacultyOne", CancellationToken.None).Result;
            var facultyId2 = DatabaseService.CreateFacultyAsync("FacultyTwo", CancellationToken.None).Result;
            var departmentId1_1 = DatabaseService
                .CreateDepartmentAsync("DepartmentOne", facultyId1, CancellationToken.None).Result;
            var departmentId1_2 = DatabaseService
                .CreateDepartmentAsync("DepartmentTwo", facultyId1, CancellationToken.None).Result;
            var departmentId2_1 = DatabaseService
                .CreateDepartmentAsync("BDepartmentOne", facultyId2, CancellationToken.None).Result;
            var departmentId2_2 = DatabaseService
                .CreateDepartmentAsync("BDepartmentTwo", facultyId2, CancellationToken.None).Result;

            var courseIds = new[]
            {
                DatabaseService.CreateCourseAsync("ACourseOne", facultyId1, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("ACourseTwo", facultyId1, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("ACourseThree", facultyId1, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("BCourseOne", facultyId2, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("BCourseTwo", facultyId2, CancellationToken.None).Result,
                DatabaseService.CreateCourseAsync("BBCourseThree", facultyId2, CancellationToken.None).Result,
            };

            DatabaseService.CreateStudentAsync(facultyId1, departmentId1_1, "NameA", "SurnameA", "StudentNoA",
                "AddressA", "CityA", "PhoneNoA", new[] { 1, 2 }, CancellationToken.None).Wait();
            DatabaseService.CreateStudentAsync(facultyId1, departmentId1_2, "NameB", "SurnameB", "StudentNoB",
                "AddressB", "CityB", "PhoneNoB", new[] { 3 }, CancellationToken.None).Wait();
            DatabaseService.CreateStudentAsync(facultyId2, departmentId2_1, "NameA1", "SurnameA1", "StudentNoA1",
                "AddressA1", "CityA1", "PhoneNoA1", new[] { 4, 5 }, CancellationToken.None).Wait();
            DatabaseService.CreateStudentAsync(facultyId2, departmentId2_2, "NameB1", "SurnameB1", "StudentNoB1",
                "AddressB1", "CityB1", "PhoneNoB1", new[] { 5 }, CancellationToken.None).Wait();
            _studentSearch = DatabaseService.SearchStudentsAsync("FacultyOn", 1, 2, SearchOrderByField.Name, true, CancellationToken.None).Result;
        }

        [Test]
        public void it_should_return_total_count() =>
            _studentSearch.Total.ShouldBe(2);

        [Test]
        public void it_should_return_current_page() =>
            _studentSearch.Page.ShouldBe(2);

        [Test]
        public void it_should_return_total_pages() =>
            _studentSearch.Pages.ShouldBe(2);

        [Test]
        public void it_should_return_page_size() =>
            _studentSearch.PageSize.ShouldBe(1);

        [Test]
        public void it_should_return_students() =>
            _studentSearch.Students.Select(s => s.Name).ShouldBe(new[] { "NameB" });
    }



    public class DatabaseServiceTest : DataAccessTest
    {
        [OneTimeSetUp]
        public new void Setup()
        {
            DatabaseService = Container.Resolve<DatabaseService>();
            DatabaseService.ExecuteScriptAsync(File.ReadAllText("scripts\\DropSchema.sql"), CancellationToken.None).Wait();
            DatabaseService.ExecuteScriptAsync(File.ReadAllText("scripts\\CreateSchema.sql"), CancellationToken.None).Wait();
        }

        [OneTimeTearDown]
        public new void Dispose()
        {
            base.Dispose();
        }
    }
}


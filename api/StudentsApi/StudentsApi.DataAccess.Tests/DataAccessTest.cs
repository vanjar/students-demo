﻿using System;
using System.IO;
using Autofac;
using NUnit.Framework;

namespace StudentsApi.DataAccess.Tests
{
    public class DataAccessTest
    {
        protected static IContainer Container;
        protected static DatabaseService DatabaseService;

        [OneTimeSetUp]
        public void Setup()
        {
            var dir = Path.GetDirectoryName(typeof(DataAccessTest).Assembly.Location);
            Directory.SetCurrentDirectory(dir);
            var builder = new ContainerBuilder();
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<TestModule>();
            Container = builder.Build();
            DatabaseService = Container.Resolve<DatabaseService>();
        }

        [OneTimeTearDown]
        public void Dispose()
        {
            Container.Dispose();
        }
    }
}
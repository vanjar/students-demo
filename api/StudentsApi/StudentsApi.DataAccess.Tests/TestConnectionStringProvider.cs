﻿namespace StudentsApi.DataAccess.Tests
{
    public class TestConnectionStringProvider : IConnectionStringProvider
    {
        public string ConnectionString { get; }

        public TestConnectionStringProvider()
        {
            ConnectionString = "Server=.;Database=StudentsDemoIntegrationTest;Trusted_Connection=True;";
        }
    }
}

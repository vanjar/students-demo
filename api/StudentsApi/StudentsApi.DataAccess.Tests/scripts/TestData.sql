set identity_insert faculty on
insert into Faculty (FacultyId, Name) values (1, 'Sugino Women''s College');
insert into Faculty (FacultyId, Name) values (2, 'Jahangirnagar University');
insert into Faculty (FacultyId, Name) values (3, 'Université du Littoral Cote d''Opale');
insert into Faculty (FacultyId, Name) values (4, 'Sri Ramachardra Medical College and Research Institute');
insert into Faculty (FacultyId, Name) values (5, 'Universität Flensburg');
insert into Faculty (FacultyId, Name) values (6, 'Durban Institute of Technology');
insert into Faculty (FacultyId, Name) values (7, 'Universidad Autónoma Gabriel René Moreno');
insert into Faculty (FacultyId, Name) values (8, 'Royal University of Agriculture');
insert into Faculty (FacultyId, Name) values (9, 'National Tainan Teachers College');
insert into Faculty (FacultyId, Name) values (10, 'Siebold University of Nagasaki');
set identity_insert faculty off

set identity_insert department on
insert into Department (DepartmentId, FacultyId, Name) values (1, 1, 'Dpt enim sit');
insert into Department (DepartmentId, FacultyId, Name) values (2, 2, 'Dpt sociis natoque');
insert into Department (DepartmentId, FacultyId, Name) values (3, 3, 'Dpt dapibus augue');
insert into Department (DepartmentId, FacultyId, Name) values (4, 4, 'Dpt volutpat in congue');
insert into Department (DepartmentId, FacultyId, Name) values (5, 5, 'Dpt nulla sed');
insert into Department (DepartmentId, FacultyId, Name) values (6, 6, 'Dpt in purus eu');
insert into Department (DepartmentId, FacultyId, Name) values (7, 7, 'Dpt eu magna vulputate');
insert into Department (DepartmentId, FacultyId, Name) values (8, 8, 'Dpt maecenas ut');
insert into Department (DepartmentId, FacultyId, Name) values (9, 9, 'Dpt amet justo');
insert into Department (DepartmentId, FacultyId, Name) values (10, 10, 'Dpt nulla tellus');
insert into Department (DepartmentId, FacultyId, Name) values (11, 1, 'Dpt egestas metus');
insert into Department (DepartmentId, FacultyId, Name) values (12, 2, 'Dpt odio elementum');
insert into Department (DepartmentId, FacultyId, Name) values (13, 3, 'Dpt morbi quis tortor');
insert into Department (DepartmentId, FacultyId, Name) values (14, 4, 'Dpt quam nec');
insert into Department (DepartmentId, FacultyId, Name) values (15, 5, 'Dpt in congue');
insert into Department (DepartmentId, FacultyId, Name) values (16, 6, 'Dpt phasellus id');
insert into Department (DepartmentId, FacultyId, Name) values (17, 7, 'Dpt nunc viverra dapibus');
insert into Department (DepartmentId, FacultyId, Name) values (18, 8, 'Dpt accumsan tellus nisi');
insert into Department (DepartmentId, FacultyId, Name) values (19, 9, 'Dpt sit amet justo');
insert into Department (DepartmentId, FacultyId, Name) values (20, 10, 'Dpt duis bibendum');
insert into Department (DepartmentId, FacultyId, Name) values (21, 1, 'Dpt ridiculus mus');
insert into Department (DepartmentId, FacultyId, Name) values (22, 2, 'Dpt elit proin');
insert into Department (DepartmentId, FacultyId, Name) values (23, 3, 'Dpt praesent blandit lacinia');
insert into Department (DepartmentId, FacultyId, Name) values (24, 4, 'Dpt sit amet');
insert into Department (DepartmentId, FacultyId, Name) values (25, 5, 'Dpt nulla elit ac');
insert into Department (DepartmentId, FacultyId, Name) values (26, 6, 'Dpt morbi a ipsum');
insert into Department (DepartmentId, FacultyId, Name) values (27, 7, 'Dpt ante ipsum primis');
insert into Department (DepartmentId, FacultyId, Name) values (28, 8, 'Dpt fermentum justo');
insert into Department (DepartmentId, FacultyId, Name) values (29, 9, 'Dpt lacus morbi sem');
insert into Department (DepartmentId, FacultyId, Name) values (30, 10, 'Dpt sed accumsan');
insert into Department (DepartmentId, FacultyId, Name) values (31, 1, 'Dpt libero nam dui');
insert into Department (DepartmentId, FacultyId, Name) values (32, 2, 'Dpt tempus sit amet');
insert into Department (DepartmentId, FacultyId, Name) values (33, 3, 'Dpt nam tristique tortor');
insert into Department (DepartmentId, FacultyId, Name) values (34, 4, 'Dpt amet consectetuer');
insert into Department (DepartmentId, FacultyId, Name) values (35, 5, 'Dpt volutpat eleifend donec');
insert into Department (DepartmentId, FacultyId, Name) values (36, 6, 'Dpt ac nibh fusce');
insert into Department (DepartmentId, FacultyId, Name) values (37, 7, 'Dpt orci luctus et');
insert into Department (DepartmentId, FacultyId, Name) values (38, 8, 'Dpt integer ac leo');
insert into Department (DepartmentId, FacultyId, Name) values (39, 9, 'Dpt nunc rhoncus');
insert into Department (DepartmentId, FacultyId, Name) values (40, 10, 'Dpt purus aliquet');
insert into Department (DepartmentId, FacultyId, Name) values (41, 1, 'Dpt mi nulla');
insert into Department (DepartmentId, FacultyId, Name) values (42, 2, 'Dpt nam congue risus');
insert into Department (DepartmentId, FacultyId, Name) values (43, 3, 'Dpt in blandit ultrices');
insert into Department (DepartmentId, FacultyId, Name) values (44, 4, 'Dpt sapien arcu sed');
insert into Department (DepartmentId, FacultyId, Name) values (45, 5, 'Dpt tortor sollicitudin');
insert into Department (DepartmentId, FacultyId, Name) values (46, 6, 'Dpt in blandit ultrices');
insert into Department (DepartmentId, FacultyId, Name) values (47, 7, 'Dpt posuere cubilia');
insert into Department (DepartmentId, FacultyId, Name) values (48, 8, 'Dpt primis in');
insert into Department (DepartmentId, FacultyId, Name) values (49, 9, 'Dpt sapien arcu sed');
insert into Department (DepartmentId, FacultyId, Name) values (50, 10, 'Dpt phasellus in felis');
insert into Department (DepartmentId, FacultyId, Name) values (51, 1, 'Dpt ut nulla');
insert into Department (DepartmentId, FacultyId, Name) values (52, 2, 'Dpt at nulla');
insert into Department (DepartmentId, FacultyId, Name) values (53, 3, 'Dpt vitae consectetuer');
insert into Department (DepartmentId, FacultyId, Name) values (54, 4, 'Dpt quis orci');
insert into Department (DepartmentId, FacultyId, Name) values (55, 5, 'Dpt in imperdiet');
insert into Department (DepartmentId, FacultyId, Name) values (56, 6, 'Dpt turpis donec posuere');
insert into Department (DepartmentId, FacultyId, Name) values (57, 7, 'Dpt donec posuere metus');
insert into Department (DepartmentId, FacultyId, Name) values (58, 8, 'Dpt suspendisse potenti in');
insert into Department (DepartmentId, FacultyId, Name) values (59, 9, 'Dpt odio donec');
insert into Department (DepartmentId, FacultyId, Name) values (60, 10, 'Dpt massa id lobortis');
insert into Department (DepartmentId, FacultyId, Name) values (61, 1, 'Dpt libero quis');
insert into Department (DepartmentId, FacultyId, Name) values (62, 2, 'Dpt nisl venenatis');
insert into Department (DepartmentId, FacultyId, Name) values (63, 3, 'Dpt pretium iaculis diam');
insert into Department (DepartmentId, FacultyId, Name) values (64, 4, 'Dpt ac est lacinia');
insert into Department (DepartmentId, FacultyId, Name) values (65, 5, 'Dpt nulla sed');
insert into Department (DepartmentId, FacultyId, Name) values (66, 6, 'Dpt lorem vitae');
insert into Department (DepartmentId, FacultyId, Name) values (67, 7, 'Dpt vestibulum aliquet');
insert into Department (DepartmentId, FacultyId, Name) values (68, 8, 'Dpt cum sociis');
insert into Department (DepartmentId, FacultyId, Name) values (69, 9, 'Dpt nulla neque');
insert into Department (DepartmentId, FacultyId, Name) values (70, 10, 'Dpt faucibus orci');
insert into Department (DepartmentId, FacultyId, Name) values (71, 1, 'Dpt sem sed sagittis');
insert into Department (DepartmentId, FacultyId, Name) values (72, 2, 'Dpt sed magna at');
insert into Department (DepartmentId, FacultyId, Name) values (73, 3, 'Dpt vestibulum eget');
insert into Department (DepartmentId, FacultyId, Name) values (74, 4, 'Dpt varius ut blandit');
insert into Department (DepartmentId, FacultyId, Name) values (75, 5, 'Dpt lobortis ligula sit');
insert into Department (DepartmentId, FacultyId, Name) values (76, 6, 'Dpt at lorem integer');
insert into Department (DepartmentId, FacultyId, Name) values (77, 7, 'Dpt blandit ultrices');
insert into Department (DepartmentId, FacultyId, Name) values (78, 8, 'Dpt fermentum justo nec');
insert into Department (DepartmentId, FacultyId, Name) values (79, 9, 'Dpt maecenas tincidunt');
insert into Department (DepartmentId, FacultyId, Name) values (80, 10, 'Dpt et eros');
insert into Department (DepartmentId, FacultyId, Name) values (81, 1, 'Dpt vitae quam suspendisse');
insert into Department (DepartmentId, FacultyId, Name) values (82, 2, 'Dpt cubilia curae donec');
insert into Department (DepartmentId, FacultyId, Name) values (83, 3, 'Dpt vestibulum quam sapien');
insert into Department (DepartmentId, FacultyId, Name) values (84, 4, 'Dpt at turpis');
insert into Department (DepartmentId, FacultyId, Name) values (85, 5, 'Dpt nam ultrices libero');
insert into Department (DepartmentId, FacultyId, Name) values (86, 6, 'Dpt libero quis');
insert into Department (DepartmentId, FacultyId, Name) values (87, 7, 'Dpt mollis molestie');
insert into Department (DepartmentId, FacultyId, Name) values (88, 8, 'Dpt eu est congue');
insert into Department (DepartmentId, FacultyId, Name) values (89, 9, 'Dpt integer tincidunt');
insert into Department (DepartmentId, FacultyId, Name) values (90, 10, 'Dpt diam in');
insert into Department (DepartmentId, FacultyId, Name) values (91, 1, 'Dpt neque vestibulum');
insert into Department (DepartmentId, FacultyId, Name) values (92, 2, 'Dpt tortor quis');
insert into Department (DepartmentId, FacultyId, Name) values (93, 3, 'Dpt viverra pede ac');
insert into Department (DepartmentId, FacultyId, Name) values (94, 4, 'Dpt eu felis');
insert into Department (DepartmentId, FacultyId, Name) values (95, 5, 'Dpt in blandit ultrices');
insert into Department (DepartmentId, FacultyId, Name) values (96, 6, 'Dpt mauris lacinia');
insert into Department (DepartmentId, FacultyId, Name) values (97, 7, 'Dpt mauris non ligula');
insert into Department (DepartmentId, FacultyId, Name) values (98, 8, 'Dpt non quam');
insert into Department (DepartmentId, FacultyId, Name) values (99, 9, 'Dpt fusce congue');
insert into Department (DepartmentId, FacultyId, Name) values (100, 10, 'Dpt lacinia nisi venenatis');
set identity_insert department off

set identity_insert course on
insert into Course (CourseId, FacultyId, Name) values (1, 1, 'Extended bi-directional concept');
insert into Course (CourseId, FacultyId, Name) values (2, 2, 'Self-enabling intermediate Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (3, 3, 'Optional scalable system engine');
insert into Course (CourseId, FacultyId, Name) values (4, 4, 'Optimized coherent attitude');
insert into Course (CourseId, FacultyId, Name) values (5, 5, 'Distributed bandwidth-monitored projection');
insert into Course (CourseId, FacultyId, Name) values (6, 6, 'Team-oriented explicit product');
insert into Course (CourseId, FacultyId, Name) values (7, 7, 'Synergistic dedicated policy');
insert into Course (CourseId, FacultyId, Name) values (8, 8, 'User-friendly 4th generation secured line');
insert into Course (CourseId, FacultyId, Name) values (9, 9, 'Fundamental executive strategy');
insert into Course (CourseId, FacultyId, Name) values (10, 10, 'Optimized value-added access');
insert into Course (CourseId, FacultyId, Name) values (11, 1, 'Sharable regional process improvement');
insert into Course (CourseId, FacultyId, Name) values (12, 2, 'Pre-emptive systemic help-desk');
insert into Course (CourseId, FacultyId, Name) values (13, 3, 'Realigned bi-directional concept');
insert into Course (CourseId, FacultyId, Name) values (14, 4, 'Universal explicit solution');
insert into Course (CourseId, FacultyId, Name) values (15, 5, 'Optimized multi-tasking success');
insert into Course (CourseId, FacultyId, Name) values (16, 6, 'Programmable holistic throughput');
insert into Course (CourseId, FacultyId, Name) values (17, 7, 'Versatile bi-directional matrix');
insert into Course (CourseId, FacultyId, Name) values (18, 8, 'Right-sized leading edge process improvement');
insert into Course (CourseId, FacultyId, Name) values (19, 9, 'Object-based executive flexibility');
insert into Course (CourseId, FacultyId, Name) values (20, 10, 'Networked leading edge functionalities');
insert into Course (CourseId, FacultyId, Name) values (21, 1, 'Cross-group composite neural-net');
insert into Course (CourseId, FacultyId, Name) values (22, 2, 'Progressive needs-based help-desk');
insert into Course (CourseId, FacultyId, Name) values (23, 3, 'Synergized analyzing challenge');
insert into Course (CourseId, FacultyId, Name) values (24, 4, 'Organic contextually-based instruction set');
insert into Course (CourseId, FacultyId, Name) values (25, 5, 'Compatible static forecast');
insert into Course (CourseId, FacultyId, Name) values (26, 6, 'Upgradable mission-critical flexibility');
insert into Course (CourseId, FacultyId, Name) values (27, 7, 'Cloned disintermediate alliance');
insert into Course (CourseId, FacultyId, Name) values (28, 8, 'Enhanced tangible emulation');
insert into Course (CourseId, FacultyId, Name) values (29, 9, 'Focused reciprocal analyzer');
insert into Course (CourseId, FacultyId, Name) values (30, 10, 'Phased system-worthy concept');
insert into Course (CourseId, FacultyId, Name) values (31, 1, 'Proactive 6th generation strategy');
insert into Course (CourseId, FacultyId, Name) values (32, 2, 'Persevering exuding hierarchy');
insert into Course (CourseId, FacultyId, Name) values (33, 3, 'Cross-platform uniform capability');
insert into Course (CourseId, FacultyId, Name) values (34, 4, 'Intuitive foreground structure');
insert into Course (CourseId, FacultyId, Name) values (35, 5, 'Right-sized didactic attitude');
insert into Course (CourseId, FacultyId, Name) values (36, 6, 'Ameliorated impactful success');
insert into Course (CourseId, FacultyId, Name) values (37, 7, 'Public-key didactic secured line');
insert into Course (CourseId, FacultyId, Name) values (38, 8, 'Reverse-engineered 3rd generation process improvement');
insert into Course (CourseId, FacultyId, Name) values (39, 9, 'Configurable web-enabled contingency');
insert into Course (CourseId, FacultyId, Name) values (40, 10, 'Advanced well-modulated architecture');
insert into Course (CourseId, FacultyId, Name) values (41, 1, 'Horizontal tertiary service-desk');
insert into Course (CourseId, FacultyId, Name) values (42, 2, 'Cross-group foreground emulation');
insert into Course (CourseId, FacultyId, Name) values (43, 3, 'Compatible methodical conglomeration');
insert into Course (CourseId, FacultyId, Name) values (44, 4, 'Networked intangible neural-net');
insert into Course (CourseId, FacultyId, Name) values (45, 5, 'Public-key systematic neural-net');
insert into Course (CourseId, FacultyId, Name) values (46, 6, 'Compatible neutral capability');
insert into Course (CourseId, FacultyId, Name) values (47, 7, 'Synchronised fault-tolerant monitoring');
insert into Course (CourseId, FacultyId, Name) values (48, 8, 'Ameliorated methodical help-desk');
insert into Course (CourseId, FacultyId, Name) values (49, 9, 'Quality-focused heuristic open architecture');
insert into Course (CourseId, FacultyId, Name) values (50, 10, 'Public-key foreground matrix');
insert into Course (CourseId, FacultyId, Name) values (51, 1, 'Stand-alone non-volatile projection');
insert into Course (CourseId, FacultyId, Name) values (52, 2, 'Optional discrete superstructure');
insert into Course (CourseId, FacultyId, Name) values (53, 3, 'Extended national product');
insert into Course (CourseId, FacultyId, Name) values (54, 4, 'Fully-configurable directional parallelism');
insert into Course (CourseId, FacultyId, Name) values (55, 5, 'Managed neutral attitude');
insert into Course (CourseId, FacultyId, Name) values (56, 6, 'Secured client-driven matrices');
insert into Course (CourseId, FacultyId, Name) values (57, 7, 'Open-architected impactful knowledge base');
insert into Course (CourseId, FacultyId, Name) values (58, 8, 'Reverse-engineered eco-centric collaboration');
insert into Course (CourseId, FacultyId, Name) values (59, 9, 'Reverse-engineered hybrid success');
insert into Course (CourseId, FacultyId, Name) values (60, 10, 'Switchable multimedia policy');
insert into Course (CourseId, FacultyId, Name) values (61, 1, 'Enterprise-wide regional instruction set');
insert into Course (CourseId, FacultyId, Name) values (62, 2, 'Multi-lateral methodical focus group');
insert into Course (CourseId, FacultyId, Name) values (63, 3, 'Reduced full-range ability');
insert into Course (CourseId, FacultyId, Name) values (64, 4, 'Distributed solution-oriented array');
insert into Course (CourseId, FacultyId, Name) values (65, 5, 'Automated attitude-oriented groupware');
insert into Course (CourseId, FacultyId, Name) values (66, 6, 'Cross-platform cohesive hardware');
insert into Course (CourseId, FacultyId, Name) values (67, 7, 'Horizontal motivating data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (68, 8, 'Self-enabling responsive extranet');
insert into Course (CourseId, FacultyId, Name) values (69, 9, 'Customer-focused background forecast');
insert into Course (CourseId, FacultyId, Name) values (70, 10, 'Vision-oriented maximized forecast');
insert into Course (CourseId, FacultyId, Name) values (71, 1, 'Re-engineered multi-state core');
insert into Course (CourseId, FacultyId, Name) values (72, 2, 'Polarised maximized policy');
insert into Course (CourseId, FacultyId, Name) values (73, 3, 'Seamless dynamic access');
insert into Course (CourseId, FacultyId, Name) values (74, 4, 'Phased tertiary emulation');
insert into Course (CourseId, FacultyId, Name) values (75, 5, 'Focused system-worthy open system');
insert into Course (CourseId, FacultyId, Name) values (76, 6, 'Synergized asynchronous encryption');
insert into Course (CourseId, FacultyId, Name) values (77, 7, 'Right-sized global product');
insert into Course (CourseId, FacultyId, Name) values (78, 8, 'Ameliorated 6th generation model');
insert into Course (CourseId, FacultyId, Name) values (79, 9, 'Networked transitional capacity');
insert into Course (CourseId, FacultyId, Name) values (80, 10, 'Decentralized secondary product');
insert into Course (CourseId, FacultyId, Name) values (81, 1, 'Streamlined uniform leverage');
insert into Course (CourseId, FacultyId, Name) values (82, 2, 'Realigned incremental leverage');
insert into Course (CourseId, FacultyId, Name) values (83, 3, 'Front-line 6th generation complexity');
insert into Course (CourseId, FacultyId, Name) values (84, 4, 'Re-contextualized next generation initiative');
insert into Course (CourseId, FacultyId, Name) values (85, 5, 'Reactive executive Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (86, 6, 'Re-contextualized holistic toolset');
insert into Course (CourseId, FacultyId, Name) values (87, 7, 'Down-sized object-oriented ability');
insert into Course (CourseId, FacultyId, Name) values (88, 8, 'Sharable optimizing process improvement');
insert into Course (CourseId, FacultyId, Name) values (89, 9, 'Monitored full-range budgetary management');
insert into Course (CourseId, FacultyId, Name) values (90, 10, 'Compatible homogeneous concept');
insert into Course (CourseId, FacultyId, Name) values (91, 1, 'Networked heuristic architecture');
insert into Course (CourseId, FacultyId, Name) values (92, 2, 'User-centric didactic functionalities');
insert into Course (CourseId, FacultyId, Name) values (93, 3, 'Persistent well-modulated capacity');
insert into Course (CourseId, FacultyId, Name) values (94, 4, 'Devolved bi-directional middleware');
insert into Course (CourseId, FacultyId, Name) values (95, 5, 'Progressive optimal interface');
insert into Course (CourseId, FacultyId, Name) values (96, 6, 'Grass-roots upward-trending hub');
insert into Course (CourseId, FacultyId, Name) values (97, 7, 'Horizontal full-range utilisation');
insert into Course (CourseId, FacultyId, Name) values (98, 8, 'Balanced upward-trending projection');
insert into Course (CourseId, FacultyId, Name) values (99, 9, 'Future-proofed hybrid standardization');
insert into Course (CourseId, FacultyId, Name) values (100, 10, 'Cross-group regional function');
insert into Course (CourseId, FacultyId, Name) values (101, 1, 'Front-line clear-thinking infrastructure');
insert into Course (CourseId, FacultyId, Name) values (102, 2, 'Automated secondary workforce');
insert into Course (CourseId, FacultyId, Name) values (103, 3, 'Programmable regional productivity');
insert into Course (CourseId, FacultyId, Name) values (104, 4, 'Future-proofed neutral analyzer');
insert into Course (CourseId, FacultyId, Name) values (105, 5, 'Ameliorated exuding utilisation');
insert into Course (CourseId, FacultyId, Name) values (106, 6, 'Versatile background policy');
insert into Course (CourseId, FacultyId, Name) values (107, 7, 'Diverse intermediate solution');
insert into Course (CourseId, FacultyId, Name) values (108, 8, 'Distributed 24 hour focus group');
insert into Course (CourseId, FacultyId, Name) values (109, 9, 'Reduced dynamic process improvement');
insert into Course (CourseId, FacultyId, Name) values (110, 10, 'Persistent motivating moderator');
insert into Course (CourseId, FacultyId, Name) values (111, 1, 'Switchable homogeneous artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (112, 2, 'Persistent systematic moratorium');
insert into Course (CourseId, FacultyId, Name) values (113, 3, 'Cloned zero defect task-force');
insert into Course (CourseId, FacultyId, Name) values (114, 4, 'Operative modular frame');
insert into Course (CourseId, FacultyId, Name) values (115, 5, 'Compatible mission-critical project');
insert into Course (CourseId, FacultyId, Name) values (116, 6, 'Sharable secondary software');
insert into Course (CourseId, FacultyId, Name) values (117, 7, 'Open-source disintermediate implementation');
insert into Course (CourseId, FacultyId, Name) values (118, 8, 'Inverse static firmware');
insert into Course (CourseId, FacultyId, Name) values (119, 9, 'De-engineered client-server throughput');
insert into Course (CourseId, FacultyId, Name) values (120, 10, 'Realigned human-resource initiative');
insert into Course (CourseId, FacultyId, Name) values (121, 1, 'Sharable impactful hierarchy');
insert into Course (CourseId, FacultyId, Name) values (122, 2, 'Persevering tertiary internet solution');
insert into Course (CourseId, FacultyId, Name) values (123, 3, 'Multi-channelled composite model');
insert into Course (CourseId, FacultyId, Name) values (124, 4, 'Synergistic solution-oriented middleware');
insert into Course (CourseId, FacultyId, Name) values (125, 5, 'Networked human-resource software');
insert into Course (CourseId, FacultyId, Name) values (126, 6, 'Seamless analyzing encryption');
insert into Course (CourseId, FacultyId, Name) values (127, 7, 'Adaptive foreground moderator');
insert into Course (CourseId, FacultyId, Name) values (128, 8, 'Versatile next generation migration');
insert into Course (CourseId, FacultyId, Name) values (129, 9, 'Multi-channelled non-volatile extranet');
insert into Course (CourseId, FacultyId, Name) values (130, 10, 'Programmable static solution');
insert into Course (CourseId, FacultyId, Name) values (131, 1, 'Cloned mobile installation');
insert into Course (CourseId, FacultyId, Name) values (132, 2, 'Persevering context-sensitive algorithm');
insert into Course (CourseId, FacultyId, Name) values (133, 3, 'Synchronised tangible portal');
insert into Course (CourseId, FacultyId, Name) values (134, 4, 'Optional motivating knowledge base');
insert into Course (CourseId, FacultyId, Name) values (135, 5, 'Visionary mobile encryption');
insert into Course (CourseId, FacultyId, Name) values (136, 6, 'Open-architected asymmetric knowledge user');
insert into Course (CourseId, FacultyId, Name) values (137, 7, 'Expanded 5th generation framework');
insert into Course (CourseId, FacultyId, Name) values (138, 8, 'Down-sized grid-enabled matrix');
insert into Course (CourseId, FacultyId, Name) values (139, 9, 'Exclusive web-enabled website');
insert into Course (CourseId, FacultyId, Name) values (140, 10, 'Managed 6th generation hardware');
insert into Course (CourseId, FacultyId, Name) values (141, 1, 'Open-architected motivating architecture');
insert into Course (CourseId, FacultyId, Name) values (142, 2, 'Secured systemic hub');
insert into Course (CourseId, FacultyId, Name) values (143, 3, 'Synchronised grid-enabled methodology');
insert into Course (CourseId, FacultyId, Name) values (144, 4, 'Organic disintermediate software');
insert into Course (CourseId, FacultyId, Name) values (145, 5, 'Persevering 3rd generation artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (146, 6, 'Expanded impactful monitoring');
insert into Course (CourseId, FacultyId, Name) values (147, 7, 'Enterprise-wide human-resource neural-net');
insert into Course (CourseId, FacultyId, Name) values (148, 8, 'User-friendly asynchronous instruction set');
insert into Course (CourseId, FacultyId, Name) values (149, 9, 'Monitored client-driven infrastructure');
insert into Course (CourseId, FacultyId, Name) values (150, 10, 'Virtual bottom-line software');
insert into Course (CourseId, FacultyId, Name) values (151, 1, 'Universal context-sensitive secured line');
insert into Course (CourseId, FacultyId, Name) values (152, 2, 'Extended dedicated structure');
insert into Course (CourseId, FacultyId, Name) values (153, 3, 'Decentralized heuristic throughput');
insert into Course (CourseId, FacultyId, Name) values (154, 4, 'Phased regional moratorium');
insert into Course (CourseId, FacultyId, Name) values (155, 5, 'Fully-configurable coherent methodology');
insert into Course (CourseId, FacultyId, Name) values (156, 6, 'Digitized asynchronous forecast');
insert into Course (CourseId, FacultyId, Name) values (157, 7, 'Monitored secondary data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (158, 8, 'Upgradable zero tolerance interface');
insert into Course (CourseId, FacultyId, Name) values (159, 9, 'Synchronised global project');
insert into Course (CourseId, FacultyId, Name) values (160, 10, 'Face to face mission-critical utilisation');
insert into Course (CourseId, FacultyId, Name) values (161, 1, 'Balanced hybrid Graphic Interface');
insert into Course (CourseId, FacultyId, Name) values (162, 2, 'Grass-roots neutral help-desk');
insert into Course (CourseId, FacultyId, Name) values (163, 3, 'Synergized full-range model');
insert into Course (CourseId, FacultyId, Name) values (164, 4, 'Assimilated value-added internet solution');
insert into Course (CourseId, FacultyId, Name) values (165, 5, 'De-engineered clear-thinking strategy');
insert into Course (CourseId, FacultyId, Name) values (166, 6, 'Virtual context-sensitive frame');
insert into Course (CourseId, FacultyId, Name) values (167, 7, 'Cloned dedicated groupware');
insert into Course (CourseId, FacultyId, Name) values (168, 8, 'Object-based optimizing migration');
insert into Course (CourseId, FacultyId, Name) values (169, 9, 'Right-sized contextually-based orchestration');
insert into Course (CourseId, FacultyId, Name) values (170, 10, 'Versatile composite paradigm');
insert into Course (CourseId, FacultyId, Name) values (171, 1, 'Re-engineered regional task-force');
insert into Course (CourseId, FacultyId, Name) values (172, 2, 'Streamlined maximized groupware');
insert into Course (CourseId, FacultyId, Name) values (173, 3, 'Persistent directional contingency');
insert into Course (CourseId, FacultyId, Name) values (174, 4, 'Decentralized asymmetric secured line');
insert into Course (CourseId, FacultyId, Name) values (175, 5, 'Enhanced optimizing database');
insert into Course (CourseId, FacultyId, Name) values (176, 6, 'Total regional focus group');
insert into Course (CourseId, FacultyId, Name) values (177, 7, 'Ameliorated analyzing application');
insert into Course (CourseId, FacultyId, Name) values (178, 8, 'Compatible high-level instruction set');
insert into Course (CourseId, FacultyId, Name) values (179, 9, 'Upgradable mission-critical secured line');
insert into Course (CourseId, FacultyId, Name) values (180, 10, 'Exclusive neutral groupware');
insert into Course (CourseId, FacultyId, Name) values (181, 1, 'User-centric systemic help-desk');
insert into Course (CourseId, FacultyId, Name) values (182, 2, 'Front-line contextually-based parallelism');
insert into Course (CourseId, FacultyId, Name) values (183, 3, 'Total interactive infrastructure');
insert into Course (CourseId, FacultyId, Name) values (184, 4, 'Configurable fresh-thinking monitoring');
insert into Course (CourseId, FacultyId, Name) values (185, 5, 'Visionary 4th generation adapter');
insert into Course (CourseId, FacultyId, Name) values (186, 6, 'User-centric multi-tasking emulation');
insert into Course (CourseId, FacultyId, Name) values (187, 7, 'Diverse leading edge functionalities');
insert into Course (CourseId, FacultyId, Name) values (188, 8, 'Cloned foreground software');
insert into Course (CourseId, FacultyId, Name) values (189, 9, 'Grass-roots zero administration strategy');
insert into Course (CourseId, FacultyId, Name) values (190, 10, 'Total tertiary implementation');
insert into Course (CourseId, FacultyId, Name) values (191, 1, 'Multi-tiered motivating Graphic Interface');
insert into Course (CourseId, FacultyId, Name) values (192, 2, 'Triple-buffered background installation');
insert into Course (CourseId, FacultyId, Name) values (193, 3, 'Robust stable info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (194, 4, 'Automated 4th generation extranet');
insert into Course (CourseId, FacultyId, Name) values (195, 5, 'Robust methodical support');
insert into Course (CourseId, FacultyId, Name) values (196, 6, 'Inverse 24/7 knowledge user');
insert into Course (CourseId, FacultyId, Name) values (197, 7, 'De-engineered asynchronous superstructure');
insert into Course (CourseId, FacultyId, Name) values (198, 8, 'Right-sized optimal capacity');
insert into Course (CourseId, FacultyId, Name) values (199, 9, 'Object-based regional conglomeration');
insert into Course (CourseId, FacultyId, Name) values (200, 10, 'Self-enabling logistical hardware');
insert into Course (CourseId, FacultyId, Name) values (201, 1, 'Seamless logistical forecast');
insert into Course (CourseId, FacultyId, Name) values (202, 2, 'Business-focused homogeneous system engine');
insert into Course (CourseId, FacultyId, Name) values (203, 3, 'Optional user-facing structure');
insert into Course (CourseId, FacultyId, Name) values (204, 4, 'Synergistic methodical database');
insert into Course (CourseId, FacultyId, Name) values (205, 5, 'Persistent secondary synergy');
insert into Course (CourseId, FacultyId, Name) values (206, 6, 'Decentralized 3rd generation middleware');
insert into Course (CourseId, FacultyId, Name) values (207, 7, 'Phased eco-centric installation');
insert into Course (CourseId, FacultyId, Name) values (208, 8, 'Open-source clear-thinking forecast');
insert into Course (CourseId, FacultyId, Name) values (209, 9, 'Open-source high-level product');
insert into Course (CourseId, FacultyId, Name) values (210, 10, 'Re-engineered dynamic synergy');
insert into Course (CourseId, FacultyId, Name) values (211, 1, 'Up-sized national flexibility');
insert into Course (CourseId, FacultyId, Name) values (212, 2, 'Object-based holistic emulation');
insert into Course (CourseId, FacultyId, Name) values (213, 3, 'Implemented bi-directional matrix');
insert into Course (CourseId, FacultyId, Name) values (214, 4, 'Quality-focused content-based archive');
insert into Course (CourseId, FacultyId, Name) values (215, 5, 'Multi-tiered upward-trending synergy');
insert into Course (CourseId, FacultyId, Name) values (216, 6, 'Vision-oriented mobile support');
insert into Course (CourseId, FacultyId, Name) values (217, 7, 'Front-line upward-trending service-desk');
insert into Course (CourseId, FacultyId, Name) values (218, 8, 'Streamlined solution-oriented solution');
insert into Course (CourseId, FacultyId, Name) values (219, 9, 'Profound systematic leverage');
insert into Course (CourseId, FacultyId, Name) values (220, 10, 'Multi-channelled optimizing paradigm');
insert into Course (CourseId, FacultyId, Name) values (221, 1, 'Organized high-level emulation');
insert into Course (CourseId, FacultyId, Name) values (222, 2, 'Re-contextualized composite project');
insert into Course (CourseId, FacultyId, Name) values (223, 3, 'Adaptive leading edge ability');
insert into Course (CourseId, FacultyId, Name) values (224, 4, 'Programmable leading edge access');
insert into Course (CourseId, FacultyId, Name) values (225, 5, 'Object-based zero defect system engine');
insert into Course (CourseId, FacultyId, Name) values (226, 6, 'Mandatory multi-tasking software');
insert into Course (CourseId, FacultyId, Name) values (227, 7, 'Universal coherent complexity');
insert into Course (CourseId, FacultyId, Name) values (228, 8, 'Front-line object-oriented application');
insert into Course (CourseId, FacultyId, Name) values (229, 9, 'Profound full-range functionalities');
insert into Course (CourseId, FacultyId, Name) values (230, 10, 'Object-based object-oriented leverage');
insert into Course (CourseId, FacultyId, Name) values (231, 1, 'Configurable client-driven middleware');
insert into Course (CourseId, FacultyId, Name) values (232, 2, 'Pre-emptive high-level function');
insert into Course (CourseId, FacultyId, Name) values (233, 3, 'De-engineered homogeneous hub');
insert into Course (CourseId, FacultyId, Name) values (234, 4, 'Expanded uniform focus group');
insert into Course (CourseId, FacultyId, Name) values (235, 5, 'Ameliorated heuristic parallelism');
insert into Course (CourseId, FacultyId, Name) values (236, 6, 'Enterprise-wide dynamic success');
insert into Course (CourseId, FacultyId, Name) values (237, 7, 'Customizable eco-centric process improvement');
insert into Course (CourseId, FacultyId, Name) values (238, 8, 'Synchronised client-server encoding');
insert into Course (CourseId, FacultyId, Name) values (239, 9, 'Triple-buffered 24 hour monitoring');
insert into Course (CourseId, FacultyId, Name) values (240, 10, 'Profound tangible forecast');
insert into Course (CourseId, FacultyId, Name) values (241, 1, 'Expanded dedicated array');
insert into Course (CourseId, FacultyId, Name) values (242, 2, 'Total composite artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (243, 3, 'Configurable clear-thinking approach');
insert into Course (CourseId, FacultyId, Name) values (244, 4, 'Realigned mission-critical methodology');
insert into Course (CourseId, FacultyId, Name) values (245, 5, 'Multi-layered optimizing orchestration');
insert into Course (CourseId, FacultyId, Name) values (246, 6, 'Function-based modular capacity');
insert into Course (CourseId, FacultyId, Name) values (247, 7, 'Streamlined optimal algorithm');
insert into Course (CourseId, FacultyId, Name) values (248, 8, 'Synergistic analyzing matrices');
insert into Course (CourseId, FacultyId, Name) values (249, 9, 'De-engineered mobile hierarchy');
insert into Course (CourseId, FacultyId, Name) values (250, 10, 'Networked foreground database');
insert into Course (CourseId, FacultyId, Name) values (251, 1, 'Multi-tiered system-worthy ability');
insert into Course (CourseId, FacultyId, Name) values (252, 2, 'Optional tangible firmware');
insert into Course (CourseId, FacultyId, Name) values (253, 3, 'Reduced mobile portal');
insert into Course (CourseId, FacultyId, Name) values (254, 4, 'Configurable explicit monitoring');
insert into Course (CourseId, FacultyId, Name) values (255, 5, 'Ameliorated global capacity');
insert into Course (CourseId, FacultyId, Name) values (256, 6, 'User-friendly client-driven productivity');
insert into Course (CourseId, FacultyId, Name) values (257, 7, 'Proactive analyzing encoding');
insert into Course (CourseId, FacultyId, Name) values (258, 8, 'Distributed system-worthy installation');
insert into Course (CourseId, FacultyId, Name) values (259, 9, 'Fully-configurable 6th generation analyzer');
insert into Course (CourseId, FacultyId, Name) values (260, 10, 'Compatible regional data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (261, 1, 'De-engineered grid-enabled hardware');
insert into Course (CourseId, FacultyId, Name) values (262, 2, 'Ameliorated human-resource encryption');
insert into Course (CourseId, FacultyId, Name) values (263, 3, 'Team-oriented contextually-based open system');
insert into Course (CourseId, FacultyId, Name) values (264, 4, 'Triple-buffered neutral info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (265, 5, 'Programmable even-keeled knowledge user');
insert into Course (CourseId, FacultyId, Name) values (266, 6, 'Virtual tangible ability');
insert into Course (CourseId, FacultyId, Name) values (267, 7, 'Re-engineered optimizing focus group');
insert into Course (CourseId, FacultyId, Name) values (268, 8, 'Grass-roots background open system');
insert into Course (CourseId, FacultyId, Name) values (269, 9, 'Function-based 6th generation local area network');
insert into Course (CourseId, FacultyId, Name) values (270, 10, 'Automated systemic budgetary management');
insert into Course (CourseId, FacultyId, Name) values (271, 1, 'Fundamental demand-driven customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (272, 2, 'Visionary full-range algorithm');
insert into Course (CourseId, FacultyId, Name) values (273, 3, 'Integrated coherent customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (274, 4, 'Customizable cohesive pricing structure');
insert into Course (CourseId, FacultyId, Name) values (275, 5, 'Multi-tiered methodical database');
insert into Course (CourseId, FacultyId, Name) values (276, 6, 'Progressive methodical time-frame');
insert into Course (CourseId, FacultyId, Name) values (277, 7, 'Extended 4th generation website');
insert into Course (CourseId, FacultyId, Name) values (278, 8, 'Cross-platform clear-thinking groupware');
insert into Course (CourseId, FacultyId, Name) values (279, 9, 'Triple-buffered mobile middleware');
insert into Course (CourseId, FacultyId, Name) values (280, 10, 'Enterprise-wide 6th generation conglomeration');
insert into Course (CourseId, FacultyId, Name) values (281, 1, 'De-engineered modular policy');
insert into Course (CourseId, FacultyId, Name) values (282, 2, 'Customizable solution-oriented definition');
insert into Course (CourseId, FacultyId, Name) values (283, 3, 'Horizontal content-based flexibility');
insert into Course (CourseId, FacultyId, Name) values (284, 4, 'Optional needs-based capacity');
insert into Course (CourseId, FacultyId, Name) values (285, 5, 'Organized actuating task-force');
insert into Course (CourseId, FacultyId, Name) values (286, 6, 'Re-contextualized logistical knowledge user');
insert into Course (CourseId, FacultyId, Name) values (287, 7, 'Vision-oriented solution-oriented standardization');
insert into Course (CourseId, FacultyId, Name) values (288, 8, 'Progressive executive collaboration');
insert into Course (CourseId, FacultyId, Name) values (289, 9, 'Versatile intangible middleware');
insert into Course (CourseId, FacultyId, Name) values (290, 10, 'Stand-alone grid-enabled hub');
insert into Course (CourseId, FacultyId, Name) values (291, 1, 'Networked local model');
insert into Course (CourseId, FacultyId, Name) values (292, 2, 'Total transitional policy');
insert into Course (CourseId, FacultyId, Name) values (293, 3, 'Programmable fault-tolerant customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (294, 4, 'Upgradable contextually-based project');
insert into Course (CourseId, FacultyId, Name) values (295, 5, 'Programmable content-based Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (296, 6, 'Assimilated directional budgetary management');
insert into Course (CourseId, FacultyId, Name) values (297, 7, 'Integrated contextually-based flexibility');
insert into Course (CourseId, FacultyId, Name) values (298, 8, 'Sharable contextually-based infrastructure');
insert into Course (CourseId, FacultyId, Name) values (299, 9, 'Profit-focused encompassing solution');
insert into Course (CourseId, FacultyId, Name) values (300, 10, 'Pre-emptive client-server array');
insert into Course (CourseId, FacultyId, Name) values (301, 1, 'Exclusive high-level software');
insert into Course (CourseId, FacultyId, Name) values (302, 2, 'Stand-alone bottom-line standardization');
insert into Course (CourseId, FacultyId, Name) values (303, 3, 'Fully-configurable systemic initiative');
insert into Course (CourseId, FacultyId, Name) values (304, 4, 'Customizable mobile product');
insert into Course (CourseId, FacultyId, Name) values (305, 5, 'Stand-alone composite secured line');
insert into Course (CourseId, FacultyId, Name) values (306, 6, 'Enhanced transitional throughput');
insert into Course (CourseId, FacultyId, Name) values (307, 7, 'Total responsive projection');
insert into Course (CourseId, FacultyId, Name) values (308, 8, 'Cross-platform grid-enabled process improvement');
insert into Course (CourseId, FacultyId, Name) values (309, 9, 'User-friendly full-range moderator');
insert into Course (CourseId, FacultyId, Name) values (310, 10, 'Stand-alone optimal process improvement');
insert into Course (CourseId, FacultyId, Name) values (311, 1, 'Seamless object-oriented budgetary management');
insert into Course (CourseId, FacultyId, Name) values (312, 2, 'Multi-channelled maximized local area network');
insert into Course (CourseId, FacultyId, Name) values (313, 3, 'Centralized zero tolerance success');
insert into Course (CourseId, FacultyId, Name) values (314, 4, 'Diverse client-server workforce');
insert into Course (CourseId, FacultyId, Name) values (315, 5, 'Seamless global extranet');
insert into Course (CourseId, FacultyId, Name) values (316, 6, 'Compatible full-range structure');
insert into Course (CourseId, FacultyId, Name) values (317, 7, 'Re-contextualized intangible benchmark');
insert into Course (CourseId, FacultyId, Name) values (318, 8, 'Face to face homogeneous model');
insert into Course (CourseId, FacultyId, Name) values (319, 9, 'Business-focused methodical monitoring');
insert into Course (CourseId, FacultyId, Name) values (320, 10, 'Inverse attitude-oriented leverage');
insert into Course (CourseId, FacultyId, Name) values (321, 1, 'Business-focused context-sensitive policy');
insert into Course (CourseId, FacultyId, Name) values (322, 2, 'User-friendly fresh-thinking strategy');
insert into Course (CourseId, FacultyId, Name) values (323, 3, 'Polarised bi-directional model');
insert into Course (CourseId, FacultyId, Name) values (324, 4, 'Down-sized static utilisation');
insert into Course (CourseId, FacultyId, Name) values (325, 5, 'Optimized scalable protocol');
insert into Course (CourseId, FacultyId, Name) values (326, 6, 'Multi-tiered leading edge projection');
insert into Course (CourseId, FacultyId, Name) values (327, 7, 'Business-focused bifurcated support');
insert into Course (CourseId, FacultyId, Name) values (328, 8, 'Triple-buffered leading edge budgetary management');
insert into Course (CourseId, FacultyId, Name) values (329, 9, 'User-centric optimal interface');
insert into Course (CourseId, FacultyId, Name) values (330, 10, 'Open-source eco-centric software');
insert into Course (CourseId, FacultyId, Name) values (331, 1, 'Reactive methodical implementation');
insert into Course (CourseId, FacultyId, Name) values (332, 2, 'Compatible actuating matrices');
insert into Course (CourseId, FacultyId, Name) values (333, 3, 'Upgradable hybrid concept');
insert into Course (CourseId, FacultyId, Name) values (334, 4, 'Future-proofed fresh-thinking strategy');
insert into Course (CourseId, FacultyId, Name) values (335, 5, 'Vision-oriented user-facing productivity');
insert into Course (CourseId, FacultyId, Name) values (336, 6, 'Expanded zero administration interface');
insert into Course (CourseId, FacultyId, Name) values (337, 7, 'Polarised maximized challenge');
insert into Course (CourseId, FacultyId, Name) values (338, 8, 'Open-architected context-sensitive structure');
insert into Course (CourseId, FacultyId, Name) values (339, 9, 'Persevering regional orchestration');
insert into Course (CourseId, FacultyId, Name) values (340, 10, 'Assimilated methodical framework');
insert into Course (CourseId, FacultyId, Name) values (341, 1, 'Triple-buffered disintermediate moderator');
insert into Course (CourseId, FacultyId, Name) values (342, 2, 'Inverse zero administration concept');
insert into Course (CourseId, FacultyId, Name) values (343, 3, 'Cross-platform disintermediate methodology');
insert into Course (CourseId, FacultyId, Name) values (344, 4, 'Intuitive next generation matrices');
insert into Course (CourseId, FacultyId, Name) values (345, 5, 'Persistent homogeneous system engine');
insert into Course (CourseId, FacultyId, Name) values (346, 6, 'Networked dedicated extranet');
insert into Course (CourseId, FacultyId, Name) values (347, 7, 'Distributed leading edge knowledge base');
insert into Course (CourseId, FacultyId, Name) values (348, 8, 'Sharable interactive workforce');
insert into Course (CourseId, FacultyId, Name) values (349, 9, 'Realigned tangible firmware');
insert into Course (CourseId, FacultyId, Name) values (350, 10, 'Diverse systemic challenge');
insert into Course (CourseId, FacultyId, Name) values (351, 1, 'Monitored systemic architecture');
insert into Course (CourseId, FacultyId, Name) values (352, 2, 'Extended system-worthy model');
insert into Course (CourseId, FacultyId, Name) values (353, 3, 'Reactive needs-based concept');
insert into Course (CourseId, FacultyId, Name) values (354, 4, 'Compatible disintermediate analyzer');
insert into Course (CourseId, FacultyId, Name) values (355, 5, 'Open-architected zero defect forecast');
insert into Course (CourseId, FacultyId, Name) values (356, 6, 'Decentralized web-enabled function');
insert into Course (CourseId, FacultyId, Name) values (357, 7, 'Synchronised tangible success');
insert into Course (CourseId, FacultyId, Name) values (358, 8, 'Expanded neutral circuit');
insert into Course (CourseId, FacultyId, Name) values (359, 9, 'Visionary exuding ability');
insert into Course (CourseId, FacultyId, Name) values (360, 10, 'Function-based zero defect info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (361, 1, 'Synchronised grid-enabled complexity');
insert into Course (CourseId, FacultyId, Name) values (362, 2, 'Cloned eco-centric archive');
insert into Course (CourseId, FacultyId, Name) values (363, 3, 'Progressive tertiary flexibility');
insert into Course (CourseId, FacultyId, Name) values (364, 4, 'Fundamental local model');
insert into Course (CourseId, FacultyId, Name) values (365, 5, 'Centralized impactful website');
insert into Course (CourseId, FacultyId, Name) values (366, 6, 'Sharable clear-thinking strategy');
insert into Course (CourseId, FacultyId, Name) values (367, 7, 'Fundamental motivating middleware');
insert into Course (CourseId, FacultyId, Name) values (368, 8, 'Robust attitude-oriented project');
insert into Course (CourseId, FacultyId, Name) values (369, 9, 'Upgradable intermediate alliance');
insert into Course (CourseId, FacultyId, Name) values (370, 10, 'Synchronised demand-driven conglomeration');
insert into Course (CourseId, FacultyId, Name) values (371, 1, 'Decentralized mobile matrix');
insert into Course (CourseId, FacultyId, Name) values (372, 2, 'Optional high-level neural-net');
insert into Course (CourseId, FacultyId, Name) values (373, 3, 'Intuitive disintermediate service-desk');
insert into Course (CourseId, FacultyId, Name) values (374, 4, 'Re-engineered 24/7 internet solution');
insert into Course (CourseId, FacultyId, Name) values (375, 5, 'Implemented heuristic hub');
insert into Course (CourseId, FacultyId, Name) values (376, 6, 'Self-enabling multi-tasking moderator');
insert into Course (CourseId, FacultyId, Name) values (377, 7, 'Triple-buffered value-added attitude');
insert into Course (CourseId, FacultyId, Name) values (378, 8, 'Visionary motivating instruction set');
insert into Course (CourseId, FacultyId, Name) values (379, 9, 'Triple-buffered intangible migration');
insert into Course (CourseId, FacultyId, Name) values (380, 10, 'Fully-configurable stable moderator');
insert into Course (CourseId, FacultyId, Name) values (381, 1, 'De-engineered holistic encryption');
insert into Course (CourseId, FacultyId, Name) values (382, 2, 'Balanced modular access');
insert into Course (CourseId, FacultyId, Name) values (383, 3, 'Ergonomic zero tolerance capacity');
insert into Course (CourseId, FacultyId, Name) values (384, 4, 'Automated uniform middleware');
insert into Course (CourseId, FacultyId, Name) values (385, 5, 'Synergistic modular alliance');
insert into Course (CourseId, FacultyId, Name) values (386, 6, 'Customizable secondary circuit');
insert into Course (CourseId, FacultyId, Name) values (387, 7, 'Seamless tangible time-frame');
insert into Course (CourseId, FacultyId, Name) values (388, 8, 'Diverse global flexibility');
insert into Course (CourseId, FacultyId, Name) values (389, 9, 'Networked homogeneous budgetary management');
insert into Course (CourseId, FacultyId, Name) values (390, 10, 'Function-based methodical instruction set');
insert into Course (CourseId, FacultyId, Name) values (391, 1, 'Ergonomic actuating capacity');
insert into Course (CourseId, FacultyId, Name) values (392, 2, 'Devolved explicit workforce');
insert into Course (CourseId, FacultyId, Name) values (393, 3, 'Triple-buffered global functionalities');
insert into Course (CourseId, FacultyId, Name) values (394, 4, 'Upgradable real-time implementation');
insert into Course (CourseId, FacultyId, Name) values (395, 5, 'Stand-alone tertiary open system');
insert into Course (CourseId, FacultyId, Name) values (396, 6, 'Virtual bi-directional approach');
insert into Course (CourseId, FacultyId, Name) values (397, 7, 'Cross-group directional local area network');
insert into Course (CourseId, FacultyId, Name) values (398, 8, 'Realigned background functionalities');
insert into Course (CourseId, FacultyId, Name) values (399, 9, 'Vision-oriented 6th generation migration');
insert into Course (CourseId, FacultyId, Name) values (400, 10, 'Reactive directional matrix');
insert into Course (CourseId, FacultyId, Name) values (401, 1, 'Grass-roots web-enabled Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (402, 2, 'Stand-alone modular monitoring');
insert into Course (CourseId, FacultyId, Name) values (403, 3, 'User-friendly national function');
insert into Course (CourseId, FacultyId, Name) values (404, 4, 'Integrated directional hierarchy');
insert into Course (CourseId, FacultyId, Name) values (405, 5, 'Reduced full-range paradigm');
insert into Course (CourseId, FacultyId, Name) values (406, 6, 'Universal static open system');
insert into Course (CourseId, FacultyId, Name) values (407, 7, 'Centralized logistical ability');
insert into Course (CourseId, FacultyId, Name) values (408, 8, 'Seamless optimizing matrices');
insert into Course (CourseId, FacultyId, Name) values (409, 9, 'Focused web-enabled structure');
insert into Course (CourseId, FacultyId, Name) values (410, 10, 'Inverse asymmetric orchestration');
insert into Course (CourseId, FacultyId, Name) values (411, 1, 'Synergized modular policy');
insert into Course (CourseId, FacultyId, Name) values (412, 2, 'Stand-alone executive hierarchy');
insert into Course (CourseId, FacultyId, Name) values (413, 3, 'Multi-channelled content-based neural-net');
insert into Course (CourseId, FacultyId, Name) values (414, 4, 'Networked web-enabled application');
insert into Course (CourseId, FacultyId, Name) values (415, 5, 'Advanced hybrid analyzer');
insert into Course (CourseId, FacultyId, Name) values (416, 6, 'User-centric systemic structure');
insert into Course (CourseId, FacultyId, Name) values (417, 7, 'Ameliorated incremental projection');
insert into Course (CourseId, FacultyId, Name) values (418, 8, 'Implemented zero administration installation');
insert into Course (CourseId, FacultyId, Name) values (419, 9, 'Multi-layered bottom-line info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (420, 10, 'User-centric real-time core');
insert into Course (CourseId, FacultyId, Name) values (421, 1, 'Programmable background collaboration');
insert into Course (CourseId, FacultyId, Name) values (422, 2, 'Mandatory disintermediate conglomeration');
insert into Course (CourseId, FacultyId, Name) values (423, 3, 'Profound solution-oriented conglomeration');
insert into Course (CourseId, FacultyId, Name) values (424, 4, 'Reactive actuating forecast');
insert into Course (CourseId, FacultyId, Name) values (425, 5, 'Stand-alone interactive secured line');
insert into Course (CourseId, FacultyId, Name) values (426, 6, 'Secured background data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (427, 7, 'Face to face needs-based approach');
insert into Course (CourseId, FacultyId, Name) values (428, 8, 'Managed mission-critical implementation');
insert into Course (CourseId, FacultyId, Name) values (429, 9, 'Streamlined mobile knowledge base');
insert into Course (CourseId, FacultyId, Name) values (430, 10, 'Reverse-engineered multi-tasking collaboration');
insert into Course (CourseId, FacultyId, Name) values (431, 1, 'Organic bi-directional database');
insert into Course (CourseId, FacultyId, Name) values (432, 2, 'Streamlined eco-centric groupware');
insert into Course (CourseId, FacultyId, Name) values (433, 3, 'Configurable 24/7 leverage');
insert into Course (CourseId, FacultyId, Name) values (434, 4, 'Programmable hybrid software');
insert into Course (CourseId, FacultyId, Name) values (435, 5, 'Distributed contextually-based architecture');
insert into Course (CourseId, FacultyId, Name) values (436, 6, 'Operative stable instruction set');
insert into Course (CourseId, FacultyId, Name) values (437, 7, 'Face to face heuristic product');
insert into Course (CourseId, FacultyId, Name) values (438, 8, 'Progressive foreground concept');
insert into Course (CourseId, FacultyId, Name) values (439, 9, 'Balanced analyzing hardware');
insert into Course (CourseId, FacultyId, Name) values (440, 10, 'Triple-buffered bi-directional utilisation');
insert into Course (CourseId, FacultyId, Name) values (441, 1, 'Synchronised motivating throughput');
insert into Course (CourseId, FacultyId, Name) values (442, 2, 'Business-focused foreground firmware');
insert into Course (CourseId, FacultyId, Name) values (443, 3, 'Diverse secondary standardization');
insert into Course (CourseId, FacultyId, Name) values (444, 4, 'Upgradable empowering artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (445, 5, 'Quality-focused well-modulated functionalities');
insert into Course (CourseId, FacultyId, Name) values (446, 6, 'Streamlined bi-directional encoding');
insert into Course (CourseId, FacultyId, Name) values (447, 7, 'Assimilated non-volatile interface');
insert into Course (CourseId, FacultyId, Name) values (448, 8, 'Customer-focused responsive firmware');
insert into Course (CourseId, FacultyId, Name) values (449, 9, 'Versatile bifurcated matrix');
insert into Course (CourseId, FacultyId, Name) values (450, 10, 'Upgradable 3rd generation concept');
insert into Course (CourseId, FacultyId, Name) values (451, 1, 'Decentralized object-oriented interface');
insert into Course (CourseId, FacultyId, Name) values (452, 2, 'Monitored contextually-based groupware');
insert into Course (CourseId, FacultyId, Name) values (453, 3, 'Focused holistic hierarchy');
insert into Course (CourseId, FacultyId, Name) values (454, 4, 'Cross-platform empowering process improvement');
insert into Course (CourseId, FacultyId, Name) values (455, 5, 'Streamlined bandwidth-monitored alliance');
insert into Course (CourseId, FacultyId, Name) values (456, 6, 'Multi-layered multi-tasking open system');
insert into Course (CourseId, FacultyId, Name) values (457, 7, 'Progressive cohesive knowledge user');
insert into Course (CourseId, FacultyId, Name) values (458, 8, 'Upgradable full-range conglomeration');
insert into Course (CourseId, FacultyId, Name) values (459, 9, 'Vision-oriented analyzing definition');
insert into Course (CourseId, FacultyId, Name) values (460, 10, 'Team-oriented intangible core');
insert into Course (CourseId, FacultyId, Name) values (461, 1, 'Phased optimizing capability');
insert into Course (CourseId, FacultyId, Name) values (462, 2, 'Sharable clear-thinking budgetary management');
insert into Course (CourseId, FacultyId, Name) values (463, 3, 'Monitored analyzing array');
insert into Course (CourseId, FacultyId, Name) values (464, 4, 'Customer-focused directional Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (465, 5, 'Exclusive 5th generation contingency');
insert into Course (CourseId, FacultyId, Name) values (466, 6, 'Universal composite support');
insert into Course (CourseId, FacultyId, Name) values (467, 7, 'Pre-emptive user-facing website');
insert into Course (CourseId, FacultyId, Name) values (468, 8, 'Progressive client-server standardization');
insert into Course (CourseId, FacultyId, Name) values (469, 9, 'Quality-focused responsive moratorium');
insert into Course (CourseId, FacultyId, Name) values (470, 10, 'Fundamental asynchronous collaboration');
insert into Course (CourseId, FacultyId, Name) values (471, 1, 'Inverse dynamic hub');
insert into Course (CourseId, FacultyId, Name) values (472, 2, 'Multi-tiered motivating alliance');
insert into Course (CourseId, FacultyId, Name) values (473, 3, 'User-centric contextually-based functionalities');
insert into Course (CourseId, FacultyId, Name) values (474, 4, 'Diverse 24/7 policy');
insert into Course (CourseId, FacultyId, Name) values (475, 5, 'Diverse mobile solution');
insert into Course (CourseId, FacultyId, Name) values (476, 6, 'Stand-alone responsive secured line');
insert into Course (CourseId, FacultyId, Name) values (477, 7, 'Distributed bifurcated contingency');
insert into Course (CourseId, FacultyId, Name) values (478, 8, 'Virtual intermediate open system');
insert into Course (CourseId, FacultyId, Name) values (479, 9, 'Virtual real-time circuit');
insert into Course (CourseId, FacultyId, Name) values (480, 10, 'Multi-lateral upward-trending challenge');
insert into Course (CourseId, FacultyId, Name) values (481, 1, 'Re-contextualized coherent matrices');
insert into Course (CourseId, FacultyId, Name) values (482, 2, 'Visionary tertiary initiative');
insert into Course (CourseId, FacultyId, Name) values (483, 3, 'Centralized mission-critical hub');
insert into Course (CourseId, FacultyId, Name) values (484, 4, 'Horizontal scalable interface');
insert into Course (CourseId, FacultyId, Name) values (485, 5, 'Stand-alone mobile encoding');
insert into Course (CourseId, FacultyId, Name) values (486, 6, 'Expanded attitude-oriented software');
insert into Course (CourseId, FacultyId, Name) values (487, 7, 'Managed methodical emulation');
insert into Course (CourseId, FacultyId, Name) values (488, 8, 'Reactive asymmetric challenge');
insert into Course (CourseId, FacultyId, Name) values (489, 9, 'Automated analyzing flexibility');
insert into Course (CourseId, FacultyId, Name) values (490, 10, 'Configurable impactful data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (491, 1, 'Reactive intermediate artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (492, 2, 'Monitored zero tolerance frame');
insert into Course (CourseId, FacultyId, Name) values (493, 3, 'Multi-channelled incremental concept');
insert into Course (CourseId, FacultyId, Name) values (494, 4, 'Organized regional customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (495, 5, 'Programmable coherent customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (496, 6, 'Ergonomic national capability');
insert into Course (CourseId, FacultyId, Name) values (497, 7, 'Pre-emptive web-enabled secured line');
insert into Course (CourseId, FacultyId, Name) values (498, 8, 'Re-engineered neutral challenge');
insert into Course (CourseId, FacultyId, Name) values (499, 9, 'Optimized cohesive emulation');
insert into Course (CourseId, FacultyId, Name) values (500, 10, 'Visionary demand-driven matrix');
insert into Course (CourseId, FacultyId, Name) values (501, 1, 'Customizable radical time-frame');
insert into Course (CourseId, FacultyId, Name) values (502, 2, 'Digitized clear-thinking framework');
insert into Course (CourseId, FacultyId, Name) values (503, 3, 'Profound mission-critical encoding');
insert into Course (CourseId, FacultyId, Name) values (504, 4, 'Versatile zero administration secured line');
insert into Course (CourseId, FacultyId, Name) values (505, 5, 'Synchronised heuristic concept');
insert into Course (CourseId, FacultyId, Name) values (506, 6, 'Balanced bandwidth-monitored open system');
insert into Course (CourseId, FacultyId, Name) values (507, 7, 'Cross-group bifurcated flexibility');
insert into Course (CourseId, FacultyId, Name) values (508, 8, 'Optional 3rd generation policy');
insert into Course (CourseId, FacultyId, Name) values (509, 9, 'Seamless 6th generation parallelism');
insert into Course (CourseId, FacultyId, Name) values (510, 10, 'Team-oriented value-added neural-net');
insert into Course (CourseId, FacultyId, Name) values (511, 1, 'Function-based 6th generation instruction set');
insert into Course (CourseId, FacultyId, Name) values (512, 2, 'Streamlined tangible flexibility');
insert into Course (CourseId, FacultyId, Name) values (513, 3, 'Team-oriented cohesive local area network');
insert into Course (CourseId, FacultyId, Name) values (514, 4, 'Adaptive 6th generation migration');
insert into Course (CourseId, FacultyId, Name) values (515, 5, 'Proactive attitude-oriented initiative');
insert into Course (CourseId, FacultyId, Name) values (516, 6, 'Streamlined explicit encoding');
insert into Course (CourseId, FacultyId, Name) values (517, 7, 'Horizontal leading edge encryption');
insert into Course (CourseId, FacultyId, Name) values (518, 8, 'Customer-focused homogeneous flexibility');
insert into Course (CourseId, FacultyId, Name) values (519, 9, 'Synergized neutral framework');
insert into Course (CourseId, FacultyId, Name) values (520, 10, 'Compatible maximized internet solution');
insert into Course (CourseId, FacultyId, Name) values (521, 1, 'Diverse coherent process improvement');
insert into Course (CourseId, FacultyId, Name) values (522, 2, 'Progressive asymmetric standardization');
insert into Course (CourseId, FacultyId, Name) values (523, 3, 'Centralized contextually-based ability');
insert into Course (CourseId, FacultyId, Name) values (524, 4, 'Digitized 24 hour circuit');
insert into Course (CourseId, FacultyId, Name) values (525, 5, 'User-centric value-added open architecture');
insert into Course (CourseId, FacultyId, Name) values (526, 6, 'Function-based modular benchmark');
insert into Course (CourseId, FacultyId, Name) values (527, 7, 'Re-engineered tangible middleware');
insert into Course (CourseId, FacultyId, Name) values (528, 8, 'Reactive client-server website');
insert into Course (CourseId, FacultyId, Name) values (529, 9, 'Function-based human-resource pricing structure');
insert into Course (CourseId, FacultyId, Name) values (530, 10, 'Assimilated systematic success');
insert into Course (CourseId, FacultyId, Name) values (531, 1, 'Reactive mobile challenge');
insert into Course (CourseId, FacultyId, Name) values (532, 2, 'Re-contextualized directional model');
insert into Course (CourseId, FacultyId, Name) values (533, 3, 'Advanced impactful firmware');
insert into Course (CourseId, FacultyId, Name) values (534, 4, 'Right-sized radical encryption');
insert into Course (CourseId, FacultyId, Name) values (535, 5, 'Triple-buffered systematic synergy');
insert into Course (CourseId, FacultyId, Name) values (536, 6, 'Secured well-modulated moratorium');
insert into Course (CourseId, FacultyId, Name) values (537, 7, 'Balanced 3rd generation neural-net');
insert into Course (CourseId, FacultyId, Name) values (538, 8, 'Face to face uniform policy');
insert into Course (CourseId, FacultyId, Name) values (539, 9, 'Versatile eco-centric forecast');
insert into Course (CourseId, FacultyId, Name) values (540, 10, 'Integrated asynchronous attitude');
insert into Course (CourseId, FacultyId, Name) values (541, 1, 'Versatile radical array');
insert into Course (CourseId, FacultyId, Name) values (542, 2, 'Seamless well-modulated flexibility');
insert into Course (CourseId, FacultyId, Name) values (543, 3, 'Digitized encompassing orchestration');
insert into Course (CourseId, FacultyId, Name) values (544, 4, 'Self-enabling national encryption');
insert into Course (CourseId, FacultyId, Name) values (545, 5, 'Upgradable dynamic analyzer');
insert into Course (CourseId, FacultyId, Name) values (546, 6, 'Realigned eco-centric neural-net');
insert into Course (CourseId, FacultyId, Name) values (547, 7, 'Optional stable forecast');
insert into Course (CourseId, FacultyId, Name) values (548, 8, 'Multi-channelled maximized challenge');
insert into Course (CourseId, FacultyId, Name) values (549, 9, 'Customer-focused 24/7 strategy');
insert into Course (CourseId, FacultyId, Name) values (550, 10, 'Phased modular process improvement');
insert into Course (CourseId, FacultyId, Name) values (551, 1, 'Polarised upward-trending archive');
insert into Course (CourseId, FacultyId, Name) values (552, 2, 'Fundamental responsive alliance');
insert into Course (CourseId, FacultyId, Name) values (553, 3, 'Secured multi-tasking contingency');
insert into Course (CourseId, FacultyId, Name) values (554, 4, 'Proactive heuristic product');
insert into Course (CourseId, FacultyId, Name) values (555, 5, 'Profit-focused actuating application');
insert into Course (CourseId, FacultyId, Name) values (556, 6, 'Ergonomic disintermediate secured line');
insert into Course (CourseId, FacultyId, Name) values (557, 7, 'Extended actuating analyzer');
insert into Course (CourseId, FacultyId, Name) values (558, 8, 'Object-based modular database');
insert into Course (CourseId, FacultyId, Name) values (559, 9, 'Enhanced client-driven software');
insert into Course (CourseId, FacultyId, Name) values (560, 10, 'Open-source intermediate groupware');
insert into Course (CourseId, FacultyId, Name) values (561, 1, 'Secured national knowledge base');
insert into Course (CourseId, FacultyId, Name) values (562, 2, 'Cloned intermediate circuit');
insert into Course (CourseId, FacultyId, Name) values (563, 3, 'Synergized analyzing circuit');
insert into Course (CourseId, FacultyId, Name) values (564, 4, 'Triple-buffered coherent forecast');
insert into Course (CourseId, FacultyId, Name) values (565, 5, 'Visionary global solution');
insert into Course (CourseId, FacultyId, Name) values (566, 6, 'Versatile client-server hardware');
insert into Course (CourseId, FacultyId, Name) values (567, 7, 'Object-based zero tolerance groupware');
insert into Course (CourseId, FacultyId, Name) values (568, 8, 'Reverse-engineered responsive array');
insert into Course (CourseId, FacultyId, Name) values (569, 9, 'Enterprise-wide zero tolerance encoding');
insert into Course (CourseId, FacultyId, Name) values (570, 10, 'Ergonomic explicit benchmark');
insert into Course (CourseId, FacultyId, Name) values (571, 1, 'User-friendly secondary knowledge user');
insert into Course (CourseId, FacultyId, Name) values (572, 2, 'Distributed multi-tasking pricing structure');
insert into Course (CourseId, FacultyId, Name) values (573, 3, 'Customer-focused systematic secured line');
insert into Course (CourseId, FacultyId, Name) values (574, 4, 'Reverse-engineered didactic pricing structure');
insert into Course (CourseId, FacultyId, Name) values (575, 5, 'Function-based dedicated open architecture');
insert into Course (CourseId, FacultyId, Name) values (576, 6, 'Persevering analyzing framework');
insert into Course (CourseId, FacultyId, Name) values (577, 7, 'Implemented optimal collaboration');
insert into Course (CourseId, FacultyId, Name) values (578, 8, 'Innovative mobile capability');
insert into Course (CourseId, FacultyId, Name) values (579, 9, 'Synchronised optimal strategy');
insert into Course (CourseId, FacultyId, Name) values (580, 10, 'Re-contextualized zero tolerance budgetary management');
insert into Course (CourseId, FacultyId, Name) values (581, 1, 'Grass-roots upward-trending matrix');
insert into Course (CourseId, FacultyId, Name) values (582, 2, 'Object-based systematic attitude');
insert into Course (CourseId, FacultyId, Name) values (583, 3, 'Face to face multi-tasking function');
insert into Course (CourseId, FacultyId, Name) values (584, 4, 'Seamless explicit data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (585, 5, 'Networked mobile projection');
insert into Course (CourseId, FacultyId, Name) values (586, 6, 'Distributed national artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (587, 7, 'Profound web-enabled challenge');
insert into Course (CourseId, FacultyId, Name) values (588, 8, 'Pre-emptive coherent paradigm');
insert into Course (CourseId, FacultyId, Name) values (589, 9, 'Realigned actuating ability');
insert into Course (CourseId, FacultyId, Name) values (590, 10, 'User-centric systematic migration');
insert into Course (CourseId, FacultyId, Name) values (591, 1, 'Reverse-engineered needs-based workforce');
insert into Course (CourseId, FacultyId, Name) values (592, 2, 'Optimized bifurcated concept');
insert into Course (CourseId, FacultyId, Name) values (593, 3, 'Profound heuristic artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (594, 4, 'Persevering bottom-line structure');
insert into Course (CourseId, FacultyId, Name) values (595, 5, 'Implemented 5th generation functionalities');
insert into Course (CourseId, FacultyId, Name) values (596, 6, 'Proactive system-worthy methodology');
insert into Course (CourseId, FacultyId, Name) values (597, 7, 'Polarised 6th generation architecture');
insert into Course (CourseId, FacultyId, Name) values (598, 8, 'Innovative regional instruction set');
insert into Course (CourseId, FacultyId, Name) values (599, 9, 'Open-architected scalable workforce');
insert into Course (CourseId, FacultyId, Name) values (600, 10, 'Front-line composite matrix');
insert into Course (CourseId, FacultyId, Name) values (601, 1, 'Open-source demand-driven Graphic Interface');
insert into Course (CourseId, FacultyId, Name) values (602, 2, 'Monitored directional implementation');
insert into Course (CourseId, FacultyId, Name) values (603, 3, 'Expanded client-server frame');
insert into Course (CourseId, FacultyId, Name) values (604, 4, 'Customizable well-modulated orchestration');
insert into Course (CourseId, FacultyId, Name) values (605, 5, 'Enhanced solution-oriented analyzer');
insert into Course (CourseId, FacultyId, Name) values (606, 6, 'Compatible clear-thinking customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (607, 7, 'Configurable uniform process improvement');
insert into Course (CourseId, FacultyId, Name) values (608, 8, 'Digitized even-keeled middleware');
insert into Course (CourseId, FacultyId, Name) values (609, 9, 'Balanced context-sensitive extranet');
insert into Course (CourseId, FacultyId, Name) values (610, 10, 'Cloned zero tolerance task-force');
insert into Course (CourseId, FacultyId, Name) values (611, 1, 'Visionary homogeneous knowledge base');
insert into Course (CourseId, FacultyId, Name) values (612, 2, 'Open-source intermediate product');
insert into Course (CourseId, FacultyId, Name) values (613, 3, 'Function-based system-worthy pricing structure');
insert into Course (CourseId, FacultyId, Name) values (614, 4, 'Progressive multimedia encoding');
insert into Course (CourseId, FacultyId, Name) values (615, 5, 'Focused bottom-line data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (616, 6, 'Cross-platform secondary emulation');
insert into Course (CourseId, FacultyId, Name) values (617, 7, 'Streamlined web-enabled standardization');
insert into Course (CourseId, FacultyId, Name) values (618, 8, 'Multi-tiered 3rd generation utilisation');
insert into Course (CourseId, FacultyId, Name) values (619, 9, 'Object-based non-volatile framework');
insert into Course (CourseId, FacultyId, Name) values (620, 10, 'Cloned coherent application');
insert into Course (CourseId, FacultyId, Name) values (621, 1, 'Triple-buffered zero tolerance open system');
insert into Course (CourseId, FacultyId, Name) values (622, 2, 'Sharable optimal support');
insert into Course (CourseId, FacultyId, Name) values (623, 3, 'Right-sized next generation website');
insert into Course (CourseId, FacultyId, Name) values (624, 4, 'Persistent mission-critical frame');
insert into Course (CourseId, FacultyId, Name) values (625, 5, 'Team-oriented grid-enabled strategy');
insert into Course (CourseId, FacultyId, Name) values (626, 6, 'Diverse responsive success');
insert into Course (CourseId, FacultyId, Name) values (627, 7, 'Public-key methodical application');
insert into Course (CourseId, FacultyId, Name) values (628, 8, 'Progressive tertiary moderator');
insert into Course (CourseId, FacultyId, Name) values (629, 9, 'Innovative uniform success');
insert into Course (CourseId, FacultyId, Name) values (630, 10, 'Streamlined mission-critical algorithm');
insert into Course (CourseId, FacultyId, Name) values (631, 1, 'Cross-platform interactive emulation');
insert into Course (CourseId, FacultyId, Name) values (632, 2, 'Realigned eco-centric info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (633, 3, 'Team-oriented fresh-thinking workforce');
insert into Course (CourseId, FacultyId, Name) values (634, 4, 'Enterprise-wide directional complexity');
insert into Course (CourseId, FacultyId, Name) values (635, 5, 'De-engineered 6th generation customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (636, 6, 'Ameliorated upward-trending local area network');
insert into Course (CourseId, FacultyId, Name) values (637, 7, 'Horizontal motivating functionalities');
insert into Course (CourseId, FacultyId, Name) values (638, 8, 'Versatile systematic hub');
insert into Course (CourseId, FacultyId, Name) values (639, 9, 'Assimilated incremental hub');
insert into Course (CourseId, FacultyId, Name) values (640, 10, 'Up-sized bottom-line neural-net');
insert into Course (CourseId, FacultyId, Name) values (641, 1, 'Progressive encompassing hierarchy');
insert into Course (CourseId, FacultyId, Name) values (642, 2, 'Visionary cohesive portal');
insert into Course (CourseId, FacultyId, Name) values (643, 3, 'Mandatory dynamic monitoring');
insert into Course (CourseId, FacultyId, Name) values (644, 4, 'Adaptive incremental orchestration');
insert into Course (CourseId, FacultyId, Name) values (645, 5, 'Integrated upward-trending adapter');
insert into Course (CourseId, FacultyId, Name) values (646, 6, 'Persistent multi-state benchmark');
insert into Course (CourseId, FacultyId, Name) values (647, 7, 'Innovative uniform monitoring');
insert into Course (CourseId, FacultyId, Name) values (648, 8, 'Automated 5th generation website');
insert into Course (CourseId, FacultyId, Name) values (649, 9, 'Customer-focused bifurcated firmware');
insert into Course (CourseId, FacultyId, Name) values (650, 10, 'Front-line analyzing array');
insert into Course (CourseId, FacultyId, Name) values (651, 1, 'Upgradable high-level Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (652, 2, 'Organized multi-tasking concept');
insert into Course (CourseId, FacultyId, Name) values (653, 3, 'Open-source tangible contingency');
insert into Course (CourseId, FacultyId, Name) values (654, 4, 'Cross-platform systemic conglomeration');
insert into Course (CourseId, FacultyId, Name) values (655, 5, 'Polarised content-based knowledge base');
insert into Course (CourseId, FacultyId, Name) values (656, 6, 'Fundamental executive ability');
insert into Course (CourseId, FacultyId, Name) values (657, 7, 'Devolved system-worthy service-desk');
insert into Course (CourseId, FacultyId, Name) values (658, 8, 'Synergistic explicit flexibility');
insert into Course (CourseId, FacultyId, Name) values (659, 9, 'Programmable asynchronous initiative');
insert into Course (CourseId, FacultyId, Name) values (660, 10, 'Multi-layered even-keeled policy');
insert into Course (CourseId, FacultyId, Name) values (661, 1, 'Exclusive systematic function');
insert into Course (CourseId, FacultyId, Name) values (662, 2, 'Profit-focused demand-driven product');
insert into Course (CourseId, FacultyId, Name) values (663, 3, 'Reactive intangible hardware');
insert into Course (CourseId, FacultyId, Name) values (664, 4, 'Synchronised optimal complexity');
insert into Course (CourseId, FacultyId, Name) values (665, 5, 'Innovative bottom-line throughput');
insert into Course (CourseId, FacultyId, Name) values (666, 6, 'Robust zero tolerance capacity');
insert into Course (CourseId, FacultyId, Name) values (667, 7, 'Face to face human-resource open architecture');
insert into Course (CourseId, FacultyId, Name) values (668, 8, 'Seamless directional monitoring');
insert into Course (CourseId, FacultyId, Name) values (669, 9, 'Up-sized 4th generation initiative');
insert into Course (CourseId, FacultyId, Name) values (670, 10, 'Cross-platform upward-trending customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (671, 1, 'Secured motivating core');
insert into Course (CourseId, FacultyId, Name) values (672, 2, 'Synergistic dedicated matrices');
insert into Course (CourseId, FacultyId, Name) values (673, 3, 'Proactive leading edge alliance');
insert into Course (CourseId, FacultyId, Name) values (674, 4, 'Focused user-facing benchmark');
insert into Course (CourseId, FacultyId, Name) values (675, 5, 'User-centric attitude-oriented contingency');
insert into Course (CourseId, FacultyId, Name) values (676, 6, 'Object-based heuristic website');
insert into Course (CourseId, FacultyId, Name) values (677, 7, 'Expanded real-time workforce');
insert into Course (CourseId, FacultyId, Name) values (678, 8, 'Profound client-server workforce');
insert into Course (CourseId, FacultyId, Name) values (679, 9, 'Upgradable modular project');
insert into Course (CourseId, FacultyId, Name) values (680, 10, 'Self-enabling clear-thinking Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (681, 1, 'Stand-alone asynchronous synergy');
insert into Course (CourseId, FacultyId, Name) values (682, 2, 'Organized asymmetric software');
insert into Course (CourseId, FacultyId, Name) values (683, 3, 'Distributed multimedia info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (684, 4, 'Inverse context-sensitive neural-net');
insert into Course (CourseId, FacultyId, Name) values (685, 5, 'Fully-configurable regional adapter');
insert into Course (CourseId, FacultyId, Name) values (686, 6, 'Compatible uniform time-frame');
insert into Course (CourseId, FacultyId, Name) values (687, 7, 'Organized zero administration approach');
insert into Course (CourseId, FacultyId, Name) values (688, 8, 'Progressive client-driven orchestration');
insert into Course (CourseId, FacultyId, Name) values (689, 9, 'Vision-oriented multi-state solution');
insert into Course (CourseId, FacultyId, Name) values (690, 10, 'Stand-alone coherent attitude');
insert into Course (CourseId, FacultyId, Name) values (691, 1, 'Vision-oriented dynamic migration');
insert into Course (CourseId, FacultyId, Name) values (692, 2, 'Enhanced intermediate groupware');
insert into Course (CourseId, FacultyId, Name) values (693, 3, 'Object-based mission-critical focus group');
insert into Course (CourseId, FacultyId, Name) values (694, 4, 'Versatile multimedia neural-net');
insert into Course (CourseId, FacultyId, Name) values (695, 5, 'Open-source holistic benchmark');
insert into Course (CourseId, FacultyId, Name) values (696, 6, 'Reduced multimedia time-frame');
insert into Course (CourseId, FacultyId, Name) values (697, 7, 'Multi-channelled disintermediate policy');
insert into Course (CourseId, FacultyId, Name) values (698, 8, 'Object-based secondary collaboration');
insert into Course (CourseId, FacultyId, Name) values (699, 9, 'Reverse-engineered multimedia concept');
insert into Course (CourseId, FacultyId, Name) values (700, 10, 'Quality-focused fresh-thinking moderator');
insert into Course (CourseId, FacultyId, Name) values (701, 1, 'Business-focused actuating hierarchy');
insert into Course (CourseId, FacultyId, Name) values (702, 2, 'Total multimedia intranet');
insert into Course (CourseId, FacultyId, Name) values (703, 3, 'Ameliorated bi-directional installation');
insert into Course (CourseId, FacultyId, Name) values (704, 4, 'User-centric user-facing instruction set');
insert into Course (CourseId, FacultyId, Name) values (705, 5, 'Balanced multimedia software');
insert into Course (CourseId, FacultyId, Name) values (706, 6, 'Mandatory secondary knowledge base');
insert into Course (CourseId, FacultyId, Name) values (707, 7, 'Reverse-engineered zero administration leverage');
insert into Course (CourseId, FacultyId, Name) values (708, 8, 'Networked neutral budgetary management');
insert into Course (CourseId, FacultyId, Name) values (709, 9, 'Secured full-range structure');
insert into Course (CourseId, FacultyId, Name) values (710, 10, 'Switchable optimal hub');
insert into Course (CourseId, FacultyId, Name) values (711, 1, 'Adaptive grid-enabled neural-net');
insert into Course (CourseId, FacultyId, Name) values (712, 2, 'Centralized scalable local area network');
insert into Course (CourseId, FacultyId, Name) values (713, 3, 'Multi-layered well-modulated knowledge base');
insert into Course (CourseId, FacultyId, Name) values (714, 4, 'Innovative object-oriented challenge');
insert into Course (CourseId, FacultyId, Name) values (715, 5, 'Vision-oriented regional projection');
insert into Course (CourseId, FacultyId, Name) values (716, 6, 'Expanded national emulation');
insert into Course (CourseId, FacultyId, Name) values (717, 7, 'Polarised leading edge encryption');
insert into Course (CourseId, FacultyId, Name) values (718, 8, 'Horizontal analyzing knowledge user');
insert into Course (CourseId, FacultyId, Name) values (719, 9, 'Advanced fresh-thinking synergy');
insert into Course (CourseId, FacultyId, Name) values (720, 10, 'Universal radical groupware');
insert into Course (CourseId, FacultyId, Name) values (721, 1, 'Monitored motivating projection');
insert into Course (CourseId, FacultyId, Name) values (722, 2, 'Proactive user-facing contingency');
insert into Course (CourseId, FacultyId, Name) values (723, 3, 'Multi-channelled zero defect functionalities');
insert into Course (CourseId, FacultyId, Name) values (724, 4, 'Ergonomic multi-state artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (725, 5, 'Balanced foreground projection');
insert into Course (CourseId, FacultyId, Name) values (726, 6, 'Open-source real-time application');
insert into Course (CourseId, FacultyId, Name) values (727, 7, 'Innovative coherent algorithm');
insert into Course (CourseId, FacultyId, Name) values (728, 8, 'Monitored fresh-thinking implementation');
insert into Course (CourseId, FacultyId, Name) values (729, 9, 'Triple-buffered fault-tolerant hub');
insert into Course (CourseId, FacultyId, Name) values (730, 10, 'Public-key next generation structure');
insert into Course (CourseId, FacultyId, Name) values (731, 1, 'Virtual heuristic definition');
insert into Course (CourseId, FacultyId, Name) values (732, 2, 'Optimized needs-based encoding');
insert into Course (CourseId, FacultyId, Name) values (733, 3, 'Digitized directional policy');
insert into Course (CourseId, FacultyId, Name) values (734, 4, 'Distributed logistical protocol');
insert into Course (CourseId, FacultyId, Name) values (735, 5, 'Enhanced executive budgetary management');
insert into Course (CourseId, FacultyId, Name) values (736, 6, 'Compatible bottom-line help-desk');
insert into Course (CourseId, FacultyId, Name) values (737, 7, 'Proactive impactful initiative');
insert into Course (CourseId, FacultyId, Name) values (738, 8, 'Configurable attitude-oriented secured line');
insert into Course (CourseId, FacultyId, Name) values (739, 9, 'Public-key regional knowledge user');
insert into Course (CourseId, FacultyId, Name) values (740, 10, 'Decentralized bifurcated framework');
insert into Course (CourseId, FacultyId, Name) values (741, 1, 'Reverse-engineered grid-enabled functionalities');
insert into Course (CourseId, FacultyId, Name) values (742, 2, 'Realigned solution-oriented interface');
insert into Course (CourseId, FacultyId, Name) values (743, 3, 'Robust intermediate alliance');
insert into Course (CourseId, FacultyId, Name) values (744, 4, 'Enterprise-wide homogeneous structure');
insert into Course (CourseId, FacultyId, Name) values (745, 5, 'Profit-focused bottom-line policy');
insert into Course (CourseId, FacultyId, Name) values (746, 6, 'Self-enabling eco-centric forecast');
insert into Course (CourseId, FacultyId, Name) values (747, 7, 'Triple-buffered needs-based product');
insert into Course (CourseId, FacultyId, Name) values (748, 8, 'Multi-tiered multimedia infrastructure');
insert into Course (CourseId, FacultyId, Name) values (749, 9, 'Horizontal exuding customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (750, 10, 'Customer-focused eco-centric budgetary management');
insert into Course (CourseId, FacultyId, Name) values (751, 1, 'Distributed coherent structure');
insert into Course (CourseId, FacultyId, Name) values (752, 2, 'Integrated 5th generation interface');
insert into Course (CourseId, FacultyId, Name) values (753, 3, 'Front-line impactful pricing structure');
insert into Course (CourseId, FacultyId, Name) values (754, 4, 'Configurable stable forecast');
insert into Course (CourseId, FacultyId, Name) values (755, 5, 'Decentralized neutral firmware');
insert into Course (CourseId, FacultyId, Name) values (756, 6, 'Managed human-resource hub');
insert into Course (CourseId, FacultyId, Name) values (757, 7, 'Adaptive human-resource algorithm');
insert into Course (CourseId, FacultyId, Name) values (758, 8, 'Ameliorated incremental protocol');
insert into Course (CourseId, FacultyId, Name) values (759, 9, 'User-friendly well-modulated productivity');
insert into Course (CourseId, FacultyId, Name) values (760, 10, 'Adaptive bi-directional matrix');
insert into Course (CourseId, FacultyId, Name) values (761, 1, 'Reduced grid-enabled archive');
insert into Course (CourseId, FacultyId, Name) values (762, 2, 'Cross-group system-worthy task-force');
insert into Course (CourseId, FacultyId, Name) values (763, 3, 'Exclusive analyzing approach');
insert into Course (CourseId, FacultyId, Name) values (764, 4, 'Quality-focused tangible protocol');
insert into Course (CourseId, FacultyId, Name) values (765, 5, 'Decentralized next generation application');
insert into Course (CourseId, FacultyId, Name) values (766, 6, 'Organic cohesive forecast');
insert into Course (CourseId, FacultyId, Name) values (767, 7, 'Sharable even-keeled encoding');
insert into Course (CourseId, FacultyId, Name) values (768, 8, 'Sharable static matrices');
insert into Course (CourseId, FacultyId, Name) values (769, 9, 'Organic intermediate solution');
insert into Course (CourseId, FacultyId, Name) values (770, 10, 'Ameliorated attitude-oriented definition');
insert into Course (CourseId, FacultyId, Name) values (771, 1, 'Configurable background firmware');
insert into Course (CourseId, FacultyId, Name) values (772, 2, 'Horizontal 24/7 matrices');
insert into Course (CourseId, FacultyId, Name) values (773, 3, 'Mandatory zero tolerance hierarchy');
insert into Course (CourseId, FacultyId, Name) values (774, 4, 'Versatile motivating open system');
insert into Course (CourseId, FacultyId, Name) values (775, 5, 'Quality-focused 6th generation collaboration');
insert into Course (CourseId, FacultyId, Name) values (776, 6, 'Centralized attitude-oriented groupware');
insert into Course (CourseId, FacultyId, Name) values (777, 7, 'Configurable 5th generation toolset');
insert into Course (CourseId, FacultyId, Name) values (778, 8, 'Business-focused composite frame');
insert into Course (CourseId, FacultyId, Name) values (779, 9, 'Innovative dedicated synergy');
insert into Course (CourseId, FacultyId, Name) values (780, 10, 'Ergonomic needs-based time-frame');
insert into Course (CourseId, FacultyId, Name) values (781, 1, 'Automated stable workforce');
insert into Course (CourseId, FacultyId, Name) values (782, 2, 'Intuitive well-modulated artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (783, 3, 'Reverse-engineered client-server standardization');
insert into Course (CourseId, FacultyId, Name) values (784, 4, 'Open-architected motivating product');
insert into Course (CourseId, FacultyId, Name) values (785, 5, 'Inverse responsive local area network');
insert into Course (CourseId, FacultyId, Name) values (786, 6, 'Expanded content-based leverage');
insert into Course (CourseId, FacultyId, Name) values (787, 7, 'Exclusive 6th generation architecture');
insert into Course (CourseId, FacultyId, Name) values (788, 8, 'Synergized interactive architecture');
insert into Course (CourseId, FacultyId, Name) values (789, 9, 'Customizable motivating monitoring');
insert into Course (CourseId, FacultyId, Name) values (790, 10, 'Customer-focused hybrid attitude');
insert into Course (CourseId, FacultyId, Name) values (791, 1, 'Function-based demand-driven frame');
insert into Course (CourseId, FacultyId, Name) values (792, 2, 'Networked needs-based groupware');
insert into Course (CourseId, FacultyId, Name) values (793, 3, 'Team-oriented human-resource service-desk');
insert into Course (CourseId, FacultyId, Name) values (794, 4, 'Organic background concept');
insert into Course (CourseId, FacultyId, Name) values (795, 5, 'Profound motivating concept');
insert into Course (CourseId, FacultyId, Name) values (796, 6, 'Synergized mobile analyzer');
insert into Course (CourseId, FacultyId, Name) values (797, 7, 'Digitized needs-based capacity');
insert into Course (CourseId, FacultyId, Name) values (798, 8, 'Sharable system-worthy definition');
insert into Course (CourseId, FacultyId, Name) values (799, 9, 'Operative asynchronous alliance');
insert into Course (CourseId, FacultyId, Name) values (800, 10, 'Distributed radical adapter');
insert into Course (CourseId, FacultyId, Name) values (801, 1, 'Team-oriented empowering installation');
insert into Course (CourseId, FacultyId, Name) values (802, 2, 'Synchronised systematic success');
insert into Course (CourseId, FacultyId, Name) values (803, 3, 'Ameliorated object-oriented strategy');
insert into Course (CourseId, FacultyId, Name) values (804, 4, 'Pre-emptive cohesive array');
insert into Course (CourseId, FacultyId, Name) values (805, 5, 'Seamless value-added forecast');
insert into Course (CourseId, FacultyId, Name) values (806, 6, 'Versatile dedicated internet solution');
insert into Course (CourseId, FacultyId, Name) values (807, 7, 'Upgradable bifurcated open system');
insert into Course (CourseId, FacultyId, Name) values (808, 8, 'Vision-oriented leading edge forecast');
insert into Course (CourseId, FacultyId, Name) values (809, 9, 'Digitized needs-based contingency');
insert into Course (CourseId, FacultyId, Name) values (810, 10, 'Proactive leading edge matrices');
insert into Course (CourseId, FacultyId, Name) values (811, 1, 'Devolved upward-trending array');
insert into Course (CourseId, FacultyId, Name) values (812, 2, 'Horizontal object-oriented protocol');
insert into Course (CourseId, FacultyId, Name) values (813, 3, 'Implemented dedicated local area network');
insert into Course (CourseId, FacultyId, Name) values (814, 4, 'Switchable regional superstructure');
insert into Course (CourseId, FacultyId, Name) values (815, 5, 'Inverse next generation artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (816, 6, 'Integrated uniform database');
insert into Course (CourseId, FacultyId, Name) values (817, 7, 'Mandatory national secured line');
insert into Course (CourseId, FacultyId, Name) values (818, 8, 'Assimilated solution-oriented benchmark');
insert into Course (CourseId, FacultyId, Name) values (819, 9, 'Grass-roots leading edge customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (820, 10, 'Programmable optimal support');
insert into Course (CourseId, FacultyId, Name) values (821, 1, 'Robust impactful circuit');
insert into Course (CourseId, FacultyId, Name) values (822, 2, 'Re-contextualized even-keeled frame');
insert into Course (CourseId, FacultyId, Name) values (823, 3, 'Persevering fresh-thinking infrastructure');
insert into Course (CourseId, FacultyId, Name) values (824, 4, 'Ameliorated scalable structure');
insert into Course (CourseId, FacultyId, Name) values (825, 5, 'Advanced client-server array');
insert into Course (CourseId, FacultyId, Name) values (826, 6, 'Synergistic regional frame');
insert into Course (CourseId, FacultyId, Name) values (827, 7, 'Universal encompassing structure');
insert into Course (CourseId, FacultyId, Name) values (828, 8, 'Fully-configurable methodical encryption');
insert into Course (CourseId, FacultyId, Name) values (829, 9, 'Exclusive non-volatile standardization');
insert into Course (CourseId, FacultyId, Name) values (830, 10, 'Virtual transitional extranet');
insert into Course (CourseId, FacultyId, Name) values (831, 1, 'Monitored clear-thinking initiative');
insert into Course (CourseId, FacultyId, Name) values (832, 2, 'Managed didactic instruction set');
insert into Course (CourseId, FacultyId, Name) values (833, 3, 'Stand-alone demand-driven protocol');
insert into Course (CourseId, FacultyId, Name) values (834, 4, 'Assimilated cohesive contingency');
insert into Course (CourseId, FacultyId, Name) values (835, 5, 'Horizontal cohesive model');
insert into Course (CourseId, FacultyId, Name) values (836, 6, 'Horizontal attitude-oriented structure');
insert into Course (CourseId, FacultyId, Name) values (837, 7, 'Diverse dynamic conglomeration');
insert into Course (CourseId, FacultyId, Name) values (838, 8, 'Realigned local contingency');
insert into Course (CourseId, FacultyId, Name) values (839, 9, 'Cross-group static website');
insert into Course (CourseId, FacultyId, Name) values (840, 10, 'Streamlined uniform encoding');
insert into Course (CourseId, FacultyId, Name) values (841, 1, 'User-centric 3rd generation framework');
insert into Course (CourseId, FacultyId, Name) values (842, 2, 'Profit-focused bi-directional infrastructure');
insert into Course (CourseId, FacultyId, Name) values (843, 3, 'Versatile upward-trending superstructure');
insert into Course (CourseId, FacultyId, Name) values (844, 4, 'Persistent fault-tolerant firmware');
insert into Course (CourseId, FacultyId, Name) values (845, 5, 'Self-enabling real-time pricing structure');
insert into Course (CourseId, FacultyId, Name) values (846, 6, 'Centralized web-enabled methodology');
insert into Course (CourseId, FacultyId, Name) values (847, 7, 'Multi-lateral multimedia complexity');
insert into Course (CourseId, FacultyId, Name) values (848, 8, 'Cloned fresh-thinking access');
insert into Course (CourseId, FacultyId, Name) values (849, 9, 'Switchable client-driven moratorium');
insert into Course (CourseId, FacultyId, Name) values (850, 10, 'Total zero defect groupware');
insert into Course (CourseId, FacultyId, Name) values (851, 1, 'Configurable didactic throughput');
insert into Course (CourseId, FacultyId, Name) values (852, 2, 'Front-line multi-tasking time-frame');
insert into Course (CourseId, FacultyId, Name) values (853, 3, 'Compatible exuding service-desk');
insert into Course (CourseId, FacultyId, Name) values (854, 4, 'Fundamental 5th generation time-frame');
insert into Course (CourseId, FacultyId, Name) values (855, 5, 'Realigned executive portal');
insert into Course (CourseId, FacultyId, Name) values (856, 6, 'Universal explicit data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (857, 7, 'Ergonomic systemic secured line');
insert into Course (CourseId, FacultyId, Name) values (858, 8, 'De-engineered homogeneous middleware');
insert into Course (CourseId, FacultyId, Name) values (859, 9, 'Visionary discrete Graphic Interface');
insert into Course (CourseId, FacultyId, Name) values (860, 10, 'Cloned full-range hierarchy');
insert into Course (CourseId, FacultyId, Name) values (861, 1, 'Ergonomic real-time capacity');
insert into Course (CourseId, FacultyId, Name) values (862, 2, 'Triple-buffered analyzing contingency');
insert into Course (CourseId, FacultyId, Name) values (863, 3, 'Reverse-engineered interactive installation');
insert into Course (CourseId, FacultyId, Name) values (864, 4, 'Upgradable intermediate firmware');
insert into Course (CourseId, FacultyId, Name) values (865, 5, 'Sharable clear-thinking frame');
insert into Course (CourseId, FacultyId, Name) values (866, 6, 'Stand-alone tertiary hub');
insert into Course (CourseId, FacultyId, Name) values (867, 7, 'User-centric systematic throughput');
insert into Course (CourseId, FacultyId, Name) values (868, 8, 'Persevering secondary projection');
insert into Course (CourseId, FacultyId, Name) values (869, 9, 'Inverse disintermediate algorithm');
insert into Course (CourseId, FacultyId, Name) values (870, 10, 'Exclusive web-enabled data-warehouse');
insert into Course (CourseId, FacultyId, Name) values (871, 1, 'Business-focused cohesive policy');
insert into Course (CourseId, FacultyId, Name) values (872, 2, 'Distributed context-sensitive conglomeration');
insert into Course (CourseId, FacultyId, Name) values (873, 3, 'Exclusive scalable groupware');
insert into Course (CourseId, FacultyId, Name) values (874, 4, 'Monitored explicit circuit');
insert into Course (CourseId, FacultyId, Name) values (875, 5, 'Business-focused even-keeled success');
insert into Course (CourseId, FacultyId, Name) values (876, 6, 'Persevering non-volatile conglomeration');
insert into Course (CourseId, FacultyId, Name) values (877, 7, 'Stand-alone asynchronous encryption');
insert into Course (CourseId, FacultyId, Name) values (878, 8, 'User-friendly stable circuit');
insert into Course (CourseId, FacultyId, Name) values (879, 9, 'Compatible even-keeled budgetary management');
insert into Course (CourseId, FacultyId, Name) values (880, 10, 'Innovative web-enabled matrices');
insert into Course (CourseId, FacultyId, Name) values (881, 1, 'Future-proofed multimedia open architecture');
insert into Course (CourseId, FacultyId, Name) values (882, 2, 'Reduced executive functionalities');
insert into Course (CourseId, FacultyId, Name) values (883, 3, 'Open-source well-modulated forecast');
insert into Course (CourseId, FacultyId, Name) values (884, 4, 'Enhanced bandwidth-monitored forecast');
insert into Course (CourseId, FacultyId, Name) values (885, 5, 'Persevering coherent artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (886, 6, 'Ergonomic disintermediate moratorium');
insert into Course (CourseId, FacultyId, Name) values (887, 7, 'Distributed exuding workforce');
insert into Course (CourseId, FacultyId, Name) values (888, 8, 'Switchable global migration');
insert into Course (CourseId, FacultyId, Name) values (889, 9, 'Customizable bifurcated parallelism');
insert into Course (CourseId, FacultyId, Name) values (890, 10, 'Visionary stable forecast');
insert into Course (CourseId, FacultyId, Name) values (891, 1, 'Profound uniform Graphical User Interface');
insert into Course (CourseId, FacultyId, Name) values (892, 2, 'Integrated real-time projection');
insert into Course (CourseId, FacultyId, Name) values (893, 3, 'De-engineered demand-driven process improvement');
insert into Course (CourseId, FacultyId, Name) values (894, 4, 'Ameliorated global alliance');
insert into Course (CourseId, FacultyId, Name) values (895, 5, 'Secured eco-centric collaboration');
insert into Course (CourseId, FacultyId, Name) values (896, 6, 'Multi-channelled encompassing info-mediaries');
insert into Course (CourseId, FacultyId, Name) values (897, 7, 'Profound 24/7 moderator');
insert into Course (CourseId, FacultyId, Name) values (898, 8, 'Future-proofed coherent software');
insert into Course (CourseId, FacultyId, Name) values (899, 9, 'Up-sized dedicated strategy');
insert into Course (CourseId, FacultyId, Name) values (900, 10, 'Persistent logistical standardization');
insert into Course (CourseId, FacultyId, Name) values (901, 1, 'Horizontal clear-thinking moderator');
insert into Course (CourseId, FacultyId, Name) values (902, 2, 'Optional transitional product');
insert into Course (CourseId, FacultyId, Name) values (903, 3, 'Cross-group impactful framework');
insert into Course (CourseId, FacultyId, Name) values (904, 4, 'Compatible client-driven installation');
insert into Course (CourseId, FacultyId, Name) values (905, 5, 'Diverse maximized access');
insert into Course (CourseId, FacultyId, Name) values (906, 6, 'Realigned global flexibility');
insert into Course (CourseId, FacultyId, Name) values (907, 7, 'Switchable 6th generation migration');
insert into Course (CourseId, FacultyId, Name) values (908, 8, 'Programmable uniform customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (909, 9, 'Progressive well-modulated Graphic Interface');
insert into Course (CourseId, FacultyId, Name) values (910, 10, 'Focused modular architecture');
insert into Course (CourseId, FacultyId, Name) values (911, 1, 'Open-source asynchronous encryption');
insert into Course (CourseId, FacultyId, Name) values (912, 2, 'Proactive 3rd generation throughput');
insert into Course (CourseId, FacultyId, Name) values (913, 3, 'Quality-focused reciprocal product');
insert into Course (CourseId, FacultyId, Name) values (914, 4, 'Pre-emptive encompassing collaboration');
insert into Course (CourseId, FacultyId, Name) values (915, 5, 'Focused transitional neural-net');
insert into Course (CourseId, FacultyId, Name) values (916, 6, 'Versatile logistical matrices');
insert into Course (CourseId, FacultyId, Name) values (917, 7, 'Implemented optimal database');
insert into Course (CourseId, FacultyId, Name) values (918, 8, 'Multi-lateral object-oriented model');
insert into Course (CourseId, FacultyId, Name) values (919, 9, 'Persevering foreground encoding');
insert into Course (CourseId, FacultyId, Name) values (920, 10, 'Streamlined optimizing contingency');
insert into Course (CourseId, FacultyId, Name) values (921, 1, 'Self-enabling clear-thinking superstructure');
insert into Course (CourseId, FacultyId, Name) values (922, 2, 'Operative content-based function');
insert into Course (CourseId, FacultyId, Name) values (923, 3, 'Focused non-volatile solution');
insert into Course (CourseId, FacultyId, Name) values (924, 4, 'Implemented web-enabled knowledge base');
insert into Course (CourseId, FacultyId, Name) values (925, 5, 'Self-enabling 6th generation secured line');
insert into Course (CourseId, FacultyId, Name) values (926, 6, 'Pre-emptive human-resource monitoring');
insert into Course (CourseId, FacultyId, Name) values (927, 7, 'Multi-channelled 4th generation orchestration');
insert into Course (CourseId, FacultyId, Name) values (928, 8, 'Up-sized hybrid groupware');
insert into Course (CourseId, FacultyId, Name) values (929, 9, 'Progressive optimal core');
insert into Course (CourseId, FacultyId, Name) values (930, 10, 'Phased systematic solution');
insert into Course (CourseId, FacultyId, Name) values (931, 1, 'Compatible homogeneous toolset');
insert into Course (CourseId, FacultyId, Name) values (932, 2, 'Open-source eco-centric pricing structure');
insert into Course (CourseId, FacultyId, Name) values (933, 3, 'Reverse-engineered 6th generation algorithm');
insert into Course (CourseId, FacultyId, Name) values (934, 4, 'Programmable motivating benchmark');
insert into Course (CourseId, FacultyId, Name) values (935, 5, 'Realigned demand-driven software');
insert into Course (CourseId, FacultyId, Name) values (936, 6, 'Fully-configurable real-time knowledge user');
insert into Course (CourseId, FacultyId, Name) values (937, 7, 'Open-source systematic flexibility');
insert into Course (CourseId, FacultyId, Name) values (938, 8, 'Integrated holistic ability');
insert into Course (CourseId, FacultyId, Name) values (939, 9, 'Centralized radical forecast');
insert into Course (CourseId, FacultyId, Name) values (940, 10, 'Virtual transitional access');
insert into Course (CourseId, FacultyId, Name) values (941, 1, 'Secured incremental service-desk');
insert into Course (CourseId, FacultyId, Name) values (942, 2, 'Team-oriented bifurcated ability');
insert into Course (CourseId, FacultyId, Name) values (943, 3, 'Self-enabling dedicated intranet');
insert into Course (CourseId, FacultyId, Name) values (944, 4, 'Assimilated discrete protocol');
insert into Course (CourseId, FacultyId, Name) values (945, 5, 'Centralized explicit hierarchy');
insert into Course (CourseId, FacultyId, Name) values (946, 6, 'Triple-buffered real-time toolset');
insert into Course (CourseId, FacultyId, Name) values (947, 7, 'Re-contextualized system-worthy firmware');
insert into Course (CourseId, FacultyId, Name) values (948, 8, 'Switchable non-volatile focus group');
insert into Course (CourseId, FacultyId, Name) values (949, 9, 'Switchable local encoding');
insert into Course (CourseId, FacultyId, Name) values (950, 10, 'Multi-channelled global website');
insert into Course (CourseId, FacultyId, Name) values (951, 1, 'Stand-alone multimedia process improvement');
insert into Course (CourseId, FacultyId, Name) values (952, 2, 'Multi-lateral coherent instruction set');
insert into Course (CourseId, FacultyId, Name) values (953, 3, 'Diverse hybrid middleware');
insert into Course (CourseId, FacultyId, Name) values (954, 4, 'Diverse zero defect definition');
insert into Course (CourseId, FacultyId, Name) values (955, 5, 'Cross-platform bi-directional middleware');
insert into Course (CourseId, FacultyId, Name) values (956, 6, 'Reactive optimizing focus group');
insert into Course (CourseId, FacultyId, Name) values (957, 7, 'Programmable disintermediate internet solution');
insert into Course (CourseId, FacultyId, Name) values (958, 8, 'Streamlined stable instruction set');
insert into Course (CourseId, FacultyId, Name) values (959, 9, 'Ameliorated stable artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (960, 10, 'Polarised systematic superstructure');
insert into Course (CourseId, FacultyId, Name) values (961, 1, 'Organic zero administration open architecture');
insert into Course (CourseId, FacultyId, Name) values (962, 2, 'Open-architected discrete circuit');
insert into Course (CourseId, FacultyId, Name) values (963, 3, 'Diverse scalable strategy');
insert into Course (CourseId, FacultyId, Name) values (964, 4, 'Advanced well-modulated customer loyalty');
insert into Course (CourseId, FacultyId, Name) values (965, 5, 'Virtual optimizing adapter');
insert into Course (CourseId, FacultyId, Name) values (966, 6, 'Multi-tiered encompassing moratorium');
insert into Course (CourseId, FacultyId, Name) values (967, 7, 'Realigned impactful complexity');
insert into Course (CourseId, FacultyId, Name) values (968, 8, 'Down-sized 24 hour collaboration');
insert into Course (CourseId, FacultyId, Name) values (969, 9, 'Reduced multi-state array');
insert into Course (CourseId, FacultyId, Name) values (970, 10, 'Inverse well-modulated array');
insert into Course (CourseId, FacultyId, Name) values (971, 1, 'Self-enabling high-level superstructure');
insert into Course (CourseId, FacultyId, Name) values (972, 2, 'Digitized modular software');
insert into Course (CourseId, FacultyId, Name) values (973, 3, 'Synergized mobile concept');
insert into Course (CourseId, FacultyId, Name) values (974, 4, 'Team-oriented optimizing success');
insert into Course (CourseId, FacultyId, Name) values (975, 5, 'Advanced motivating application');
insert into Course (CourseId, FacultyId, Name) values (976, 6, 'Mandatory modular challenge');
insert into Course (CourseId, FacultyId, Name) values (977, 7, 'Intuitive tangible access');
insert into Course (CourseId, FacultyId, Name) values (978, 8, 'Multi-lateral foreground artificial intelligence');
insert into Course (CourseId, FacultyId, Name) values (979, 9, 'Expanded bi-directional concept');
insert into Course (CourseId, FacultyId, Name) values (980, 10, 'Face to face actuating throughput');
insert into Course (CourseId, FacultyId, Name) values (981, 1, 'Devolved cohesive flexibility');
insert into Course (CourseId, FacultyId, Name) values (982, 2, 'Expanded directional model');
insert into Course (CourseId, FacultyId, Name) values (983, 3, 'Devolved systematic hub');
insert into Course (CourseId, FacultyId, Name) values (984, 4, 'Proactive hybrid monitoring');
insert into Course (CourseId, FacultyId, Name) values (985, 5, 'Integrated client-server local area network');
insert into Course (CourseId, FacultyId, Name) values (986, 6, 'Upgradable needs-based knowledge user');
insert into Course (CourseId, FacultyId, Name) values (987, 7, 'Enhanced regional policy');
insert into Course (CourseId, FacultyId, Name) values (988, 8, 'Multi-lateral local utilisation');
insert into Course (CourseId, FacultyId, Name) values (989, 9, 'Horizontal client-server definition');
insert into Course (CourseId, FacultyId, Name) values (990, 10, 'Multi-lateral solution-oriented help-desk');
insert into Course (CourseId, FacultyId, Name) values (991, 1, 'Seamless heuristic secured line');
insert into Course (CourseId, FacultyId, Name) values (992, 2, 'Organized zero administration matrix');
insert into Course (CourseId, FacultyId, Name) values (993, 3, 'Multi-tiered asymmetric function');
insert into Course (CourseId, FacultyId, Name) values (994, 4, 'Managed bi-directional matrices');
insert into Course (CourseId, FacultyId, Name) values (995, 5, 'Fundamental neutral monitoring');
insert into Course (CourseId, FacultyId, Name) values (996, 6, 'Pre-emptive zero tolerance budgetary management');
insert into Course (CourseId, FacultyId, Name) values (997, 7, 'Visionary multi-tasking contingency');
insert into Course (CourseId, FacultyId, Name) values (998, 8, 'Reduced next generation middleware');
insert into Course (CourseId, FacultyId, Name) values (999, 9, 'Virtual radical solution');
insert into Course (CourseId, FacultyId, Name) values (1000, 10, 'Advanced multi-tasking time-frame');
set identity_insert course off

set identity_insert student on
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (1, 1, 1, 'Giselbert', 'Keune', '84-617-4901', null, '178 Stang Pass', 'Aquiraz');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (2, 2, 2, 'Murial', 'Drezzer', '74-062-9657', '1-(404)109-4308', '416 Novick Junction', 'Atlanta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (3, 3, 3, 'Anson', 'Fishly', '77-021-5120', null, '545 Farwell Plaza', 'Dongshan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (4, 4, 4, 'Ursuline', 'Whapple', '08-190-6624', '220-(562)426-3168', '00 Algoma Trail', 'Pateh Sam');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (5, 5, 5, 'Dacey', 'Druett', '38-899-5141', null, '323 Fair Oaks Avenue', 'Xiangtan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (6, 6, 6, 'Bo', 'Iorizzi', '13-090-4830', '1-(713)950-6007', '1 Muir Lane', 'Houston');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (7, 7, 7, 'Maynord', 'Barczynski', '67-630-2056', '62-(349)665-0029', '50152 Talisman Lane', 'Karanganyar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (8, 8, 8, 'Estele', 'Bineham', '76-200-6640', '86-(912)491-4910', '1 Amoth Way', 'Gaopu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (9, 9, 9, 'Ofilia', 'Peasegood', '59-899-8224', '86-(808)518-3353', '9943 Donald Road', 'Qiancang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (10, 10, 10, 'Rorie', 'Aitchinson', '06-439-1435', '62-(171)169-7890', '0 Summerview Place', 'Cempa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (11, 1, 11, 'Lynsey', 'Illingsworth', '70-267-5980', '63-(757)798-0869', '9 Maywood Circle', 'Amio');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (12, 2, 12, 'Adelheid', 'Brachell', '75-055-4176', '55-(506)783-0015', '5 Roth Parkway', 'Hortolândia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (13, 3, 13, 'Agnese', 'Finlason', '77-842-4975', '86-(628)855-4656', '84111 Donald Plaza', 'Donghe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (14, 4, 14, 'Retha', 'Rawsthorn', '67-874-1735', '86-(167)718-7422', '8900 Village Park', 'Hongyan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (15, 5, 15, 'Kacie', 'Gunther', '62-491-6968', '249-(510)575-8381', '6 Troy Lane', 'Tonj');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (16, 6, 16, 'Fidelity', 'Swanson', '66-018-5066', '358-(317)390-6614', '088 Sachs Point', 'Tampere');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (17, 7, 17, 'Evey', 'Kigelman', '22-421-8528', null, '458 Thompson Plaza', 'Umuquena');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (18, 8, 18, 'Julita', 'Salvin', '10-916-7999', '7-(514)893-9413', '98697 2nd Avenue', 'Shalushka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (19, 9, 19, 'Bibi', 'Clarkin', '47-762-6574', '63-(634)851-7604', '23 Monica Center', 'Bankaw');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (20, 10, 20, 'Kanya', 'Hellis', '92-259-8806', '86-(184)828-2234', '593 Bunting Circle', 'Dongxin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (21, 1, 21, 'Wolfie', 'Bierling', '64-053-9975', null, '2 Kim Road', 'Małkinia Górna');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (22, 2, 22, 'Rosanna', 'Fronek', '23-851-3462', '54-(339)568-8860', '186 Lien Terrace', 'Sunchales');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (23, 3, 23, 'Irene', 'Collibear', '71-271-9994', '62-(690)110-1490', '462 Hoepker Plaza', 'Manalu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (24, 4, 24, 'Hedi', 'Longhurst', '91-938-0880', '86-(725)889-2555', '8 Everett Hill', 'Tongda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (25, 5, 25, 'Dulcine', 'Dorman', '46-273-3985', '1-(907)651-6788', '0064 Golf Course Parkway', 'Fairbanks');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (26, 6, 26, 'Dunn', 'Hatcher', '64-927-5023', null, '8 Harper Court', 'Vidago');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (27, 7, 27, 'Caprice', 'Castleton', '70-064-2161', '55-(784)209-3548', '599 Bayside Drive', 'São Gabriel');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (28, 8, 28, 'Tori', 'Yaus', '94-386-8815', '351-(887)889-4102', '4 Moland Crossing', 'Seixas do Douro');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (29, 9, 29, 'Lilah', 'Rudgard', '98-731-2358', '256-(286)594-5916', '18464 Havey Junction', 'Bukedea');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (30, 10, 30, 'Marissa', 'Mayte', '50-076-5235', '86-(681)634-6010', '5 Jana Court', 'Shangzhang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (31, 1, 31, 'Sergeant', 'Huxham', '15-502-4873', '234-(804)956-5563', '29 Larry Hill', 'Zuru');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (32, 2, 32, 'Tucker', 'Seally', '20-040-3186', '51-(615)107-4051', '0 Caliangt Court', 'Carhuac');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (33, 3, 33, 'Shayna', 'Jozaitis', '57-938-8732', '66-(243)273-7032', '7775 Sachtjen Park', 'Kut Chum');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (34, 4, 34, 'Vivienne', 'Titheridge', '18-806-6028', null, '9 Sheridan Center', 'Songhe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (35, 5, 35, 'Tynan', 'Muneely', '42-412-9718', null, '9 Boyd Way', 'Itatinga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (36, 6, 36, 'Conant', 'Bye', '08-896-8415', '86-(991)336-7755', '2302 Autumn Leaf Pass', 'Kulun');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (37, 7, 37, 'Holly-anne', 'Gemeau', '41-861-8688', '7-(705)279-7928', '5 Darwin Junction', 'Nagornyy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (38, 8, 38, 'Kalindi', 'Jenne', '25-335-2395', null, '661 Division Terrace', 'Punata');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (39, 9, 39, 'Ruperto', 'Lakeman', '77-780-3417', null, '09434 Loomis Drive', 'Namayingo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (40, 10, 40, 'Gearalt', 'Calway', '86-963-3625', '86-(363)417-0337', '70 South Crossing', 'Doumen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (41, 1, 41, 'Anatol', 'Barrick', '85-984-1115', null, '730 Ilene Plaza', 'Ntoke');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (42, 2, 42, 'Kaitlyn', 'McGovern', '95-946-4379', '86-(228)680-0545', '6798 Forest Dale Way', 'Suhe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (43, 3, 43, 'Karlotta', 'Helliker', '21-712-8665', '351-(517)860-6567', '62436 Myrtle Pass', 'Sobreiro');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (44, 4, 44, 'Adrienne', 'Gartsyde', '94-822-8675', '1-(339)118-5579', '839 Vidon Hill', 'Lynn');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (45, 5, 45, 'Lynne', 'Offener', '66-771-8939', '86-(624)264-8658', '27 Kennedy Court', 'Lihu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (46, 6, 46, 'Paco', 'Dowry', '73-122-5095', '55-(395)211-3191', '00 Bayside Park', 'Raposos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (47, 7, 47, 'Shirlee', 'Featherbie', '30-512-4362', '7-(212)356-2704', '827 East Crossing', 'Ust’-Katav');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (48, 8, 48, 'Brig', 'Gonzalo', '59-138-8160', '1-(202)673-8057', '071 Bultman Park', 'Washington');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (49, 9, 49, 'Annnora', 'Thome', '74-826-6230', '48-(192)609-6951', '8 Mallard Center', 'Długosiodło');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (50, 10, 50, 'Felike', 'Spuffard', '08-065-6868', null, '152 Main Street', 'La Libertad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (51, 1, 51, 'Graham', 'Blacksell', '54-783-5568', '86-(927)594-0707', '18 Bunting Circle', 'Diaopu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (52, 2, 52, 'Ozzie', 'Moy', '41-572-0234', '63-(135)511-8173', '579 Old Gate Plaza', 'Bagumbayan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (53, 3, 53, 'Bent', 'Harmar', '14-145-8526', '63-(822)380-4689', '765 Kropf Place', 'Santa Barbara');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (54, 4, 54, 'Andreana', 'Polle', '48-932-4012', '51-(731)955-6269', '47 Tennyson Terrace', 'Santa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (55, 5, 55, 'Randy', 'Acey', '53-920-7602', null, '50696 Schlimgen Pass', 'Banjar Pande');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (56, 6, 56, 'Arturo', 'Djekic', '64-562-4818', '86-(148)105-7568', '33 South Center', 'Xinxu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (57, 7, 57, 'Riane', 'Kemmis', '18-106-5998', '63-(963)770-6687', '4947 Macpherson Alley', 'Pangapisan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (58, 8, 58, 'Marijn', 'Cowlishaw', '91-751-6712', '506-(197)768-1045', '28097 Roth Point', 'Mercedes');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (59, 9, 59, 'Rosaleen', 'Leaves', '00-152-2756', '255-(422)385-2481', '5 Nova Avenue', 'Namikupa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (60, 10, 60, 'Frannie', 'Blissett', '76-657-4684', null, '77924 Delladonna Alley', 'Pleiku');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (61, 1, 61, 'Galven', 'Killingbeck', '45-764-5269', '54-(235)703-0394', '111 Sycamore Hill', 'Cañada de Gómez');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (62, 2, 62, 'Coop', 'Walewicz', '52-281-5395', '856-(199)200-1693', '8662 Valley Edge Crossing', 'Thakhèk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (63, 3, 63, 'Nissie', 'Alven', '86-217-5689', null, '1668 Debs Street', 'Stubno');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (64, 4, 64, 'Rouvin', 'McGrah', '62-985-4410', '62-(865)380-0747', '3 Forster Crossing', 'Sawahan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (65, 5, 65, 'Kirbee', 'Pick', '45-974-8695', '58-(283)116-6849', '777 Monument Junction', 'La Mula');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (66, 6, 66, 'Herta', 'Cleal', '44-109-2070', '62-(905)970-8423', '14 Browning Court', 'Argotirto Krajan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (67, 7, 67, 'Elberta', 'Gregon', '48-639-5962', '62-(732)646-7559', '7220 Shoshone Crossing', 'Cimara');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (68, 8, 68, 'Sherill', 'Kirwood', '37-228-7329', '976-(773)394-8824', '1484 Mockingbird Junction', 'Höviyn Am');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (69, 9, 69, 'Tarrah', 'Pavek', '84-246-2398', '63-(301)756-5426', '7 Arizona Point', 'Malasila');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (70, 10, 70, 'Evie', 'Griffitts', '94-782-4573', null, '9 Iowa Court', 'Cirompang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (71, 1, 71, 'Domini', 'McLeese', '25-709-3393', '86-(152)278-5398', '74 Hanson Plaza', 'Sandouping');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (72, 2, 72, 'Laverne', 'Siege', '15-386-4028', '62-(434)446-1161', '4492 Donald Junction', 'Singkup');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (73, 3, 73, 'Jules', 'Alldis', '50-468-3837', '63-(578)663-8961', '0123 Fuller Terrace', 'Purac');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (74, 4, 74, 'Lief', 'Casino', '54-626-9170', '81-(200)440-4582', '2200 4th Point', 'Sunagawa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (75, 5, 75, 'Kippy', 'Gilstoun', '70-794-4180', '55-(530)229-7925', '65035 Oriole Road', 'Vitória');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (76, 6, 76, 'Rhoda', 'Galpin', '78-126-2366', '66-(175)840-1410', '29 Park Meadow Terrace', 'Ta Phraya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (77, 7, 77, 'Vannie', 'Lejeune', '67-042-7755', null, '508 Beilfuss Pass', 'Tubajon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (78, 8, 78, 'Salem', 'Hayler', '96-285-0288', null, '0636 Summer Ridge Place', 'Macabugos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (79, 9, 79, 'Constantino', 'Brandoni', '90-467-2398', '359-(137)825-9468', '990 Pierstorff Circle', 'Nikopol');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (80, 10, 80, 'Jo-ann', 'Matuschek', '14-899-0691', '30-(144)452-0704', '9 Meadow Ridge Park', 'Megalópoli');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (81, 1, 81, 'Aubine', 'Eadon', '35-126-7054', '62-(998)837-7481', '0816 Autumn Leaf Junction', 'Krajan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (82, 2, 82, 'Thorndike', 'Minifie', '08-249-2545', '62-(931)548-5339', '07476 Lyons Alley', 'Tawangsari');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (83, 3, 83, 'Marshall', 'Ivell', '28-742-4978', null, '0 Sunfield Park', 'Tadmur');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (84, 4, 84, 'Monique', 'Nimmo', '68-717-2003', '7-(116)427-5420', '56 Sunfield Center', 'Serebryanyye Prudy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (85, 5, 85, 'Jonie', 'Titterton', '65-041-3256', null, '41 Fallview Street', 'Bishan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (86, 6, 86, 'Eachelle', 'Mallabund', '20-820-2266', '351-(202)129-5053', '793 Utah Terrace', 'Torreira');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (87, 7, 87, 'Phillie', 'Trunks', '96-998-3147', null, '23621 Lillian Center', 'Tangchijie');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (88, 8, 88, 'Catlee', 'Colthard', '91-583-7472', '53-(841)174-7909', '7255 Northridge Court', 'Florida');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (89, 9, 89, 'Vasilis', 'Shalcras', '28-283-9470', '1-(732)336-6938', '5 Jana Park', 'New Brunswick');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (90, 10, 90, 'Nesta', 'Lossman', '05-010-4473', '66-(161)969-5187', '29 6th Hill', 'Mueang Nonthaburi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (91, 1, 91, 'Darleen', 'Salazar', '43-445-5346', '86-(935)319-6581', '084 Sauthoff Alley', 'Yushugou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (92, 2, 92, 'Trixie', 'Whitten', '99-591-7351', '86-(545)599-9595', '02 Northview Avenue', 'Junkou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (93, 3, 93, 'Milo', 'Stanney', '98-976-4028', null, '53 Pond Place', 'Julun');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (94, 4, 94, 'Ruddie', 'Dagger', '00-304-9649', '86-(282)239-9140', '9972 Coleman Center', 'Ulan Us');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (95, 5, 95, 'Shae', 'Palethorpe', '18-116-6528', null, '30231 Tomscot Pass', 'Nantuo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (96, 6, 96, 'Daisy', 'MacTrustram', '81-468-6471', null, '86440 Pleasure Plaza', 'La Loma');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (97, 7, 97, 'Alden', 'O''Connel', '96-910-5156', '381-(114)706-6028', '2117 Dottie Park', 'Platičevo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (98, 8, 98, 'Ruby', 'Jakucewicz', '09-480-7993', null, '1 Lien Terrace', 'Fujikawaguchiko');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (99, 9, 99, 'Maryanne', 'Gartsyde', '99-502-7449', '46-(227)123-4809', '7 Armistice Alley', 'Mariestad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (100, 10, 100, 'Dennis', 'Fulker', '36-606-4902', null, '3514 Derek Junction', 'Ban Thai Tan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (101, 1, 1, 'Rivy', 'Cargo', '62-112-2669', null, '0 Hoffman Place', 'Wantian');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (102, 2, 2, 'Cleo', 'Simcox', '20-725-8546', '7-(330)974-6018', '452 Kinsman Crossing', 'Kokshetau');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (103, 3, 3, 'Adiana', 'Gabbitis', '39-796-2731', null, '19 Milwaukee Court', 'Ciharalang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (104, 4, 4, 'Lucilia', 'Ure', '84-989-4922', '57-(953)831-2757', '69210 Sutherland Avenue', 'Yarumal');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (105, 5, 5, 'Nicolis', 'Sewart', '75-041-8133', '55-(337)282-9052', '09 Green Ridge Hill', 'Bacabal');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (106, 6, 6, 'Lyle', 'Badham', '96-697-8215', '55-(724)313-8201', '81089 Dovetail Terrace', 'Itapevi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (107, 7, 7, 'Ede', 'Blei', '33-298-1827', null, '4 Artisan Junction', 'Emmen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (108, 8, 8, 'Timmi', 'Binton', '71-282-0119', '30-(462)658-2587', '14431 Hauk Parkway', 'Néa Smýrni');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (109, 9, 9, 'Richart', 'Arthurs', '84-231-7357', '63-(900)911-1323', '36 Waywood Court', 'Sumalig');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (110, 10, 10, 'Torrin', 'Terlinden', '72-585-6887', '62-(410)490-6980', '1 Towne Place', 'Sokobanteng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (111, 1, 11, 'Fania', 'Philipson', '68-193-4311', null, '82 Vermont Parkway', 'Liucun');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (112, 2, 12, 'Matty', 'Mongin', '91-050-3104', '55-(614)593-5790', '1943 Springview Center', 'Costeira do Pirajubae');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (113, 3, 13, 'Phineas', 'Stienton', '34-691-8945', '47-(905)930-6162', '37849 Corscot Center', 'Oslo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (114, 4, 14, 'Lottie', 'Bremond', '02-691-0561', null, '88 Beilfuss Point', 'Górki Wielkie');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (115, 5, 15, 'Charlot', 'Iremonger', '30-596-3000', null, '54496 Evergreen Point', 'Novi Slankamen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (116, 6, 16, 'Gavrielle', 'Nickoles', '98-557-9515', '86-(543)328-1038', '04 Carey Hill', 'Limushan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (117, 7, 17, 'Woodman', 'Mackley', '77-317-4873', '55-(705)797-3211', '50 Buena Vista Hill', 'Araxá');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (118, 8, 18, 'Raimundo', 'Denslow', '35-006-1208', '62-(993)471-6530', '1733 Sachtjen Drive', 'Laikit, Laikit II (Dimembe)');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (119, 9, 19, 'Mallissa', 'Henriques', '33-678-6668', '234-(786)846-7292', '379 Tennessee Alley', 'Aku');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (120, 10, 20, 'Farra', 'Fadden', '31-168-9994', '1-(409)608-2022', '1 Havey Way', 'Saint-Ambroise');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (121, 1, 21, 'Lida', 'Coultish', '90-108-2305', '7-(820)351-4239', '9 Beilfuss Terrace', 'Voskresensk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (122, 2, 22, 'Jecho', 'Twiggs', '32-274-1376', '36-(312)986-2371', '58 Calypso Street', 'Győr');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (123, 3, 23, 'Karalee', 'Sulley', '31-987-7940', '55-(918)294-2674', '380 Forest Run Avenue', 'Orleans');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (124, 4, 24, 'Kalvin', 'Hartnup', '96-842-5938', '86-(942)542-3737', '3203 Shoshone Hill', 'Sanxixiang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (125, 5, 25, 'Remington', 'Crowhurst', '23-553-9364', '62-(498)400-5311', '35683 6th Plaza', 'Huineno');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (126, 6, 26, 'Linnet', 'Jakucewicz', '11-322-0772', '86-(509)968-9337', '728 Kings Place', 'Youlongchuan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (127, 7, 27, 'Myra', 'Verry', '01-586-9402', null, '3 Heath Drive', 'Zhouxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (128, 8, 28, 'Jonie', 'Ebenezer', '67-900-2509', '86-(195)234-9922', '372 Westend Terrace', 'Suizhou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (129, 9, 29, 'Kristos', 'Crumb', '06-830-2319', null, '2993 Dunning Parkway', 'Chengmen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (130, 10, 30, 'Fallon', 'Matthisson', '80-649-1967', '507-(140)549-1360', '78946 Sundown Crossing', 'Pueblo Nuevo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (131, 1, 31, 'Jakie', 'Walczynski', '18-325-0339', '86-(237)889-1114', '6 Kings Terrace', 'Fenhe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (132, 2, 32, 'Murdoch', 'Penhalewick', '56-848-9074', '966-(537)352-3505', '1903 Manufacturers Avenue', 'Julayjilah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (133, 3, 33, 'Germayne', 'Lovart', '76-577-9672', null, '96 Parkside Court', 'Samangawah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (134, 4, 34, 'Virgilio', 'Grunwald', '10-216-7317', null, '5869 Kim Park', 'El Potrero');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (135, 5, 35, 'Lennard', 'Dargan', '60-005-3086', null, '77037 Sherman Alley', 'Banjar Ponggang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (136, 6, 36, 'Reynolds', 'Zucker', '30-032-3842', '62-(272)708-3790', '549 Elmside Center', 'Kanigorokrajan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (137, 7, 37, 'Rycca', 'Zottoli', '67-532-4876', '86-(427)701-5107', '62 Vera Lane', 'Gaopai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (138, 8, 38, 'Siouxie', 'Vaskov', '83-682-8338', '48-(828)293-1945', '005 Merchant Drive', 'Pamiątkowo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (139, 9, 39, 'Roxy', 'Lanfere', '69-094-7205', '63-(661)503-6518', '8368 Loomis Place', 'Rebe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (140, 10, 40, 'Kellen', 'Adamsson', '90-602-6092', '86-(360)510-3303', '28 Prairie Rose Place', 'Dingqing');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (141, 1, 41, 'Robina', 'Blaxter', '91-335-3787', '86-(799)722-9885', '34652 Mockingbird Terrace', 'Mengdadazhuang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (142, 2, 42, 'Steffi', 'Vuittet', '30-541-5047', null, '9 Susan Junction', 'Ipoti');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (143, 3, 43, 'Bunni', 'McNish', '38-809-3016', '52-(505)316-0907', '1 Longview Circle', 'Arroyo Seco');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (144, 4, 44, 'Meghann', 'Muxworthy', '31-692-7887', '351-(393)944-3720', '6 Mariners Cove Junction', 'Costa Nova do Prado');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (145, 5, 45, 'Ive', 'Eckersall', '61-974-3453', '375-(596)408-1982', '2529 4th Center', 'Lahoysk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (146, 6, 46, 'Torie', 'Cosford', '69-802-9449', '27-(444)803-5364', '249 Rusk Way', 'Sasolburg');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (147, 7, 47, 'Willis', 'Kinchin', '00-169-0129', '62-(856)976-4075', '9586 Pleasure Lane', 'Kauman');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (148, 8, 48, 'Aurora', 'Simic', '89-543-2035', null, '0 Hollow Ridge Trail', 'Duqiao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (149, 9, 49, 'Midge', 'Stanyard', '71-817-1723', '20-(661)144-7817', '645 Spohn Pass', 'Samannūd');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (150, 10, 50, 'Bettina', 'Stopps', '62-458-5461', null, '40 Oak Terrace', 'Shihua');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (151, 1, 51, 'Romona', 'Gosker', '19-668-2390', '58-(124)818-8270', '09 Dunning Plaza', 'El Regalo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (152, 2, 52, 'Ellswerth', 'Bidmead', '28-786-8552', null, '09589 Bluestem Hill', 'Boyany');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (153, 3, 53, 'Paula', 'Cribbin', '11-041-0658', '55-(223)331-8299', '48291 Oriole Center', 'Passo Fundo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (154, 4, 54, 'Laurence', 'Cranson', '65-140-8504', '380-(804)835-0651', '5 Milwaukee Avenue', 'Kosmach');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (155, 5, 55, 'Cindra', 'Peagrim', '92-827-5686', '1-(810)351-3538', '3 Shoshone Park', 'Detroit');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (156, 6, 56, 'Brucie', 'Bonde', '81-703-7048', '86-(379)742-8208', '8824 Prairieview Place', 'Duowa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (157, 7, 57, 'Emmye', 'Davenhall', '60-140-0065', '970-(169)559-1540', '88712 Schiller Lane', 'Faqqū‘ah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (158, 8, 58, 'Sallee', 'Dean', '32-772-9487', '62-(775)345-6657', '6465 Carioca Circle', 'Cisaro');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (159, 9, 59, 'Sondra', 'Water', '32-405-1916', '591-(223)250-3939', '66066 Forster Way', 'Yotala');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (160, 10, 60, 'Yovonnda', 'Andriss', '77-440-3514', '86-(207)313-7659', '3361 Grim Place', 'Caicun');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (161, 1, 61, 'Sergeant', 'Treagus', '05-652-6128', '62-(306)228-4721', '3 Bluejay Plaza', 'Sumberbatas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (162, 2, 62, 'Arty', 'Kinchlea', '84-271-4199', '48-(212)348-6021', '2 Mesta Pass', 'Kaczory');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (163, 3, 63, 'Matilde', 'Brenstuhl', '64-047-5886', null, '2 Jenna Pass', 'Priekule');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (164, 4, 64, 'Chaddie', 'Matyukon', '35-247-4244', '7-(434)521-3819', '55 Northridge Center', 'Ustyuzhna');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (165, 5, 65, 'Vitia', 'Geertz', '44-488-0084', '62-(874)223-7027', '9 Hauk Crossing', 'Gunungmanik');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (166, 6, 66, 'Casey', 'Headey', '41-123-0209', '420-(618)196-4967', '1347 Coolidge Street', 'Staré Hradiště');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (167, 7, 67, 'Federico', 'Fautley', '00-314-2225', '46-(295)637-4370', '34 Florence Plaza', 'Ronneby');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (168, 8, 68, 'Tyrus', 'Claridge', '29-456-1458', null, '02 Harbort Place', 'Chemal');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (169, 9, 69, 'Lucita', 'Skoughman', '52-746-6563', '502-(604)179-8618', '15 Arrowood Junction', 'Cabañas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (170, 10, 70, 'Ewell', 'Mucklo', '54-249-0969', '504-(482)229-0994', '3 Sunnyside Junction', 'La Libertad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (171, 1, 71, 'Joscelin', 'Beller', '13-779-0731', '7-(827)363-4184', '2163 Garrison Street', 'Pryazha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (172, 2, 72, 'Jenny', 'Leaburn', '15-511-2049', '33-(266)157-3244', '38 Corry Park', 'Paris 02');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (173, 3, 73, 'Jennilee', 'Speachley', '29-391-8287', '962-(854)761-4174', '753 Twin Pines Center', 'Al Jīzah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (174, 4, 74, 'Tallou', 'Gillow', '34-771-4115', '374-(713)196-6089', '34742 Di Loreto Center', 'Dvin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (175, 5, 75, 'Randy', 'Dennick', '43-076-9575', '351-(938)789-2078', '13119 Clyde Gallagher Circle', 'Vila Maior');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (176, 6, 76, 'Lynde', 'Pavelin', '10-291-2987', null, '906 Steensland Pass', 'Xincheng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (177, 7, 77, 'Giffie', 'Avramow', '18-016-7403', '34-(516)873-3658', '1 Dexter Lane', 'Aviles');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (178, 8, 78, 'Baron', 'Whiscard', '14-179-9658', '7-(380)709-5306', '1680 Arkansas Pass', 'Vorontsovka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (179, 9, 79, 'Darrick', 'Golthorpp', '17-672-7954', null, '1691 Mayer Terrace', 'Caieiras');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (180, 10, 80, 'Ilyse', 'Merrilees', '30-954-3699', null, '2 Declaration Terrace', 'Fermelã');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (181, 1, 81, 'Ethyl', 'Malkin', '97-393-2370', '351-(982)517-8595', '0 East Crossing', 'Vilar do Monte');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (182, 2, 82, 'Stefania', 'Kuhnert', '52-452-9099', '86-(821)715-6643', '90238 Superior Drive', 'Shijing');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (183, 3, 83, 'Devora', 'Quilleash', '47-356-1204', '1-(124)891-4023', '488 Golf View Alley', 'Bouctouche');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (184, 4, 84, 'Karoly', 'MacParland', '47-423-7225', '380-(595)118-1279', '73 Drewry Crossing', 'Bratslav');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (185, 5, 85, 'Lianna', 'Borland', '46-119-0256', '62-(761)448-1298', '99 Kingsford Alley', 'Ayamaru');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (186, 6, 86, 'Druci', 'Milius', '69-847-8230', null, '418 Hansons Junction', 'Rawawilis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (187, 7, 87, 'Skippie', 'Elington', '39-769-8829', '998-(974)439-0495', '62263 Northview Park', 'Tashkent');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (188, 8, 88, 'Jdavie', 'Voysey', '40-472-1774', '1-(504)891-1930', '1 Knutson Plaza', 'New Orleans');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (189, 9, 89, 'Gates', 'Ibeson', '00-391-3078', '86-(430)958-6031', '9024 Pankratz Trail', 'Jiupu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (190, 10, 90, 'Sterling', 'Irce', '26-101-3691', null, '65 Mendota Place', 'San Carlos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (191, 1, 91, 'Sherlock', 'Hosten', '47-600-7235', '86-(472)355-1641', '11 Moland Park', 'Liulin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (192, 2, 92, 'Raimund', 'Fallen', '57-607-7864', '63-(444)713-6740', '43806 Old Shore Junction', 'Lawa-an');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (193, 3, 93, 'Wilbert', 'Ernshaw', '89-772-3175', '86-(756)111-1209', '7 Bluestem Road', 'Luzhou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (194, 4, 94, 'Ruddy', 'Mosconi', '50-607-9348', '66-(223)123-7230', '61600 Acker Road', 'Huai Mek');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (195, 5, 95, 'Price', 'Snoday', '58-647-5935', '7-(183)325-1150', '3 Fisk Place', 'Yaroslavskiy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (196, 6, 96, 'Biddie', 'Venton', '67-635-7803', '976-(356)683-5982', '864 Summerview Crossing', 'Havirga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (197, 7, 97, 'Elisabet', 'Puncher', '16-218-4961', null, '70 Corry Road', 'Rodionovo-Nesvetaiskoye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (198, 8, 98, 'Benedick', 'Cleever', '60-137-7341', '54-(168)229-6627', '83 New Castle Hill', 'Laborde');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (199, 9, 99, 'Elia', 'Bluck', '40-760-9646', '62-(932)475-3201', '67965 Dapin Court', 'Tempursari Wetan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (200, 10, 100, 'Waring', 'Albarez', '44-499-3825', null, '6 Prairieview Parkway', 'Emmen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (201, 1, 1, 'Elisha', 'Deering', '63-420-7995', '380-(246)248-7847', '37 American Ash Place', 'Bilohirs’k');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (202, 2, 2, 'Brion', 'Tydeman', '19-736-3495', null, '730 Fieldstone Crossing', 'Obanazawa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (203, 3, 3, 'Sabine', 'Willoughway', '62-374-5834', '86-(310)186-0367', '4 Golf Course Terrace', 'Jinkou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (204, 4, 4, 'Ivar', 'Goddman', '27-640-6548', '48-(817)341-4650', '39729 Michigan Way', 'Brok');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (205, 5, 5, 'Cosetta', 'Barnsley', '26-843-0372', null, '78 Mayfield Pass', 'Yongxing');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (206, 6, 6, 'Bambi', 'Alasdair', '71-178-8347', '55-(139)176-8628', '1 Hoard Alley', 'Medeiros Neto');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (207, 7, 7, 'Lizabeth', 'Conklin', '47-490-6548', null, '1107 Dwight Way', 'Santa Maria');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (208, 8, 8, 'Wally', 'Binfield', '89-278-5652', null, '527 Ludington Point', 'Gomel');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (209, 9, 9, 'Pavia', 'Gladbach', '19-714-6729', null, '88767 Maryland Point', 'Khao Kho');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (210, 10, 10, 'Muffin', 'Issacov', '55-718-2291', '48-(254)200-2849', '71964 Mandrake Avenue', 'Świętajno');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (211, 1, 11, 'Erminie', 'Lycett', '00-088-4286', '62-(766)168-8386', '1392 Northridge Alley', 'Baroh');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (212, 2, 12, 'Franciskus', 'Tomaello', '00-682-5689', '48-(371)160-1152', '9 Rusk Terrace', 'Czerwin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (213, 3, 13, 'Johanna', 'Goody', '88-006-7291', '84-(222)432-4725', '2635 Rigney Place', 'Thị Trấn Na Hang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (214, 4, 14, 'Royall', 'Shillitto', '85-672-3368', '230-(495)647-8102', '326 Cardinal Hill', 'Centre de Flacq');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (215, 5, 15, 'Hale', 'Heisham', '14-651-6300', '55-(822)117-6439', '36 High Crossing Avenue', 'Iporá');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (216, 6, 16, 'Amil', 'Baird', '50-590-3274', '351-(934)559-9700', '80286 Morrow Center', 'São Pedro');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (217, 7, 17, 'Marnia', 'Clawley', '73-871-8934', '351-(242)442-0638', '3172 Continental Court', 'Penteado');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (218, 8, 18, 'Christalle', 'Woof', '92-463-9973', '57-(452)416-7217', '415 Surrey Park', 'Guaitarilla');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (219, 9, 19, 'Jedd', 'Mitchely', '99-649-7165', '47-(975)247-1850', '24 Goodland Plaza', 'Oslo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (220, 10, 20, 'Berti', 'Dunford', '31-114-8814', '212-(137)188-2082', '17 Oneill Pass', 'Tabia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (221, 1, 21, 'Dilan', 'Druitt', '80-678-1988', '351-(677)786-5939', '35113 Green Park', 'Bairros');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (222, 2, 22, 'Judon', 'Ankers', '48-564-1410', null, '88950 Melby Terrace', 'Kinalansan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (223, 3, 23, 'Sara', 'Lobell', '44-559-4829', '62-(849)663-4663', '32696 Caliangt Road', 'Srabah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (224, 4, 24, 'Isacco', 'McKendo', '33-920-9939', '86-(699)677-7451', '1135 Lindbergh Circle', 'Sidu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (225, 5, 25, 'Bettina', 'Gumm', '55-298-9802', '260-(134)611-3338', '5 Stang Road', 'Luanshya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (226, 6, 26, 'Sheffie', 'Silverton', '94-980-6807', '1-(253)255-2021', '7 Waywood Road', 'Seattle');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (227, 7, 27, 'Cassey', 'Truelock', '66-724-0641', '63-(856)372-6996', '797 Melrose Court', 'Maindang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (228, 8, 28, 'Waylin', 'Gwinnett', '05-869-7549', '62-(577)840-0616', '36747 Paget Parkway', 'Situsari');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (229, 9, 29, 'Isabeau', 'O'' Driscoll', '73-643-8672', '53-(708)403-2783', '51 Anthes Hill', 'Minas de Matahambre');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (230, 10, 30, 'Jeffry', 'Wiltshear', '26-895-2690', null, '09 John Wall Alley', 'Béthune');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (231, 1, 31, 'Darbee', 'Gaylord', '05-668-4504', '30-(607)736-2494', '30362 Eagle Crest Point', 'Koilás');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (232, 2, 32, 'Ingmar', 'Liepina', '92-989-7509', '49-(953)672-6205', '535 Dwight Pass', 'Hamburg');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (233, 3, 33, 'Laraine', 'Paolinelli', '90-342-6048', '351-(731)746-4286', '730 Gale Way', 'Vale da Bajouca');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (234, 4, 34, 'Harry', 'Dawnay', '61-813-7278', '7-(303)375-8744', '4 Grayhawk Pass', 'Zhireken');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (235, 5, 35, 'Arabele', 'Ridout', '96-302-4908', null, '28868 Merchant Lane', 'Mlonggo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (236, 6, 36, 'Gill', 'Geaveny', '73-025-9131', '375-(527)610-5256', '88230 Declaration Street', 'Kokhanava');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (237, 7, 37, 'Fin', 'Ryrie', '08-638-9400', '595-(360)540-7125', '701 Mallard Hill', 'San Juan Bautista');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (238, 8, 38, 'Lanie', 'Doby', '75-746-0855', '351-(119)182-1792', '65556 Manley Park', 'Chãos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (239, 9, 39, 'Law', 'Van Dale', '77-817-5151', '81-(379)641-9689', '656 Prentice Parkway', 'Gifu-shi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (240, 10, 40, 'Shirlene', 'Meddows', '02-486-2626', '86-(434)778-8966', '9553 Surrey Road', 'Lingquan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (241, 1, 41, 'Carmelia', 'Skells', '72-167-9781', '386-(375)788-1951', '6717 Sutherland Avenue', 'Osilnica');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (242, 2, 42, 'Alley', 'Yuryshev', '63-509-4281', '44-(778)176-2062', '71 Elmside Point', 'Hatton');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (243, 3, 43, 'Lorilee', 'Gooder', '18-298-9573', '56-(402)160-4714', '24839 Pleasure Crossing', 'Pucón');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (244, 4, 44, 'Benny', 'Southcombe', '12-783-7125', '46-(952)994-8115', '1 Northfield Park', 'Övertorneå');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (245, 5, 45, 'Kania', 'Leger', '31-202-1662', '33-(955)971-0813', '06 Coolidge Center', 'Courbevoie');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (246, 6, 46, 'Ronna', 'Giacopini', '70-536-9084', '46-(829)430-6945', '0524 Lake View Court', 'Kristinehamn');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (247, 7, 47, 'Bram', 'Trengrouse', '73-706-8032', null, '15 Sunbrook Street', 'Pangkajene');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (248, 8, 48, 'Adrian', 'Kerman', '32-899-7534', '30-(414)584-3686', '5068 Mesta Drive', 'Kampánis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (249, 9, 49, 'Roseanna', 'Bolino', '77-786-4483', null, '652 Armistice Road', 'Gubkinskiy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (250, 10, 50, 'Tessie', 'Le Marquis', '68-678-5578', '62-(780)985-9765', '8 Karstens Place', 'Koa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (251, 1, 51, 'Jana', 'Keohane', '88-307-8596', '86-(566)852-6861', '51790 Vernon Street', 'Yingkeng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (252, 2, 52, 'Latisha', 'Gaine of England', '44-934-8414', '48-(998)539-3899', '9117 Briar Crest Trail', 'Szprotawa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (253, 3, 53, 'Avigdor', 'Strute', '88-322-2486', '63-(461)215-9778', '3966 Shasta Lane', 'Siayan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (254, 4, 54, 'Lonna', 'Siggins', '19-397-7473', '63-(757)766-2107', '1 Shopko Pass', 'Salvacion');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (255, 5, 55, 'Linn', 'Videan', '36-080-0247', null, '0 Mallard Point', 'Wonorejo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (256, 6, 56, 'Rhianon', 'Canellas', '69-539-1160', '55-(401)424-3208', '45816 Bayside Alley', 'Mairinque');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (257, 7, 57, 'Chelsey', 'Ismail', '54-464-1782', '351-(251)212-6369', '15 Summerview Point', 'Urrô');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (258, 8, 58, 'Deina', 'Hacking', '26-877-8455', '51-(883)838-9090', '3 Rusk Parkway', 'Lincha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (259, 9, 59, 'Lilyan', 'Venn', '36-975-1434', null, '6 Huxley Way', 'Kalimati');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (260, 10, 60, 'Oates', 'Pren', '04-969-4130', '27-(228)546-1025', '3 Nobel Junction', 'Midrand');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (261, 1, 61, 'Edgardo', 'Caesman', '70-024-4758', '63-(285)239-8600', '51 Lakewood Point', 'Kaytitinga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (262, 2, 62, 'Adolf', 'Goulden', '33-334-8327', null, '4 Hoard Center', 'Kiruna');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (263, 3, 63, 'Collin', 'Ricard', '45-015-0419', '81-(224)434-1088', '10934 Browning Circle', 'Yaita');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (264, 4, 64, 'Herrick', 'Mazey', '57-753-8322', '7-(789)944-5561', '9564 Buena Vista Trail', 'Kirovgrad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (265, 5, 65, 'Halette', 'Hurley', '00-160-6134', '62-(511)968-9137', '654 Pleasure Hill', 'Wolofeo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (266, 6, 66, 'Jodie', 'Izaac', '04-088-2150', null, '1 Columbus Circle', 'Gubeikou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (267, 7, 67, 'Elwira', 'Penchen', '78-031-7704', '7-(229)159-3684', '41988 American Ash Parkway', 'Yasenskaya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (268, 8, 68, 'Katusha', 'Thorwarth', '98-793-7490', '86-(198)759-3337', '3 Forster Road', 'Qingjiangqiao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (269, 9, 69, 'Fonsie', 'Watford', '20-133-2371', '963-(855)464-7106', '48 Daystar Street', 'Tall Abyaḑ');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (270, 10, 70, 'Lambert', 'Lamanby', '88-540-2186', '507-(286)350-3352', '65532 John Wall Junction', 'Calzada Larga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (271, 1, 71, 'Ulric', 'Brasier', '59-466-3412', '51-(600)910-8340', '56 Northfield Pass', 'Tibillo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (272, 2, 72, 'Carie', 'Tiner', '35-841-6087', '62-(567)588-9158', '61384 Butternut Pass', 'Pasarkayu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (273, 3, 73, 'Quinton', 'Clutton', '54-514-4112', '62-(790)488-1017', '83938 Amoth Avenue', 'Hantara');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (274, 4, 74, 'Benita', 'Stredder', '96-525-5722', '381-(510)771-9962', '68 Spaight Road', 'Senta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (275, 5, 75, 'Graehme', 'Linnett', '76-506-1393', '351-(545)141-9254', '802 Gulseth Pass', 'Algarvia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (276, 6, 76, 'Amalle', 'Yegorkov', '13-727-4795', '1-(512)335-2543', '553 Basil Avenue', 'Austin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (277, 7, 77, 'Rosamond', 'Dellenbrook', '99-114-3266', '27-(406)309-0560', '48793 Lakewood Gardens Plaza', 'Lebowakgomo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (278, 8, 78, 'Pearline', 'Arnely', '87-043-5742', '970-(908)420-6905', '7 Vernon Plaza', 'Bayt ‘Ūr at Taḩtā');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (279, 9, 79, 'Cate', 'Doxsey', '46-035-0764', '86-(112)606-3630', '18 Jenifer Park', 'Chengshan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (280, 10, 80, 'Sigfried', 'Gillhespy', '77-424-6208', null, '830 Miller Drive', 'Velké Karlovice');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (281, 1, 81, 'Berke', 'Prince', '21-563-1656', '33-(727)920-6182', '765 Claremont Street', 'Lescar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (282, 2, 82, 'Corrinne', 'O''Meara', '85-789-4806', '505-(655)256-8075', '52219 Dexter Pass', 'Matiguás');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (283, 3, 83, 'Bria', 'Ellson', '38-822-9427', '86-(281)172-1249', '77133 Gerald Pass', 'Kesheng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (284, 4, 84, 'Joana', 'Adamoli', '78-026-7506', '591-(985)790-2625', '39 Hollow Ridge Point', 'Azurduy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (285, 5, 85, 'Stuart', 'Townsley', '34-309-0528', '374-(280)651-9828', '376 Blaine Court', 'Gandzak');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (286, 6, 86, 'Elihu', 'Urquhart', '01-079-4030', null, '36034 Esch Junction', 'Chishan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (287, 7, 87, 'Al', 'Defries', '25-889-9438', '86-(603)153-0936', '75629 Fuller Court', 'Zhitang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (288, 8, 88, 'Vance', 'Stang-Gjertsen', '29-684-2058', '234-(469)764-6017', '932 Corry Road', 'Eha Amufu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (289, 9, 89, 'Tristan', 'Crush', '77-313-8874', '86-(509)543-4270', '0 Ridgeview Pass', 'Shuiquan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (290, 10, 90, 'Teodorico', 'Berriball', '61-056-6261', '226-(306)289-6608', '53499 Bashford Drive', 'Tougan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (291, 1, 91, 'Carey', 'Nise', '03-673-3950', null, '510 Novick Parkway', 'Carromeu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (292, 2, 92, 'Zack', 'Noor', '80-573-3456', '30-(523)974-6655', '9827 Golf View Crossing', 'Genisséa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (293, 3, 93, 'Lurline', 'Praten', '82-428-9184', null, '6515 Morning Court', 'Shaxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (294, 4, 94, 'Gerladina', 'Botfield', '96-658-9988', '7-(619)719-4781', '8 1st Point', 'Semënovskoye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (295, 5, 95, 'Allegra', 'Napoli', '75-455-5209', '355-(756)332-5052', '77 Hoard Hill', 'Sukth');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (296, 6, 96, 'Erick', 'Rudyard', '69-519-3270', null, '629 Westport Pass', 'Paris 11');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (297, 7, 97, 'Joanne', 'Woolcocks', '07-080-8322', '994-(303)598-8878', '93 Crowley Junction', 'Dondar Quşçu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (298, 8, 98, 'Christen', 'Olle', '06-035-8985', null, '827 Mccormick Pass', 'Fatou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (299, 9, 99, 'Quinta', 'Ney', '55-040-2294', '62-(964)611-1109', '736 Paget Pass', 'Bakau');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (300, 10, 100, 'Edik', 'Regina', '57-211-5787', '86-(566)289-5768', '34 8th Place', 'Dayuan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (301, 1, 1, 'Kaye', 'Titheridge', '66-579-0207', '62-(230)639-0663', '3 Mcguire Road', 'Kedungsumurkrajan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (302, 2, 2, 'Cristiano', 'Loseke', '63-436-2177', null, '4 Oxford Center', 'Sergeyevka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (303, 3, 3, 'Cassaundra', 'Cunah', '31-519-8070', '63-(802)427-7541', '5302 Lien Trail', 'Nunguan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (304, 4, 4, 'Alexei', 'Brisset', '57-805-8299', '62-(698)373-5249', '0508 Mallard Center', 'Akarakar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (305, 5, 5, 'Darb', 'Coneley', '84-479-4375', '86-(940)746-8832', '77740 Porter Road', 'Shuangmiaojie');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (306, 6, 6, 'Tabbitha', 'Tender', '54-615-0187', '86-(799)829-9334', '64 Miller Junction', 'Gaotian');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (307, 7, 7, 'Devland', 'Kobera', '38-554-1835', '224-(728)733-0595', '15734 Mendota Junction', 'Tokonou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (308, 8, 8, 'Gloria', 'Perdue', '34-841-0477', '254-(957)641-7636', '705 Maryland Trail', 'Pumwani');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (309, 9, 9, 'Alanson', 'Shoebottom', '26-511-0500', '86-(762)585-5481', '1631 Pierstorff Parkway', 'Nizui');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (310, 10, 10, 'Marla', 'Balmer', '22-774-6797', null, '78709 Oakridge Point', 'Neietsu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (311, 1, 11, 'Cthrine', 'Gilliland', '64-591-9383', '63-(407)924-8809', '95 Drewry Park', 'Saraza');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (312, 2, 12, 'Mariam', 'Passingham', '65-203-6254', '7-(676)333-8028', '2 Armistice Point', 'Gavrilov-Yam');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (313, 3, 13, 'Jimmie', 'Chastenet', '27-105-5268', '226-(145)546-9129', '70350 Scofield Place', 'Bobo-Dioulasso');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (314, 4, 14, 'Wesley', 'McNelis', '94-083-6646', '46-(399)987-0321', '67551 Lindbergh Park', 'Stockholm');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (315, 5, 15, 'Adelbert', 'Hriinchenko', '91-940-7995', '1-(385)187-3198', '4 Michigan Drive', 'Saint Ann’s Bay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (316, 6, 16, 'Marissa', 'Chave', '15-105-3094', '976-(111)129-4049', '5671 Red Cloud Alley', 'Bayangol');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (317, 7, 17, 'Gui', 'Wasylkiewicz', '97-685-4009', null, '869 Ryan Way', 'Nanyang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (318, 8, 18, 'Page', 'Swadlen', '50-885-2202', '51-(868)248-2130', '6970 Fordem Place', 'Iñapari');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (319, 9, 19, 'Cybil', 'Peaple', '23-328-0117', '234-(350)181-4488', '3 Scofield Center', 'Kaiama');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (320, 10, 20, 'Ahmed', 'McDill', '02-842-1037', null, '32 Doe Crossing Junction', 'Liangcunchang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (321, 1, 21, 'Monika', 'Covendon', '02-806-2386', '62-(388)716-1032', '53511 Eastwood Parkway', 'Pawitan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (322, 2, 22, 'Baily', 'Behninck', '62-688-1527', '60-(367)653-5712', '88945 Truax Trail', 'Kangar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (323, 3, 23, 'Wadsworth', 'Kelbie', '76-444-5564', '48-(595)150-0622', '4 Longview Park', 'Mieścisko');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (324, 4, 24, 'Barn', 'Souten', '99-866-0985', null, '19 Raven Road', 'Kablukan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (325, 5, 25, 'Myrtie', 'Andrysek', '37-187-3178', null, '19 Eagan Point', 'Barra do Garças');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (326, 6, 26, 'Mareah', 'Utting', '56-558-5971', '48-(851)440-1157', '8 Gerald Road', 'Łużna');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (327, 7, 27, 'Alyce', 'Bugg', '84-649-1560', '92-(353)209-3737', '0167 Clove Point', 'Jāti');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (328, 8, 28, 'Brand', 'Di Bartolomeo', '26-233-6097', '62-(774)309-3512', '20358 Ramsey Drive', 'Harjamukti');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (329, 9, 29, 'Randal', 'Thairs', '19-908-4268', '46-(785)797-0368', '5066 Hoard Point', 'Stockholm');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (330, 10, 30, 'Roley', 'Raffels', '84-955-6824', '60-(879)678-0816', '1596 Northwestern Lane', 'Alor Setar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (331, 1, 31, 'Gunar', 'Maven', '29-436-4358', '53-(243)248-8656', '40 Talmadge Terrace', 'Bolondrón');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (332, 2, 32, 'Bram', 'Winkless', '97-426-4407', '86-(203)256-6334', '50834 Kenwood Center', 'Qingtong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (333, 3, 33, 'Marty', 'Volker', '46-101-2133', '33-(581)409-2866', '06530 Maple Wood Alley', 'Paris 01');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (334, 4, 34, 'Odo', 'Boyer', '07-974-7300', '1-(975)762-2217', '54792 Derek Way', 'Agat Village');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (335, 5, 35, 'Pamelina', 'Kemson', '40-430-9404', null, '5006 Oak Court', 'Bobadela');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (336, 6, 36, 'Tabbi', 'Dawe', '56-539-1991', '86-(241)886-9038', '7552 Hoffman Pass', 'Baidian');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (337, 7, 37, 'Mufi', 'Breinl', '54-758-8030', '63-(394)892-8214', '29700 Hoepker Lane', 'Bantogon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (338, 8, 38, 'Ronald', 'Byatt', '39-628-3804', '86-(460)326-2999', '77594 Mosinee Street', 'Dushi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (339, 9, 39, 'Fabien', 'Combes', '00-198-5361', null, '30 Troy Way', 'Gubkinskiy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (340, 10, 40, 'Murvyn', 'Heasly', '84-363-2354', '380-(929)967-0741', '02 Erie Terrace', 'Komyshuvakha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (341, 1, 41, 'Kahlil', 'Loadsman', '99-072-8542', '1-(812)138-8051', '4015 Johnson Circle', 'Jeffersonville');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (342, 2, 42, 'Jean', 'Gillbee', '84-985-8451', '48-(939)739-7986', '4 Express Street', 'Werbkowice');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (343, 3, 43, 'Brady', 'Caudwell', '80-575-3079', '63-(358)448-1767', '279 Tennyson Center', 'Bitin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (344, 4, 44, 'Elisha', 'Cornewall', '03-777-2360', '33-(251)475-3650', '0718 Graceland Plaza', 'Millau');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (345, 5, 45, 'Aldis', 'McPeck', '06-242-3187', '86-(264)778-5311', '93420 Del Mar Crossing', 'Tongguan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (346, 6, 46, 'Phaedra', 'Jagger', '93-664-0424', '63-(757)836-6350', '87 Kennedy Drive', 'Guinticgan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (347, 7, 47, 'Gerhardt', 'Dunstan', '62-136-8139', '86-(566)406-6122', '3 Vahlen Plaza', 'Changping');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (348, 8, 48, 'Cindy', 'Paradis', '31-757-4700', '52-(588)966-9043', '01 Morrow Pass', 'Pemex');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (349, 9, 49, 'Mignonne', 'Duffill', '12-743-9603', '62-(429)336-5399', '3555 Stone Corner Lane', 'Babat');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (350, 10, 50, 'Demott', 'Pitrelli', '41-860-1614', '86-(667)544-5550', '887 Washington Trail', 'Guanshan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (351, 1, 51, 'Brooke', 'Joddins', '36-911-8395', '86-(406)641-9943', '8 Eagan Lane', 'Nanlü');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (352, 2, 52, 'Aymer', 'Whitehair', '40-080-3050', '380-(830)522-9915', '2 Hudson Way', 'Sambir');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (353, 3, 53, 'Polly', 'Keymer', '95-662-7986', '86-(492)550-1531', '20 Kingsford Trail', 'Fushan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (354, 4, 54, 'Jemie', 'Burdess', '08-148-6641', null, '060 Ludington Pass', 'Gurinai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (355, 5, 55, 'Vanny', 'd''Escoffier', '66-093-0294', '62-(523)888-7404', '29147 Monica Parkway', 'Krajan Tengah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (356, 6, 56, 'Karina', 'Eastup', '30-682-4622', '1-(918)406-5313', '375 Forest Dale Terrace', 'Tulsa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (357, 7, 57, 'Marcile', 'Cornelisse', '77-598-4510', '359-(977)789-4290', '6 Brown Avenue', 'Botevgrad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (358, 8, 58, 'Colline', 'Hundal', '33-923-6940', '229-(692)784-5361', '0603 Hintze Point', 'Tanguiéta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (359, 9, 59, 'Cherice', 'Andrzej', '56-841-8721', '86-(845)733-4272', '55 Esch Circle', 'Wanling');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (360, 10, 60, 'Claresta', 'Alejo', '82-612-6910', null, '94 Butterfield Circle', 'Maundai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (361, 1, 61, 'Annamaria', 'Bimson', '04-587-9570', '63-(124)624-8782', '7628 Dovetail Court', 'Gubat');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (362, 2, 62, 'Korie', 'Ixor', '30-893-5640', '63-(547)663-9133', '0 Arkansas Road', 'Calancuasan Norte');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (363, 3, 63, 'Bryn', 'Hassey', '36-413-3118', null, '46 Basil Plaza', 'Anjō');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (364, 4, 64, 'Vinnie', 'D''Costa', '78-845-5628', null, '299 Esch Trail', 'Temryuk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (365, 5, 65, 'Lyell', 'Kenward', '19-729-2539', null, '4 Derek Alley', 'Krajan Kulon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (366, 6, 66, 'Darnall', 'Orviss', '97-116-6317', '63-(258)204-8428', '7 Shopko Court', 'Cullalabo del Sur');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (367, 7, 67, 'Goraud', 'Birney', '30-000-8624', '977-(424)668-9607', '23154 Del Sol Alley', 'Khāndbāri');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (368, 8, 68, 'Britteny', 'Heisham', '69-045-9934', null, '70422 Truax Court', 'Woja');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (369, 9, 69, 'Decca', 'Pettiford', '79-930-0833', null, '8810 Hoard Point', 'Huangcun');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (370, 10, 70, 'Torrance', 'Hammant', '69-740-9045', '86-(489)728-1092', '760 Village Plaza', 'Dalongdong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (371, 1, 71, 'Nichole', 'Hanway', '06-477-5891', '51-(494)710-3547', '819 Thompson Crossing', 'Pitumarca');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (372, 2, 72, 'Malina', 'Moston', '99-594-0531', '46-(453)986-4654', '9 Golden Leaf Junction', 'Torslanda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (373, 3, 73, 'Giraldo', 'Keslake', '48-863-8459', '51-(937)366-8323', '02847 Oak Valley Circle', 'Huaraz');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (374, 4, 74, 'Ginnie', 'Kiggel', '43-795-2921', null, '9809 Commercial Pass', 'Tucurú');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (375, 5, 75, 'Charlotte', 'Hellsdon', '87-208-5201', '55-(555)265-6836', '51456 Clove Place', 'Várzea da Palma');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (376, 6, 76, 'Colene', 'Fynan', '72-910-4778', '7-(107)624-8501', '47 Killdeer Plaza', 'Aksu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (377, 7, 77, 'Frederich', 'Gouldstraw', '66-369-0053', '52-(154)694-1787', '916 Vermont Pass', 'Lazaro Cardenas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (378, 8, 78, 'Merralee', 'Wolfart', '46-787-1228', '86-(861)697-2652', '2017 Sheridan Terrace', 'Liuqiao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (379, 9, 79, 'Hunfredo', 'Annes', '72-929-9978', '31-(327)720-8736', '0 Nevada Place', 'Nijmegen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (380, 10, 80, 'Madonna', 'Thacker', '22-244-3754', '46-(891)249-8908', '36850 Forest Run Circle', 'Östersund');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (381, 1, 81, 'Marylee', 'Vanyushkin', '89-745-5187', '86-(754)852-2799', '19709 Anzinger Center', 'Xinyi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (382, 2, 82, 'Saloma', 'Acey', '73-015-4259', '1-(812)463-6400', '09 Forest Run Way', 'Evansville');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (383, 3, 83, 'Jannel', 'Bicknell', '26-978-7829', '265-(810)302-3312', '163 Sugar Junction', 'Chipoka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (384, 4, 84, 'Decca', 'Chowne', '83-045-7606', '7-(853)427-9680', '80900 Luster Lane', 'Vypolzovo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (385, 5, 85, 'Jereme', 'Pane', '52-413-4327', '7-(484)847-8573', '22304 Rieder Alley', 'Zvenigorod');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (386, 6, 86, 'Kev', 'Blinckhorne', '45-820-1031', null, '00643 Maple Wood Trail', 'Hicksville');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (387, 7, 87, 'Fin', 'Rizzillo', '28-455-7050', null, '78 Towne Park', 'Ancenis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (388, 8, 88, 'Novelia', 'Breslau', '75-416-5636', '63-(443)899-0824', '97 North Point', 'Muñoz East');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (389, 9, 89, 'Meagan', 'Napoli', '67-568-3408', '351-(330)453-8432', '5 8th Terrace', 'Lomba do Loução');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (390, 10, 90, 'Drugi', 'Chatell', '27-617-5959', '86-(230)442-0243', '48214 Dawn Pass', 'Jinggan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (391, 1, 91, 'Urbanus', 'Wimp', '17-311-3776', null, '37656 Oneill Way', 'Radiměř');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (392, 2, 92, 'Phyllida', 'Darlasson', '03-401-1432', '51-(389)354-5313', '06 Utah Road', 'San Salvador');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (393, 3, 93, 'Ferguson', 'Severwright', '68-966-1500', '381-(710)194-2329', '835 Mallory Alley', 'Jagodina');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (394, 4, 94, 'Svend', 'Onele', '92-089-5505', null, '68 Tony Avenue', 'Gävle');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (395, 5, 95, 'Lilia', 'Vargas', '91-323-0452', '351-(324)317-8284', '441 Bellgrove Hill', 'Pisões');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (396, 6, 96, 'Tobye', 'MacEnelly', '27-652-7708', '49-(405)427-2520', '87 Nancy Plaza', 'Köln');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (397, 7, 97, 'Jeffry', 'Redmile', '54-959-7713', '242-(653)144-3158', '05557 Swallow Trail', 'Buta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (398, 8, 98, 'Cody', 'Tatum', '36-670-3816', '351-(129)324-0405', '8229 Corry Street', 'Lameiras');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (399, 9, 99, 'Giustina', 'McFfaden', '43-263-3856', '86-(296)249-4566', '0 Chinook Drive', 'Fengyang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (400, 10, 100, 'Dory', 'Bowbrick', '23-000-4810', '86-(868)768-5877', '7311 Bluejay Plaza', 'Shangma');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (401, 1, 1, 'Carlee', 'Hebson', '54-533-5751', '375-(188)577-2293', '4295 Melby Parkway', 'Vishow');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (402, 2, 2, 'Udell', 'Sirl', '23-116-0302', '86-(154)843-7502', '27 Ludington Circle', 'Qingkou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (403, 3, 3, 'Matthus', 'Hyam', '89-895-6594', '86-(884)614-4248', '80 Longview Trail', 'Henggang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (404, 4, 4, 'Ozzie', 'Driuzzi', '18-843-4337', null, '77480 Debs Avenue', 'Bauang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (405, 5, 5, 'Noam', 'Anear', '53-488-7287', '375-(674)479-3772', '37318 Mallard Park', 'Mir');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (406, 6, 6, 'Bendicty', 'Van Leeuwen', '94-062-4548', '7-(668)255-8810', '67 Cherokee Parkway', 'Samagaltay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (407, 7, 7, 'Meg', 'Tees', '42-932-1334', '51-(541)920-8492', '60 Larry Park', 'Niepos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (408, 8, 8, 'Ruggiero', 'Whiteway', '69-662-0560', '48-(801)725-5480', '7839 Park Meadow Avenue', 'Sośnie');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (409, 9, 9, 'Hanson', 'Pollak', '15-685-0152', '385-(620)851-3942', '1743 Schurz Avenue', 'Repušnica');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (410, 10, 10, 'Johan', 'D''Agostini', '23-544-6796', '64-(176)556-1662', '84 Blackbird Hill', 'Tauranga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (411, 1, 11, 'Marietta', 'Zaniolini', '71-027-7549', null, '7 Homewood Avenue', 'Montbéliard');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (412, 2, 12, 'Rolph', 'Viggers', '78-477-8844', null, '73086 Aberg Court', 'Brandfort');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (413, 3, 13, 'Carilyn', 'Redwing', '46-257-0653', null, '32 Nobel Parkway', 'Rio Largo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (414, 4, 14, 'Weston', 'Blasdale', '03-827-5459', '86-(560)309-2125', '26 Duke Drive', 'Zhangjia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (415, 5, 15, 'Gilbertina', 'Daouze', '59-181-6023', '86-(253)372-9938', '766 Northport Road', 'Shimen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (416, 6, 16, 'Jan', 'McLean', '92-731-7827', '380-(633)916-2802', '8 Briar Crest Road', 'Bezlyudivka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (417, 7, 17, 'Austina', 'Zanitti', '41-206-9921', null, '8 Parkside Place', 'Bendan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (418, 8, 18, 'Marcus', 'Mannooch', '60-430-0373', null, '193 Linden Parkway', 'Amiens');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (419, 9, 19, 'Rianon', 'Janauschek', '99-412-7240', '66-(541)699-6237', '2 Buena Vista Terrace', 'Bang Lamung');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (420, 10, 20, 'Marlyn', 'Sinkinson', '64-702-2243', '86-(580)898-7515', '6 Sunbrook Parkway', 'Chengnan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (421, 1, 21, 'Giacobo', 'Bernakiewicz', '11-102-9798', null, '479 Goodland Place', 'Bandung');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (422, 2, 22, 'Torrey', 'Jurges', '46-932-3820', null, '33 Jay Avenue', 'Renhe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (423, 3, 23, 'Krishnah', 'Gauchier', '44-985-0103', '33-(887)788-2778', '84 Kensington Center', 'Paris 08');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (424, 4, 24, 'Valeda', 'Howler', '68-145-9160', '7-(153)232-5483', '87207 Sunnyside Junction', 'Parfino');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (425, 5, 25, 'Minny', 'California', '64-099-4356', '7-(243)661-4485', '165 Kipling Way', 'Donskoy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (426, 6, 26, 'Nathanil', 'Tommis', '17-397-0676', '7-(808)617-3368', '70580 Graedel Court', 'Gorbatovka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (427, 7, 27, 'Misty', 'Cathro', '92-349-2489', '31-(568)113-0223', '383 Basil Drive', 'Enschede');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (428, 8, 28, 'Glory', 'Woodhams', '12-219-1349', null, '99943 Corry Way', 'Infonavit');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (429, 9, 29, 'Gilberto', 'Okker', '75-370-5286', '60-(750)871-4581', '382 Fairfield Crossing', 'Kuala Lumpur');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (430, 10, 30, 'Kamila', 'Caughte', '62-560-8436', '380-(211)993-3261', '93 Hauk Park', 'Ovidiopol’');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (431, 1, 31, 'Stan', 'Lazell', '36-970-5318', '54-(360)873-0625', '82450 Delladonna Park', 'Concepción del Bermejo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (432, 2, 32, 'Yovonnda', 'Cordrey', '49-612-0618', '7-(907)737-0090', '54 Huxley Court', 'Ketovo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (433, 3, 33, 'Tasia', 'Giannotti', '68-429-8674', '48-(330)320-9830', '773 Sachtjen Alley', 'Jankowice');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (434, 4, 34, 'Luelle', 'Kapelhof', '43-566-5176', '51-(133)774-1628', '09271 Summerview Street', 'Iberia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (435, 5, 35, 'Esme', 'Aylmore', '90-019-9564', null, '678 Debra Park', 'Baraçais');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (436, 6, 36, 'Sidnee', 'Madigan', '67-898-1158', '31-(280)222-1108', '6 La Follette Parkway', 'Heerlen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (437, 7, 37, 'Marie-jeanne', 'Hardwich', '35-888-1212', '1-(118)679-0528', '6 Express Parkway', 'Saint-Lambert-de-Lauzon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (438, 8, 38, 'Silva', 'Corp', '68-389-3205', null, '73255 Elmside Road', 'Opatów');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (439, 9, 39, 'Nesta', 'Sawter', '17-596-9659', null, '0 Esker Crossing', 'Perbaungan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (440, 10, 40, 'Rolph', 'Dilawey', '38-882-0121', '58-(652)651-6143', '719 Grayhawk Crossing', 'Anaco');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (441, 1, 41, 'Elora', 'Darragh', '44-020-5870', '386-(351)664-9748', '36 Norway Maple Court', 'Dobrovo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (442, 2, 42, 'Angele', 'Grissett', '63-060-6422', '32-(340)568-8283', '6245 Veith Place', 'Tournai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (443, 3, 43, 'Hurleigh', 'Wistance', '85-603-9020', null, '3341 Sundown Point', 'Kokstad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (444, 4, 44, 'Alec', 'Quene', '20-444-9552', '82-(137)563-9303', '093 Nancy Hill', 'Sindong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (445, 5, 45, 'Fara', 'Creggan', '40-746-2918', '62-(366)286-6055', '2 Quincy Circle', 'Sampangan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (446, 6, 46, 'Babette', 'Amiss', '05-362-3653', '261-(285)247-1896', '06 Linden Crossing', 'Fandriana');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (447, 7, 47, 'Rhody', 'Matteris', '08-118-2677', null, '682 Hallows Avenue', 'Vilppula');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (448, 8, 48, 'Gaultiero', 'Mizzen', '81-724-7305', '86-(502)910-0100', '76 Waxwing Street', 'Bohai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (449, 9, 49, 'Trevor', 'Bosch', '89-442-8122', null, '2546 Warrior Lane', 'Douarnenez');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (450, 10, 50, 'Enrico', 'Gladden', '50-839-7423', '62-(987)627-0712', '485 Morrow Court', 'Fuan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (451, 1, 51, 'Jeno', 'Gutch', '47-260-7961', '599-(288)573-9220', '7099 Derek Circle', 'Willemstad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (452, 2, 52, 'Levin', 'Headington', '55-410-7380', '86-(515)756-0368', '36846 Mallard Alley', 'Rongcheng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (453, 3, 53, 'Lorilee', 'Dedden', '14-206-6796', '62-(915)831-6913', '5 Norway Maple Trail', 'Cigadog');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (454, 4, 54, 'Blair', 'Johnke', '09-637-4592', '66-(420)243-5733', '11305 North Plaza', 'Ban Nong Wua So');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (455, 5, 55, 'Timmy', 'Jaycox', '26-355-6558', '86-(269)624-4501', '7492 Luster Park', 'Wanmu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (456, 6, 56, 'Vina', 'Peteri', '97-679-7236', '33-(269)833-1870', '79 Acker Point', 'Landivisiau');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (457, 7, 57, 'Fayina', 'McIlory', '20-998-2196', '7-(507)997-0094', '234 Eastlawn Lane', 'Uglich');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (458, 8, 58, 'Jerrie', 'Serchwell', '18-076-4369', '86-(479)343-6056', '724 Iowa Plaza', 'Mulan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (459, 9, 59, 'Conway', 'Millwall', '59-319-2905', '593-(717)131-1791', '761 Westridge Street', 'Naranjito');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (460, 10, 60, 'Lay', 'Briggdale', '69-574-8238', '86-(832)741-3770', '79 Park Meadow Junction', 'Shanxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (461, 1, 61, 'Lira', 'Jiri', '55-426-9428', '55-(735)771-8853', '2 Corscot Point', 'José de Freitas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (462, 2, 62, 'Antony', 'Brislan', '74-683-5145', '420-(380)785-9026', '611 Graceland Junction', 'Železná Ruda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (463, 3, 63, 'Ariella', 'Baldini', '88-474-0608', null, '45 Mallory Crossing', 'Nagareyama');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (464, 4, 64, 'Perri', 'Hansed', '44-807-9869', '48-(428)581-3219', '1958 Dawn Pass', 'Sulechów');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (465, 5, 65, 'Araldo', 'Seymour', '76-986-6554', '86-(977)416-4064', '0 Oak Terrace', 'Batan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (466, 6, 66, 'Roch', 'Cullinan', '12-448-6073', null, '12 Farmco Way', 'Chame');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (467, 7, 67, 'Vin', 'Barham', '32-769-7367', '7-(811)630-6637', '319 Namekagon Pass', 'Reshetikha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (468, 8, 68, 'Godfree', 'Slinn', '71-305-3385', '86-(464)488-3859', '6448 Lakewood Gardens Hill', 'Gangba');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (469, 9, 69, 'Ilysa', 'Ciccarelli', '53-714-6499', '355-(313)488-1003', '3 Lillian Point', 'Kryevidh');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (470, 10, 70, 'Cece', 'Joust', '43-505-7869', '86-(267)911-7129', '2 Arrowood Road', 'Cangqian');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (471, 1, 71, 'Jaime', 'Habbeshaw', '50-576-1844', '86-(585)549-2160', '3 School Terrace', 'Hepu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (472, 2, 72, 'Joyan', 'Roskell', '92-123-6603', '86-(429)511-4279', '03431 Delladonna Pass', 'Wuyanquan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (473, 3, 73, 'Brew', 'Northin', '18-185-2909', '299-(373)207-2671', '556 Southridge Trail', 'Aasiaat');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (474, 4, 74, 'Barnabas', 'Yekel', '19-113-0036', '81-(320)355-5414', '70099 Riverside Trail', 'Narutō');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (475, 5, 75, 'Gaelan', 'Barbier', '76-704-7762', '62-(546)799-8743', '36 Steensland Pass', 'Longka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (476, 6, 76, 'Micheil', 'Tootal', '32-478-4437', '55-(250)953-5001', '604 Green Ridge Junction', 'Niterói');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (477, 7, 77, 'Oliy', 'Glancy', '51-824-2318', '46-(756)262-5776', '5 Twin Pines Circle', 'Spånga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (478, 8, 78, 'Burty', 'Henden', '69-077-5878', null, '97932 Crowley Plaza', 'Las Vegas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (479, 9, 79, 'Lida', 'Dinjes', '15-280-0114', '1-(323)430-8909', '63365 Grayhawk Drive', 'Los Angeles');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (480, 10, 80, 'Noemi', 'Battams', '14-907-3948', '62-(138)457-9361', '3 Aberg Place', 'Banjar Pasekan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (481, 1, 81, 'Cirstoforo', 'Fiske', '26-875-8374', '62-(149)574-4078', '48242 Thompson Pass', 'Jayaraga Kaler');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (482, 2, 82, 'Berne', 'Licquorish', '17-103-3240', '86-(236)472-2414', '8 Gina Alley', 'Diaoling');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (483, 3, 83, 'Brockie', 'Cossam', '25-358-6193', '86-(396)389-5559', '02308 Amoth Court', 'A’ershan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (484, 4, 84, 'Uri', 'McNea', '97-337-4280', '30-(348)462-5876', '06 Dorton Court', 'Xinó Neró');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (485, 5, 85, 'Natty', 'Bienvenu', '60-934-0954', '86-(605)348-5964', '500 Graedel Avenue', 'Jiujiang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (486, 6, 86, 'Grove', 'Cottham', '80-189-9402', null, '0 Longview Way', 'Smārde');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (487, 7, 87, 'Orin', 'Goodie', '48-365-0159', '48-(432)350-6345', '8 Pond Plaza', 'Niedrzwica Duża');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (488, 8, 88, 'Noel', 'Brach', '16-815-8320', null, '0940 Mallard Circle', 'Abbotsford');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (489, 9, 89, 'Susan', 'Jerrard', '96-481-2981', '86-(660)925-7378', '2 Burrows Way', 'Zhuangxing');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (490, 10, 90, 'Minna', 'McAvaddy', '52-744-6839', '380-(138)965-3026', '488 Trailsway Junction', 'Dniprodzerzhyns’k');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (491, 1, 91, 'Tudor', 'Quixley', '00-442-4957', '33-(443)704-9044', '55 Cody Alley', 'Villeurbanne');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (492, 2, 92, 'Sean', 'Ullett', '82-862-8770', null, '1 Fordem Crossing', 'Dongcheng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (493, 3, 93, 'Darryl', 'Caulton', '70-673-4524', null, '003 Corscot Pass', 'Huangduobu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (494, 4, 94, 'Paten', 'Korous', '44-389-7291', '86-(228)118-2346', '02638 Fieldstone Drive', 'Waihai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (495, 5, 95, 'Ulberto', 'Stoyles', '50-322-8835', '242-(329)730-9104', '845 International Lane', 'Goma');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (496, 6, 96, 'Ardra', 'Gherarducci', '41-789-4894', null, '5 American Ash Street', 'Wolowiro');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (497, 7, 97, 'Effie', 'Gierck', '09-609-5164', '62-(922)379-8567', '21121 Stephen Road', 'Malimaneek');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (498, 8, 98, 'Errol', 'Paz', '11-496-8463', null, '8376 Springview Crossing', 'Fonte da Aldeia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (499, 9, 99, 'Vittorio', 'Cracie', '36-521-0448', '86-(921)879-3301', '9105 Express Way', 'Dongyuan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (500, 10, 100, 'Jeri', 'Yellowley', '00-878-7035', '62-(903)659-8259', '6604 Vahlen Road', 'Nangahale');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (501, 1, 1, 'Marielle', 'Pierrepont', '14-359-8948', '86-(658)294-6205', '18366 Hollow Ridge Road', 'Longping');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (502, 2, 2, 'Lorene', 'Cartmell', '51-002-8558', '385-(727)727-7801', '7 Crest Line Lane', 'Veliki Grđevac');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (503, 3, 3, 'Alanah', 'Sowte', '26-028-3879', null, '36 Elgar Road', 'Samoš');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (504, 4, 4, 'Clark', 'Lange', '30-168-3837', '7-(358)120-7340', '12 Rusk Alley', 'Krasnyy Oktyabr’');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (505, 5, 5, 'Bondon', 'Mayers', '98-644-8709', null, '25 Washington Park', 'Tashkent');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (506, 6, 6, 'Costanza', 'Erratt', '58-523-6382', '66-(139)973-8114', '2786 Vermont Street', 'Khon Kaen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (507, 7, 7, 'Traver', 'Leggen', '58-825-6925', '62-(389)277-8299', '6767 Meadow Valley Plaza', 'Ciherang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (508, 8, 8, 'Darby', 'Swendell', '94-860-3281', null, '74284 Bartelt Alley', 'Fuenlabrada');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (509, 9, 9, 'Marlo', 'Leyband', '06-796-5049', '7-(788)964-8666', '3812 Katie Alley', 'Kochenëvo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (510, 10, 10, 'Taddeusz', 'Jozef', '39-583-6020', '358-(138)217-9843', '831 Becker Avenue', 'Kalvola');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (511, 1, 11, 'Christabella', 'Beecraft', '26-558-0973', '27-(318)867-9500', '7 Scoville Road', 'Mondlo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (512, 2, 12, 'Killie', 'Glasser', '48-162-3092', '375-(698)545-3101', '6 Tennyson Center', 'Zel’va');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (513, 3, 13, 'Myrlene', 'Dibsdale', '20-989-3560', '245-(882)103-0600', '56 Luster Street', 'Quebo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (514, 4, 14, 'Rhetta', 'De Lisle', '58-407-7887', null, '21 Talisman Plaza', 'Xiayang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (515, 5, 15, 'Wakefield', 'Deakes', '90-019-7270', '7-(711)620-8012', '7 Weeping Birch Hill', 'Alpatovo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (516, 6, 16, 'Willow', 'Le Hucquet', '97-172-5150', null, '50 Thierer Plaza', 'Bantry');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (517, 7, 17, 'Elane', 'Kuhnhardt', '53-671-2116', '86-(422)228-6869', '93 Vera Drive', 'Huangfang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (518, 8, 18, 'Marlowe', 'Yanson', '41-455-6940', '358-(641)297-1079', '77804 Starling Point', 'Pukkila');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (519, 9, 19, 'Maxie', 'Schollar', '89-160-7174', '55-(335)727-0505', '19 Westport Alley', 'Mauá');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (520, 10, 20, 'Sheilakathryn', 'Lyburn', '78-343-0109', null, '6 American Avenue', 'München');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (521, 1, 21, 'Carter', 'Softley', '36-956-2888', '54-(920)705-9677', '95557 Quincy Terrace', 'San Antonio Oeste');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (522, 2, 22, 'Toni', 'Dellenbrok', '45-692-9966', '46-(304)202-9298', '890 Erie Place', 'Stockholm');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (523, 3, 23, 'Tades', 'Massenhove', '06-717-0488', '62-(359)742-2824', '6 Village Green Hill', 'Langsa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (524, 4, 24, 'Randee', 'Primarolo', '51-714-3557', '86-(247)605-9286', '74 Browning Parkway', 'Xiaozhoushan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (525, 5, 25, 'Tami', 'Petheridge', '97-984-4284', null, '81679 Reindahl Way', 'Guisser');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (526, 6, 26, 'Clara', 'Dionisetti', '25-004-6096', '86-(992)319-9043', '3 Pond Avenue', 'Nalinggou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (527, 7, 27, 'Ramona', 'Dudbridge', '76-239-9449', '63-(819)813-3147', '20 Arkansas Plaza', 'Santa Clara');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (528, 8, 28, 'Clemmie', 'Hehir', '63-785-4028', '63-(559)408-4934', '21 Sage Drive', 'Leon Postigo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (529, 9, 29, 'Burg', 'Huband', '94-670-4895', null, '27 Dexter Pass', 'Sumber Tengah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (530, 10, 30, 'Shell', 'O''Day', '88-040-7969', null, '3 Mcguire Plaza', 'Buçimas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (531, 1, 31, 'Emilio', 'Orehead', '98-350-4775', '352-(670)118-2375', '87 Texas Avenue', 'Kopstal');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (532, 2, 32, 'Veradis', 'Vasyukhin', '11-686-9160', null, '5931 Fieldstone Alley', 'Mtambile');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (533, 3, 33, 'Bibbye', 'Bellefant', '21-210-1130', '86-(911)759-0707', '194 Schiller Drive', 'Shanggu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (534, 4, 34, 'Idelle', 'Waterfall', '20-576-1375', '380-(120)933-7990', '63 Milwaukee Drive', 'Tomakivka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (535, 5, 35, 'Modesty', 'Bruni', '15-204-3592', '30-(603)137-7289', '5 Lake View Court', 'Vélo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (536, 6, 36, 'Westley', 'Wheldon', '95-596-3021', null, '570 Schurz Lane', 'Bobojong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (537, 7, 37, 'Juli', 'Daly', '06-886-8872', '86-(516)413-0421', '49 Upham Plaza', 'Zhaxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (538, 8, 38, 'Olvan', 'Heikkinen', '47-285-4730', '63-(719)668-5143', '67 Sunbrook Alley', 'Camaligan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (539, 9, 39, 'Basia', 'Beacham', '13-332-8841', '66-(565)398-9956', '7133 Dottie Crossing', 'Chang Klang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (540, 10, 40, 'Kristin', 'Hainsworth', '06-009-5789', '56-(704)498-7224', '1065 Doe Crossing Center', 'La Laja');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (541, 1, 41, 'Ethyl', 'Brunone', '49-310-8874', null, '04951 Swallow Hill', 'Hrušica');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (542, 2, 42, 'Ramsay', 'Christoffe', '70-160-3331', '509-(709)460-2159', '160 Butternut Center', 'Anse à Galets');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (543, 3, 43, 'Christean', 'Slocom', '61-296-3169', '86-(743)399-8414', '823 Monument Pass', 'Huomachong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (544, 4, 44, 'Sammy', 'Guidi', '55-261-3800', '62-(472)572-0649', '0901 Havey Plaza', 'Muting');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (545, 5, 45, 'Marya', 'Cosstick', '09-062-1177', '86-(328)870-6751', '4 Walton Street', 'Huangbei');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (546, 6, 46, 'Hebert', 'Lethem', '80-892-2165', null, '98296 Orin Drive', 'Tarnowskie Góry');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (547, 7, 47, 'Lily', 'Spiers', '75-068-0234', null, '842 Stephen Parkway', 'Chuantang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (548, 8, 48, 'Wendel', 'Please', '72-392-1831', '86-(103)882-1486', '06 Autumn Leaf Place', 'Xinhang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (549, 9, 49, 'Chantal', 'Creeber', '89-908-4658', '351-(980)617-2292', '490 Moulton Terrace', 'Póvoa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (550, 10, 50, 'Rip', 'Berrecloth', '57-347-2094', '51-(553)571-1234', '90209 Lakeland Place', 'Huancabamba');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (551, 1, 51, 'Kaylil', 'Cockshoot', '20-030-7581', '62-(926)117-9263', '40 Pine View Hill', 'Parse');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (552, 2, 52, 'Katy', 'Trevain', '06-957-1021', '86-(422)941-5548', '0567 Mitchell Court', 'Lukou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (553, 3, 53, 'Jammie', 'Bauldry', '52-840-7575', '1-(813)421-2824', '23816 Jay Drive', 'Zephyrhills');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (554, 4, 54, 'Rubie', 'Schruyers', '20-741-2684', '355-(265)413-3672', '96517 Harbort Junction', 'Shkodër');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (555, 5, 55, 'Sarajane', 'Batter', '99-696-7439', '420-(252)530-7838', '7 Esker Hill', 'Všeruby');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (556, 6, 56, 'Emlyn', 'Dallewater', '01-349-1864', '380-(552)255-3855', '0902 Warbler Alley', 'Komyshany');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (557, 7, 57, 'Karalee', 'Portwaine', '51-938-5992', null, '789 Fair Oaks Drive', 'Matango');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (558, 8, 58, 'Laurena', 'Winchcum', '16-920-9942', null, '32367 Lillian Plaza', 'Khlong Toei');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (559, 9, 59, 'Nolana', 'Strettell', '27-255-6885', '62-(414)550-6690', '9234 Bluestem Hill', 'Warugunung');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (560, 10, 60, 'Olivero', 'Medcraft', '75-777-1929', '351-(292)304-7407', '963 Washington Lane', 'Vale de Açores');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (561, 1, 61, 'Pryce', 'Fadian', '45-854-5978', null, '90 Ryan Lane', 'Palana');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (562, 2, 62, 'Delinda', 'Giurio', '16-808-4114', '7-(984)483-6056', '74 Clyde Gallagher Junction', 'Bystryanka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (563, 3, 63, 'Brigitta', 'Croad', '59-467-3974', null, '5 Pleasure Drive', 'Banjar Kelodan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (564, 4, 64, 'Neill', 'Springtorp', '95-461-8135', '7-(350)252-3841', '973 Killdeer Avenue', 'Tomilino');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (565, 5, 65, 'Aubine', 'Ough', '07-588-5962', null, '84173 Hudson Junction', 'Namyang-dong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (566, 6, 66, 'Janel', 'Trusse', '52-459-4948', '55-(762)927-7539', '981 Debra Park', 'Mirandopólis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (567, 7, 67, 'Roanne', 'Tessier', '37-652-7133', null, '3961 Canary Pass', 'Fulu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (568, 8, 68, 'Ole', 'de Verson', '53-711-3359', '51-(156)342-2771', '88 Petterle Place', 'Yuyapichis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (569, 9, 69, 'Randie', 'Kippen', '43-363-3653', '55-(278)768-7955', '3 Arapahoe Drive', 'Camaquã');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (570, 10, 70, 'Shelley', 'Marushak', '83-324-8027', '351-(477)943-2869', '27586 Dawn Drive', 'Monte de Trigo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (571, 1, 71, 'Hunter', 'Kulver', '74-816-1701', '689-(393)244-9160', '18501 Charing Cross Circle', 'Papetoai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (572, 2, 72, 'Burke', 'MacKaig', '14-726-5817', '62-(606)925-8533', '3 Hermina Drive', 'Raejeru');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (573, 3, 73, 'Andi', 'Hallock', '50-575-8863', '63-(145)888-8950', '616 New Castle Junction', 'Quinipot');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (574, 4, 74, 'Tadio', 'Cogle', '23-227-1473', '351-(611)114-2915', '744 Milwaukee Crossing', 'Cimo de Vila');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (575, 5, 75, 'Lorraine', 'Blofeld', '00-104-6008', '48-(482)319-6559', '94 Eagle Crest Center', 'Kosakowo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (576, 6, 76, 'Maxie', 'Govern', '52-277-7368', '62-(990)123-6298', '3885 Old Shore Crossing', 'Dorupare');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (577, 7, 77, 'Jory', 'Antonellini', '50-816-4912', '54-(753)433-4230', '624 Ridge Oak Place', 'San Javier');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (578, 8, 78, 'Vale', 'Rhydderch', '37-448-7380', '86-(719)116-6516', '987 Pearson Road', 'Xue’an');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (579, 9, 79, 'Emiline', 'Dreelan', '67-745-1456', '57-(724)253-8133', '2 6th Drive', 'Puerto Boyacá');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (580, 10, 80, 'Tobiah', 'Bonallick', '64-482-8807', '62-(727)448-3486', '44942 Killdeer Hill', 'Kurungannyawa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (581, 1, 81, 'Daloris', 'Bonney', '84-973-4461', '86-(823)581-1123', '14816 Merrick Street', 'Yuanhou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (582, 2, 82, 'Tymothy', 'Groves', '69-470-5966', '685-(876)742-4425', '73 Jenifer Crossing', 'Gataivai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (583, 3, 83, 'Janella', 'Misk', '88-585-8280', null, '017 Johnson Circle', 'Benešov nad Černou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (584, 4, 84, 'Astra', 'Rivaland', '48-527-6265', '86-(507)179-3827', '303 Continental Street', 'Changfeng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (585, 5, 85, 'Meryl', 'Podd', '65-475-3588', '33-(320)674-5921', '828 Charing Cross Crossing', 'Orange');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (586, 6, 86, 'Gallard', 'Fillis', '08-788-8144', '51-(238)843-1437', '4 Old Gate Way', 'Pacucha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (587, 7, 87, 'Sabina', 'Knivett', '15-325-9254', null, '260 Dayton Junction', 'New Orleans');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (588, 8, 88, 'Shepherd', 'Rablan', '06-229-4978', '86-(846)315-9791', '7994 Namekagon Hill', 'Zhonghualu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (589, 9, 89, 'Lian', 'Pringer', '65-129-5546', null, '97 Anniversary Court', 'Annapolis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (590, 10, 90, 'Hildegaard', 'Bridle', '93-366-1910', '86-(775)431-2358', '998 Lakewood Gardens Plaza', 'Bailu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (591, 1, 91, 'Findley', 'Brade', '09-026-8661', '86-(817)933-9847', '7041 Rusk Place', 'Sangke');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (592, 2, 92, 'Inessa', 'Aughton', '14-529-4065', null, '1 Barnett Place', 'Dongzaogang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (593, 3, 93, 'Rance', 'Becom', '59-023-7178', null, '75 Carpenter Alley', 'Jiahe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (594, 4, 94, 'Isidore', 'Larman', '72-971-9252', '55-(629)484-4311', '8 Utah Junction', 'Rosário do Sul');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (595, 5, 95, 'Everard', 'Harridge', '84-366-3074', '1-(714)315-4512', '712 Clyde Gallagher Road', 'Anaheim');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (596, 6, 96, 'Orelie', 'Zorn', '94-958-3774', '964-(933)740-1451', '8 Longview Alley', 'Al Başrah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (597, 7, 97, 'Miof mela', 'Burcombe', '86-036-9246', '62-(767)672-3777', '59674 Express Hill', 'Sumbersarikrajan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (598, 8, 98, 'Timothee', 'Litterick', '23-372-2077', '63-(101)916-5925', '70590 Larry Alley', 'Salcedo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (599, 9, 99, 'Bevon', 'Dany', '82-838-3856', '62-(867)626-9477', '36 Mendota Drive', 'Higetegera');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (600, 10, 100, 'Brigitta', 'Kirimaa', '48-351-8294', '1-(714)917-2967', '343 Amoth Trail', 'Huntington Beach');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (601, 1, 1, 'Kamila', 'Rowell', '48-629-0419', '98-(941)415-6546', '7974 Blaine Road', 'Yazd');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (602, 2, 2, 'Alvinia', 'Catteroll', '47-682-9884', '7-(829)619-6126', '9 Red Cloud Avenue', 'Zamoskvorech’ye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (603, 3, 3, 'Jazmin', 'Swaite', '68-454-4472', '54-(689)517-9554', '00 Kenwood Terrace', 'Arias');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (604, 4, 4, 'Ajay', 'Gilbank', '10-317-9845', '387-(559)611-5124', '8 Arapahoe Avenue', 'Svojat');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (605, 5, 5, 'Sammy', 'Howick', '54-234-2109', '27-(408)295-7768', '63 Maple Drive', 'Eshowe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (606, 6, 6, 'Sophey', 'Kollach', '09-898-6445', null, '08 John Wall Crossing', 'Inazawa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (607, 7, 7, 'Meredithe', 'Simonitto', '99-338-1175', null, '1 Chinook Plaza', 'Wailolong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (608, 8, 8, 'Katlin', 'Shelborne', '53-670-2735', '1-(303)842-1164', '9188 Laurel Street', 'Englewood');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (609, 9, 9, 'Collen', 'Giacomazzo', '18-698-0958', '212-(335)858-7739', '795 Saint Paul Plaza', 'Saddina');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (610, 10, 10, 'Curry', 'Houlden', '72-425-6909', null, '796 Anhalt Terrace', 'Mo I Rana');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (611, 1, 11, 'Mortimer', 'Combe', '31-318-2837', '351-(632)144-6328', '6979 Tony Center', 'Bouças');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (612, 2, 12, 'Maiga', 'Lamperd', '55-353-4487', '95-(647)356-4242', '921 Sunbrook Alley', 'Myingyan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (613, 3, 13, 'Werner', 'McCart', '06-334-3326', '7-(347)168-0182', '4510 Green Circle', 'Podkumskiy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (614, 4, 14, 'Hiram', 'Classen', '54-742-8656', '995-(684)935-4103', '25 Prentice Drive', 'Zhinvali');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (615, 5, 15, 'Wilmette', 'Minall', '50-705-3532', '63-(898)836-2703', '7347 Scofield Road', 'Tagana-an');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (616, 6, 16, 'Dannie', 'Hollier', '47-930-3288', null, '99 Morningstar Alley', 'Kalmar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (617, 7, 17, 'Adolphus', 'Kilborn', '54-615-4431', null, '15161 Portage Street', 'Hluhluwe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (618, 8, 18, 'Mikol', 'McKeurtan', '21-302-3967', '62-(262)745-1485', '45677 Bunker Hill Pass', 'Kiuteta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (619, 9, 19, 'Camile', 'Maciejak', '98-236-4480', null, '3292 Carioca Alley', 'Waldbillig');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (620, 10, 20, 'Dillie', 'Scoone', '54-129-1749', '420-(155)247-5112', '9 Maple Pass', 'Rokycany');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (621, 1, 21, 'Kai', 'Gonnet', '78-597-2982', '55-(457)986-0143', '10 Hovde Street', 'Jacobina');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (622, 2, 22, 'Bryana', 'Ionesco', '14-748-6077', '55-(767)341-5413', '0212 Vermont Crossing', 'Forquilhinha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (623, 3, 23, 'Melita', 'Leaver', '73-450-5440', '48-(863)147-2163', '5334 Red Cloud Crossing', 'Żarki-Letnisko');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (624, 4, 24, 'Sharai', 'Faraday', '50-334-0696', '420-(784)363-7591', '32 Beilfuss Plaza', 'Stochov');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (625, 5, 25, 'Ellswerth', 'MacAvaddy', '00-402-3604', '62-(416)501-6944', '8477 Acker Road', 'Krajan Dua Padomasan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (626, 6, 26, 'Shirline', 'Lightning', '11-315-3230', null, '9 Gina Road', 'Chalon-sur-Saône');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (627, 7, 27, 'Tony', 'Chilcotte', '29-010-7438', '66-(935)825-1999', '90668 Roxbury Lane', 'Phu Kradueng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (628, 8, 28, 'Sheree', 'Toke', '96-065-8142', '998-(672)452-5779', '4 Browning Drive', 'Xo’jayli Shahri');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (629, 9, 29, 'Welsh', 'Barthrup', '86-038-8068', '352-(702)774-4277', '79747 International Parkway', 'Bergem');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (630, 10, 30, 'Crichton', 'Tremblot', '34-733-0171', '58-(281)425-9712', '04642 Division Parkway', 'San José de Barlovento');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (631, 1, 31, 'Ragnar', 'Hymans', '55-149-3330', null, '9617 Sycamore Way', 'Woro');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (632, 2, 32, 'Colene', 'Vint', '50-100-2687', '234-(361)781-9382', '7485 Garrison Place', 'Gwadabawa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (633, 3, 33, 'Uriah', 'Halsworth', '36-792-4711', '62-(521)868-9017', '2 La Follette Crossing', 'Yosowilangun');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (634, 4, 34, 'Winna', 'Philips', '98-256-5554', '86-(133)529-0787', '11036 Chinook Parkway', 'Darya Boyi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (635, 5, 35, 'Thekla', 'Pilgrim', '11-912-9675', null, '42832 Clove Circle', 'Vysehrad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (636, 6, 36, 'Pietrek', 'Emanuelli', '36-477-5664', '52-(161)743-5530', '1 Loftsgordon Crossing', 'San Agustin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (637, 7, 37, 'Robin', 'Kneeshaw', '44-460-2197', '63-(801)551-6069', '5079 Waywood Place', 'Cut-cut Primero');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (638, 8, 38, 'Elfreda', 'Atcheson', '31-905-4682', '86-(387)122-9042', '360 Dawn Circle', 'Shuangxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (639, 9, 39, 'Verne', 'Print', '35-853-0978', null, '1 Elka Court', 'Batas Barat');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (640, 10, 40, 'Fifine', 'Sisnett', '51-879-7188', '963-(533)226-5184', '64 Shasta Plaza', 'Al Fākhūrah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (641, 1, 41, 'Dallas', 'Scolding', '20-983-6499', '1-(810)754-0214', '34 Ilene Road', 'Abraham’s Bay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (642, 2, 42, 'Waverley', 'Dolohunty', '53-377-0134', '51-(531)438-2513', '9 Rigney Avenue', 'Churubamba');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (643, 3, 43, 'Lissa', 'Croser', '67-432-5924', '63-(785)841-3022', '64 Roxbury Plaza', 'La Hacienda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (644, 4, 44, 'Madlen', 'Bilfoot', '66-391-2026', '593-(317)904-9190', '53262 Sauthoff Park', 'Cayambe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (645, 5, 45, 'Fannie', 'Cosgry', '97-713-4262', '992-(616)503-0526', '4 Division Road', 'Qalaikhumb');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (646, 6, 46, 'Daniella', 'Royl', '53-056-5462', '1-(619)385-0546', '9 Springview Parkway', 'San Diego');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (647, 7, 47, 'Sylvia', 'MacDermott', '98-124-0972', '62-(212)742-5429', '2 Sheridan Avenue', 'Bojongsarung');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (648, 8, 48, 'Silvio', 'Iacovone', '06-451-3245', '976-(433)938-0208', '995 Bartillon Drive', 'Haylaastay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (649, 9, 49, 'Brook', 'Rillett', '60-427-8593', '234-(296)513-7486', '11598 Messerschmidt Junction', 'Dan Sadau');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (650, 10, 50, 'Carny', 'Lukesch', '34-746-1361', null, '41253 Little Fleur Circle', 'Campurrejo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (651, 1, 51, 'Tamar', 'Umpleby', '70-895-8890', '63-(998)910-5644', '6802 Macpherson Hill', 'Paradahan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (652, 2, 52, 'Kassie', 'Vedishchev', '34-258-3006', null, '6 Coleman Place', 'Hejia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (653, 3, 53, 'Florentia', 'Greste', '73-459-7457', '255-(980)517-8747', '9122 Scoville Drive', 'Magomeni');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (654, 4, 54, 'Della', 'Lazare', '82-992-8867', '81-(155)339-8765', '3 Sycamore Way', 'Kariya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (655, 5, 55, 'Junia', 'Ioannou', '57-116-7813', '60-(897)258-7195', '7571 Susan Alley', 'Pulau Pinang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (656, 6, 56, 'Bordie', 'Jurczik', '31-074-5752', '48-(318)488-8853', '1648 Gina Plaza', 'Stalowa Wola');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (657, 7, 57, 'Britteny', 'Michelet', '55-588-0360', '86-(793)901-7416', '1 Bartelt Terrace', 'Haolin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (658, 8, 58, 'Erastus', 'Garling', '35-452-9272', null, '2 Fordem Park', 'Qinglian');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (659, 9, 59, 'Marianne', 'Lowson', '66-625-6627', '86-(611)345-7233', '35 Truax Parkway', 'Yinla');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (660, 10, 60, 'Jobey', 'Stratz', '57-509-4659', '62-(646)142-8875', '0 Spenser Avenue', 'Doko');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (661, 1, 61, 'Hilliard', 'Cordle', '86-120-9708', '86-(747)892-0342', '22 Golf Course Street', 'Liupai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (662, 2, 62, 'Renaldo', 'Burtonwood', '89-969-5526', '7-(193)856-5477', '9 Dixon Hill', 'Novotitarovskaya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (663, 3, 63, 'Cort', 'Beeck', '20-057-9887', '7-(822)572-3253', '8 Nobel Center', 'Karpushikha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (664, 4, 64, 'Hillery', 'Gillio', '38-101-6845', '86-(183)500-3586', '4 Hayes Plaza', 'Heqiao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (665, 5, 65, 'Westleigh', 'Stanway', '78-512-1873', null, '28 Maple Wood Park', 'Ush-Tyube');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (666, 6, 66, 'Tarra', 'Paulisch', '52-504-2874', null, '00816 Raven Way', 'Hanban');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (667, 7, 67, 'Derry', 'Snodin', '07-792-4607', '30-(622)915-0841', '30776 Surrey Way', 'Keratéa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (668, 8, 68, 'Reinaldos', 'Audsley', '63-512-2263', '55-(196)932-2935', '8 Monica Plaza', 'Tonantins');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (669, 9, 69, 'Halli', 'Glazebrook', '13-635-2286', null, '210 Village Junction', 'Kirawsk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (670, 10, 70, 'Eric', 'Norsister', '16-176-6708', null, '0 Rusk Way', 'El Zapotal del Norte');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (671, 1, 71, 'Kass', 'Gerrit', '93-290-9354', '598-(795)154-4806', '223 Lotheville Circle', 'Young');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (672, 2, 72, 'Filide', 'Havik', '79-049-5089', '58-(970)755-2417', '0069 Clove Junction', 'Cúa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (673, 3, 73, 'Nestor', 'Hecks', '87-068-8831', '55-(650)135-7117', '54862 Luster Avenue', 'Conceição das Alagoas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (674, 4, 74, 'Brent', 'Keaysell', '11-289-5020', '33-(948)908-6575', '2 Ridgeview Plaza', 'Périgueux');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (675, 5, 75, 'Bebe', 'Janic', '73-272-6703', '380-(430)611-5219', '9199 Kingsford Plaza', 'Dokuchayevs’k');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (676, 6, 76, 'Florida', 'Smalridge', '31-530-7212', '972-(663)399-0218', '50763 Erie Court', 'Kfar NaOranim');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (677, 7, 77, 'Mabelle', 'Wetherell', '85-666-9465', null, '8 Forest Drive', 'Poyang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (678, 8, 78, 'Batholomew', 'Kilby', '10-090-2024', null, '96281 Victoria Center', 'Landerneau');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (679, 9, 79, 'Nessie', 'Baltzar', '56-732-4921', '62-(589)881-7108', '5 Judy Junction', 'Tungguwaneng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (680, 10, 80, 'Waylon', 'Graffin', '72-387-5912', '48-(258)551-4609', '0 Lyons Lane', 'Purda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (681, 1, 81, 'Chere', 'O''Hear', '85-523-6709', '216-(506)917-1540', '5 Blue Bill Park Lane', 'Al Marsá');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (682, 2, 82, 'Roosevelt', 'Cracie', '53-468-5153', '352-(770)464-1888', '728 Shelley Center', 'Moutfort');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (683, 3, 83, 'Dylan', 'Essame', '75-580-6210', null, '06 Forest Dale Place', 'Cergy-Pontoise');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (684, 4, 84, 'Margit', 'Kenward', '67-607-2561', '86-(150)195-8393', '4419 Lighthouse Bay Junction', 'Qiaotou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (685, 5, 85, 'Clarie', 'Brounsell', '63-654-8017', null, '047 Buell Drive', 'Storvreta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (686, 6, 86, 'Val', 'Fremantle', '98-307-8739', '63-(876)279-1777', '9562 Novick Trail', 'Esperanza');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (687, 7, 87, 'Othilie', 'Simms', '42-914-5000', '351-(182)183-0781', '013 Eagle Crest Alley', 'A-da-Gorda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (688, 8, 88, 'Vidovik', 'Stannah', '99-247-9691', null, '5 Sullivan Alley', 'Al Karmil');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (689, 9, 89, 'Yasmin', 'Nunnery', '61-388-2867', '355-(301)707-6478', '647 Menomonie Hill', 'Zall-Herr');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (690, 10, 90, 'Mala', 'Breheny', '08-920-2604', '48-(952)821-7623', '67163 Armistice Court', 'Sosnówka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (691, 1, 91, 'Josselyn', 'McLese', '39-983-0416', null, '7 Village Green Lane', 'Yangjiapo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (692, 2, 92, 'Minne', 'Burnhams', '48-703-6322', '359-(882)761-8004', '28794 High Crossing Center', 'Velingrad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (693, 3, 93, 'Laurent', 'Gravenell', '69-248-7254', null, '95570 Independence Drive', 'Ash Sharyah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (694, 4, 94, 'Selby', 'Cowderoy', '72-537-4762', '351-(124)642-4150', '47266 Barnett Avenue', 'Carapelhos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (695, 5, 95, 'Norby', 'Hindes', '73-138-9638', '351-(901)243-1079', '384 Carpenter Drive', 'Cimo de Vila');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (696, 6, 96, 'Evered', 'Sibylla', '69-914-9917', '63-(480)993-0502', '24 Lillian Place', 'Unidos');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (697, 7, 97, 'Tedman', 'Ainsley', '88-419-4430', null, '80 La Follette Road', 'Philadelphia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (698, 8, 98, 'Salvador', 'Devonish', '05-857-9627', '86-(514)993-2720', '2812 Sloan Lane', 'Shiren');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (699, 9, 99, 'Aindrea', 'Pitkin', '55-218-3658', '33-(233)721-3167', '949 Lukken Plaza', 'Chantilly');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (700, 10, 100, 'Delano', 'Meddemmen', '49-755-2929', '237-(221)784-1513', '106 Susan Point', 'Mbouda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (701, 1, 1, 'Crysta', 'Errey', '02-960-9627', '252-(585)814-4381', '785 Lawn Trail', 'Berbera');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (702, 2, 2, 'Ingra', 'McMichan', '40-736-1328', null, '36 Superior Terrace', 'Vilque Chico');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (703, 3, 3, 'Seward', 'Muddle', '67-728-7428', '92-(727)170-0566', '187 Ridgeview Avenue', 'Tānk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (704, 4, 4, 'Nerty', 'Songhurst', '97-199-4555', '86-(315)790-2389', '70235 Clyde Gallagher Plaza', 'Xiaji');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (705, 5, 5, 'Vale', 'Wellington', '15-357-2095', '63-(953)138-7057', '7 Toban Place', 'Linmansangan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (706, 6, 6, 'Jakie', 'Death', '13-476-1697', null, '83 Kim Parkway', 'Bcharré');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (707, 7, 7, 'Shena', 'Lasselle', '91-713-4072', '30-(177)413-2123', '7532 Elgar Plaza', 'Petroúpolis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (708, 8, 8, 'Kaitlin', 'Beausang', '25-568-2495', '7-(892)292-6469', '08182 Kipling Drive', 'Kyzyl');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (709, 9, 9, 'Mylo', 'Tinwell', '56-510-0282', null, '400 Anzinger Place', 'Vällingby');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (710, 10, 10, 'Rhonda', 'Johansen', '40-691-5493', null, '080 Maple Wood Drive', 'Mel’nikovo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (711, 1, 11, 'Sigfrid', 'Shelmardine', '15-379-4760', '7-(792)547-2157', '8 Mcguire Hill', 'Glazov');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (712, 2, 12, 'Julie', 'Haddington', '62-212-8748', '86-(710)578-0663', '1 Pankratz Lane', 'Shezhu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (713, 3, 13, 'Bekki', 'Semerad', '78-038-0794', '504-(393)699-3172', '385 Talisman Crossing', 'Baja Mar');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (714, 4, 14, 'Brig', 'Dranfield', '30-215-2879', '53-(371)103-3291', '12 2nd Point', 'Contramaestre');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (715, 5, 15, 'Marcille', 'Barenski', '61-364-7967', '358-(602)651-9155', '45155 Erie Park', 'Tornio');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (716, 6, 16, 'Susanna', 'Libreros', '36-607-4193', '66-(401)690-5352', '814 Delaware Road', 'Ban Mai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (717, 7, 17, 'Padgett', 'Smeeton', '28-301-6109', '380-(577)485-8581', '625 Prairie Rose Circle', 'Subottsi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (718, 8, 18, 'Jillian', 'Rochell', '70-989-6349', '60-(105)106-2285', '8194 Esker Drive', 'Kuala Terengganu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (719, 9, 19, 'Alan', 'Shernock', '69-906-4092', '967-(781)284-2849', '6193 Red Cloud Crossing', 'Dhamār');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (720, 10, 20, 'Hallsy', 'Farahar', '89-026-0277', '62-(636)488-6324', '11 Express Park', 'Cungking');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (721, 1, 21, 'Evvy', 'Spragg', '16-489-6026', null, '5225 Nelson Road', 'Jiang’an');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (722, 2, 22, 'Gloriana', 'Bristowe', '20-095-9050', null, '95 David Alley', 'Dijon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (723, 3, 23, 'Deedee', 'Edeson', '87-963-0784', null, '9 Southridge Junction', 'Fundong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (724, 4, 24, 'Bird', 'Billo', '58-561-9410', '33-(502)431-8351', '9 Di Loreto Center', 'Bobigny');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (725, 5, 25, 'Vittorio', 'Gyford', '75-193-0426', '7-(573)788-4522', '16980 Waywood Avenue', 'Tonshayevo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (726, 6, 26, 'Marcile', 'Bamforth', '32-241-7289', null, '781 Anthes Pass', 'Devesa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (727, 7, 27, 'Bradley', 'Liquorish', '45-215-5979', '84-(919)535-1325', '24 Rusk Alley', 'Thành Phố Uông Bí');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (728, 8, 28, 'Albert', 'Thorrington', '63-510-3637', '86-(364)550-6191', '954 Amoth Junction', 'Qianyou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (729, 9, 29, 'Skye', 'Flear', '95-735-8307', '62-(741)459-8726', '04 Anderson Place', 'Talu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (730, 10, 30, 'Alie', 'Gore', '86-476-7858', '86-(950)270-1621', '4 Karstens Junction', 'Bajingzi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (731, 1, 31, 'Hakim', 'Biskupiak', '78-793-6885', '62-(265)988-8324', '84502 Lakewood Gardens Place', 'Kalangsari');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (732, 2, 32, 'Goldia', 'Workman', '69-688-7791', null, '80964 Mallory Hill', 'Vetlanda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (733, 3, 33, 'Wayland', 'Cadman', '76-575-9902', '380-(926)520-3835', '4 Hagan Avenue', 'Tyachiv');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (734, 4, 34, 'Alva', 'Whewell', '26-682-5910', '502-(621)738-3373', '68492 Spaight Trail', 'Cajolá');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (735, 5, 35, 'Baxie', 'Dumbrell', '44-853-3517', '62-(456)718-4562', '8882 Hauk Court', 'Kauman');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (736, 6, 36, 'Clarisse', 'Conrath', '30-777-2858', null, '0 2nd Place', 'Teuva');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (737, 7, 37, 'Matti', 'Aylett', '29-033-2620', '359-(611)270-4455', '83591 Merrick Park', 'Klisura');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (738, 8, 38, 'Loella', 'Algeo', '23-259-4306', '57-(918)965-4102', '7 Anzinger Place', 'Chiriguaná');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (739, 9, 39, 'Zsazsa', 'Renzo', '82-979-6115', '224-(831)906-4236', '69 Gateway Court', 'Lélouma');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (740, 10, 40, 'Sloane', 'Colliar', '42-737-6762', '81-(660)873-6450', '76880 Ohio Avenue', 'Ichinohe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (741, 1, 41, 'Broderick', 'Taplin', '40-273-6156', '86-(440)308-2720', '00 High Crossing Crossing', 'Yulin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (742, 2, 42, 'Michell', 'Talman', '41-965-0010', null, '09703 Brickson Park Way', 'Gorang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (743, 3, 43, 'Gregg', 'Crambie', '02-489-4242', '7-(267)268-5974', '1 Buhler Plaza', 'Mikhaylovsk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (744, 4, 44, 'Grant', 'Pharrow', '86-211-4947', '81-(865)876-0682', '889 International Street', 'Tsukumiura');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (745, 5, 45, 'Sander', 'Koppeck', '48-476-4048', null, '1787 Spohn Lane', 'Krasnyy Oktyabr’');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (746, 6, 46, 'Allissa', 'Ible', '96-564-9718', '1-(692)956-0747', '3 Blue Bill Park Lane', 'Lethbridge');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (747, 7, 47, 'Georgi', 'Lund', '67-064-5382', '33-(940)311-3591', '5 Mallory Crossing', 'Auch');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (748, 8, 48, 'Ulla', 'Alliban', '91-268-5638', '1-(989)419-5820', '88 Tony Alley', 'Saginaw');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (749, 9, 49, 'Milena', 'Peppard', '62-539-0597', '86-(617)899-9220', '71 Lerdahl Way', 'Lidong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (750, 10, 50, 'Stanislaw', 'Hankey', '62-918-2709', '86-(463)887-8938', '93756 Warner Point', 'Fengcheng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (751, 1, 51, 'Charles', 'Anstie', '30-020-5224', '48-(946)464-6627', '8246 Havey Parkway', 'Maniowy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (752, 2, 52, 'Livia', 'Dumbell', '81-692-9154', null, '76916 Glacier Hill Terrace', 'Yuantan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (753, 3, 53, 'Lodovico', 'Dinan', '46-914-4318', null, '0 Green Ridge Point', 'Tomice');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (754, 4, 54, 'Halsy', 'Potter', '61-497-1956', null, '89935 Judy Terrace', 'Clarin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (755, 5, 55, 'Julius', 'Debling', '05-440-5377', '46-(979)313-1796', '581 Dottie Trail', 'Täby');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (756, 6, 56, 'Garry', 'Egger', '10-054-0594', '63-(558)704-2903', '783 Messerschmidt Crossing', 'Ramon Magsaysay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (757, 7, 57, 'Tisha', 'Kinglesyd', '36-595-7308', null, '7397 Nova Place', 'Amboasary');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (758, 8, 58, 'Lindy', 'Piatkowski', '60-566-5440', null, '5 Moose Park', 'Waco');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (759, 9, 59, 'Julieta', 'Geoghegan', '14-540-2085', '62-(440)280-1901', '609 Lawn Road', 'Karawatu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (760, 10, 60, 'Roz', 'Fleet', '27-793-7608', '351-(596)640-0673', '77277 Prairie Rose Terrace', 'São João do Campo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (761, 1, 61, 'Sashenka', 'Blague', '87-735-3260', null, '9 Northport Lane', 'Xumai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (762, 2, 62, 'Sheeree', 'Hanning', '23-003-3185', '48-(756)874-1555', '245 Lunder Parkway', 'Dziadkowice');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (763, 3, 63, 'Heall', 'Anderbrugge', '57-962-4361', '63-(701)356-4787', '96 Muir Circle', 'Calumpit');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (764, 4, 64, 'Gisella', 'Cammiemile', '14-570-5743', null, '62 Vera Lane', 'Łobez');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (765, 5, 65, 'Edouard', 'Kruschev', '32-059-5599', '81-(627)366-3079', '9 Lien Circle', 'Kawanoechō');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (766, 6, 66, 'Dalila', 'Novis', '08-709-2904', '62-(652)164-8700', '1 Anzinger Terrace', 'Wirodayan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (767, 7, 67, 'Kaye', 'Janney', '06-962-3719', '86-(653)648-6438', '2505 Merchant Parkway', 'Hengliang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (768, 8, 68, 'Arlana', 'Rooze', '11-436-1114', '81-(254)833-7357', '49588 Erie Parkway', 'Makurazaki');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (769, 9, 69, 'Francis', 'McNae', '11-377-1066', '33-(824)112-1946', '7403 American Ash Alley', 'Paris La Défense');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (770, 10, 70, 'Phaedra', 'Ahearne', '97-698-1265', '86-(332)311-9545', '35650 Pawling Point', 'Jianxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (771, 1, 71, 'Gilligan', 'Spawforth', '23-174-1193', '963-(383)444-7734', '1 Riverside Place', 'Ḩarastā');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (772, 2, 72, 'Kristi', 'Satyford', '71-842-2719', '351-(952)769-3989', '48720 Warbler Trail', 'Vila Nova');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (773, 3, 73, 'Worthy', 'Cromwell', '99-796-9106', '86-(692)428-1088', '37 Mcguire Parkway', 'Doumen');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (774, 4, 74, 'Fraze', 'Di Frisco', '44-827-6016', '380-(746)260-3411', '272 Warner Parkway', 'Chyhyryn');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (775, 5, 75, 'Claiborn', 'Dunstone', '53-297-8343', null, '7078 Graceland Place', 'Bordeaux');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (776, 6, 76, 'Berte', 'Wilcocks', '83-907-5833', null, '8152 Ryan Circle', 'Västerås');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (777, 7, 77, 'Kellby', 'Krysztofiak', '32-040-7105', '86-(943)558-1329', '10 Porter Way', 'Erqu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (778, 8, 78, 'Dori', 'Gwillim', '69-048-4744', '218-(305)484-8148', '4 Hanover Parkway', 'Giado');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (779, 9, 79, 'Lurette', 'Goldthorp', '47-769-4229', '62-(558)201-9486', '142 Scofield Place', 'Tanjungbalai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (780, 10, 80, 'Matthaeus', 'Cristofano', '34-750-6276', '976-(796)973-5988', '1 Maywood Place', 'Sharïngol');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (781, 1, 81, 'Roxine', 'Zottoli', '43-738-1359', '255-(675)713-3785', '87 Erie Plaza', 'Lindi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (782, 2, 82, 'Giustina', 'Albisser', '29-328-8700', '265-(747)240-0323', '136 Evergreen Plaza', 'Liwonde');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (783, 3, 83, 'Constantino', 'Cisec', '74-293-5396', '86-(705)995-3477', '9686 Clyde Gallagher Hill', 'Miaoxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (784, 4, 84, 'Sabra', 'Dodgshon', '23-044-1016', '51-(411)431-3806', '99946 Grover Drive', 'Ayo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (785, 5, 85, 'Henryetta', 'Broadfield', '33-228-9705', '1-(127)335-7814', '4 Logan Drive', 'Invermere');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (786, 6, 86, 'Benni', 'Hedde', '21-776-8445', '86-(237)182-9671', '93764 Pond Terrace', 'Wangji');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (787, 7, 87, 'Leonora', 'Manoelli', '55-483-9937', '33-(196)277-7034', '54 Scott Drive', 'Ajaccio');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (788, 8, 88, 'Josephina', 'Caughey', '53-664-5208', '62-(672)910-5425', '8 Pine View Circle', 'Mandala');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (789, 9, 89, 'Jasmina', 'Brittin', '71-799-0158', '1-(536)113-9240', '1644 Mallard Place', 'Petersfield');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (790, 10, 90, 'Amby', 'Hamilton', '18-936-1042', '62-(413)207-6475', '1 Village Green Junction', 'Dulolong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (791, 1, 91, 'Dacy', 'Mandry', '01-559-9792', '269-(306)442-8657', '77779 Independence Trail', 'Hajoho');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (792, 2, 92, 'Nealy', 'Methley', '41-434-4997', '355-(292)392-4549', '55041 Eastlawn Parkway', 'Gurra e Vogël');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (793, 3, 93, 'Claudio', 'Mungane', '41-431-6084', '92-(531)861-9913', '9058 Lawn Road', 'Thul');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (794, 4, 94, 'Zeb', 'Pettisall', '15-283-7720', '420-(382)736-8808', '766 Corry Terrace', 'Březová nad Svitavou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (795, 5, 95, 'Alphonso', 'Nolte', '52-904-5225', '30-(474)448-0269', '78668 Fuller Center', 'Aryiropoúlion');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (796, 6, 96, 'Sharla', 'Massot', '35-021-9230', null, '44 1st Parkway', 'Lasusua');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (797, 7, 97, 'Nata', 'Cripwell', '95-386-6103', '53-(451)684-5771', '23 Birchwood Circle', 'Bahía Honda');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (798, 8, 98, 'Holli', 'Garroch', '77-012-8387', '994-(291)939-4718', '02431 Mendota Center', 'Baş Göynük');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (799, 9, 99, 'Licha', 'McEttigen', '65-022-7298', null, '36849 Ronald Regan Park', 'Ivanec');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (800, 10, 100, 'Marcelo', 'Anker', '75-879-1034', '55-(354)677-1033', '54584 Debra Plaza', 'Uarini');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (801, 1, 1, 'Tam', 'Bolstridge', '17-294-4205', '52-(105)832-1604', '47974 Del Sol Center', 'San Martin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (802, 2, 2, 'Bondy', 'Eddicott', '05-817-8176', null, '02 Ruskin Pass', 'Khvastovichi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (803, 3, 3, 'Darleen', 'Newson', '73-812-2755', null, '94524 Summit Parkway', 'Yanliang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (804, 4, 4, 'Lorne', 'Danovich', '42-819-3092', null, '32 Talisman Crossing', 'Singosari');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (805, 5, 5, 'Isadore', 'Kennally', '85-322-4179', '351-(255)113-6407', '3470 Vernon Alley', 'Portelinha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (806, 6, 6, 'Killy', 'Wilman', '41-056-7018', '380-(140)145-3978', '81 Swallow Circle', 'Pidvynohradiv');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (807, 7, 7, 'Niko', 'Helversen', '60-495-5587', '86-(403)794-9281', '395 Village Green Hill', 'Miaoya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (808, 8, 8, 'Bennie', 'Weinmann', '93-329-6745', '57-(695)118-0127', '88 Mcbride Junction', 'Sampués');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (809, 9, 9, 'Justis', 'Platt', '63-027-2664', '33-(730)538-2449', '2 Dottie Hill', 'Marseille');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (810, 10, 10, 'Caroljean', 'Bend', '25-475-9820', '46-(723)937-6608', '46736 Dunning Plaza', 'Knivsta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (811, 1, 11, 'Margaretta', 'Posselow', '32-552-3938', '86-(529)677-0600', '5 Eggendart Crossing', 'Hexing');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (812, 2, 12, 'Emelen', 'Flood', '26-508-9121', '52-(104)277-2213', '02724 Northport Parkway', 'Lazaro Cardenas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (813, 3, 13, 'Dani', 'Bertomeu', '64-917-3076', '351-(250)479-8206', '3970 Heffernan Street', 'Boco');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (814, 4, 14, 'Hamlen', 'Ishak', '37-026-1376', '964-(731)293-2765', '4 Vidon Trail', '‘Alī al Gharbī');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (815, 5, 15, 'Gabriella', 'Barefoot', '34-821-4335', '86-(552)151-4156', '968 Weeping Birch Crossing', 'Nanmu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (816, 6, 16, 'Susannah', 'Gouldeby', '23-629-5881', '967-(150)766-2960', '9 Sycamore Point', 'Al ‘Anān');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (817, 7, 17, 'Esteban', 'Goodenough', '02-275-1992', null, '1880 Golf Course Way', 'Göteborg');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (818, 8, 18, 'Cody', 'Vescovini', '92-165-9760', '86-(295)528-3030', '624 Banding Crossing', 'Hongxi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (819, 9, 19, 'Cecile', 'Cropp', '28-396-6318', '55-(204)526-0336', '1604 Farmco Parkway', 'Penha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (820, 10, 20, 'Xerxes', 'Aloigi', '86-919-0158', '84-(142)455-1087', '0 Randy Pass', 'Thị Trấn Văn Quan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (821, 1, 21, 'Obie', 'Hazeldene', '88-035-0241', '7-(817)393-7864', '61445 Thierer Drive', 'Losevo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (822, 2, 22, 'Marika', 'Cholerton', '95-562-3739', '7-(850)422-0621', '938 Veith Avenue', 'Kosaya Gora');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (823, 3, 23, 'Donn', 'Ratcliff', '89-094-1442', null, '35 Sutteridge Road', 'Kuala Lumpur');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (824, 4, 24, 'Fionnula', 'Riordan', '55-283-5007', '86-(572)192-5584', '07 Kennedy Center', 'Baohe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (825, 5, 25, 'Valenka', 'Jayes', '28-367-8513', '258-(473)546-8143', '333 Pearson Avenue', 'Chimoio');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (826, 6, 26, 'Lauri', 'Nettle', '23-667-3427', '81-(171)538-1504', '2 Wayridge Park', 'Shiroi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (827, 7, 27, 'Gene', 'Ovanesian', '35-476-6252', null, '227 Eastlawn Pass', 'Mutum');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (828, 8, 28, 'Lola', 'Rambadt', '07-142-5115', '86-(910)545-9512', '1 Anniversary Terrace', 'Caohe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (829, 9, 29, 'Gusti', 'Brane', '94-699-2500', '86-(227)927-7579', '50 Norway Maple Hill', 'Jiangdulu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (830, 10, 30, 'Letta', 'Humble', '23-119-8517', '86-(252)892-8975', '088 Merrick Parkway', 'Huangni');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (831, 1, 31, 'Christoffer', 'MacElane', '20-874-1364', '86-(350)933-4843', '43 Ramsey Road', 'Xindi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (832, 2, 32, 'Tome', 'Venes', '93-412-5745', null, '61 Menomonie Road', 'Stockholm');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (833, 3, 33, 'Ronni', 'Collisson', '58-015-5715', '1-(901)428-2222', '88 Blaine Hill', 'Memphis');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (834, 4, 34, 'Hersch', 'Burgise', '25-341-1131', null, '70 Jackson Drive', 'Toulon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (835, 5, 35, 'Vyky', 'MacLachlan', '68-093-2177', '81-(217)707-6202', '50 Shopko Alley', 'Yanai');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (836, 6, 36, 'Olympia', 'Whitty', '44-137-4585', '30-(696)531-2913', '5 Derek Pass', 'Graikochóri');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (837, 7, 37, 'Drucy', 'Burchill', '94-392-4327', null, '7 Kensington Crossing', 'Nidek');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (838, 8, 38, 'Franzen', 'Plose', '79-021-1785', null, '833 Shasta Park', 'Tinaquillo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (839, 9, 39, 'Alejandro', 'Southern', '62-287-8444', null, '05 Ilene Drive', 'Bone South');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (840, 10, 40, 'Essy', 'Creffeild', '90-704-0930', '62-(601)114-6989', '9304 Arapahoe Parkway', 'Kademangan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (841, 1, 41, 'Jaime', 'Goutcher', '11-833-3092', '86-(647)106-1023', '1 Arkansas Terrace', 'Jiudu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (842, 2, 42, 'Emmey', 'Dommerque', '53-644-5712', null, '624 Melvin Point', 'Sölvesborg');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (843, 3, 43, 'Hernando', 'Cristoforetti', '14-930-3393', '7-(542)847-2091', '37943 Toban Center', 'Bogorodsk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (844, 4, 44, 'Regina', 'Derx', '14-909-4714', '7-(261)692-8078', '83 Sherman Point', 'Bobrovka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (845, 5, 45, 'Geoff', 'Olivas', '68-916-9312', '82-(194)126-2868', '11399 Springview Road', 'Yangsan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (846, 6, 46, 'Anatol', 'Stormont', '82-130-3679', null, '8 Myrtle Park', 'Hayes');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (847, 7, 47, 'Remington', 'Drinkwater', '96-675-2562', '55-(577)995-9696', '1 Alpine Lane', 'Iperó');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (848, 8, 48, 'Muffin', 'Eastman', '27-213-0435', '62-(507)830-0380', '8 Hintze Court', 'Sungai Nyamuk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (849, 9, 49, 'Denny', 'Elsmor', '55-919-6708', '86-(305)723-1987', '27 Stephen Road', 'Jinlong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (850, 10, 50, 'Raf', 'Shortland', '95-823-7001', null, '8400 Porter Plaza', 'Zhinvali');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (851, 1, 51, 'Ulrica', 'Ninnotti', '08-277-3680', '86-(397)957-6678', '54913 Badeau Plaza', 'Xinxing');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (852, 2, 52, 'Janenna', 'Barmby', '58-826-8491', null, '650 Summit Circle', 'Chaihe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (853, 3, 53, 'Osbert', 'Hearty', '97-796-2641', '380-(612)801-7743', '62 Logan Place', 'Novomoskovs’k');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (854, 4, 54, 'Dougie', 'Aberdeen', '19-260-1100', '351-(969)986-5788', '47504 Hazelcrest Road', 'Ribeira');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (855, 5, 55, 'Scottie', 'Ingreda', '42-022-6086', '86-(245)332-9979', '88 Carberry Place', 'Xindu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (856, 6, 56, 'Averill', 'Aiston', '31-086-3010', '7-(919)204-3978', '625 Hanson Point', 'Krasnoye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (857, 7, 57, 'Loella', 'Willman', '69-182-7395', '86-(172)304-0845', '484 Vermont Parkway', 'Nanfeng');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (858, 8, 58, 'Hollis', 'Spoors', '00-366-2503', null, '696 Morning Lane', 'Verba');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (859, 9, 59, 'Florida', 'Wallworke', '70-655-6230', '81-(740)549-9574', '017 Northview Trail', 'Yūki');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (860, 10, 60, 'Shepperd', 'Langridge', '30-719-9697', '373-(827)790-1236', '37 Mcbride Way', 'Rîbniţa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (861, 1, 61, 'Britteny', 'Defond', '32-650-1065', '7-(374)314-3645', '222 American Terrace', 'Georgiyevka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (862, 2, 62, 'Aileen', 'Quinlan', '60-987-4177', null, '9488 Nova Avenue', 'Tokarnia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (863, 3, 63, 'Jillayne', 'Hriinchenko', '66-154-2932', null, '9 Lakewood Junction', 'Pasarnangka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (864, 4, 64, 'Fanechka', 'Chew', '34-864-3908', '375-(813)875-2410', '24461 Clove Way', 'Kirawsk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (865, 5, 65, 'Waldon', 'Cescot', '89-264-4595', '86-(408)339-8727', '28564 Glacier Hill Point', 'Zhuozishan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (866, 6, 66, 'Ashby', 'Averall', '39-249-1760', null, '0869 Ohio Terrace', 'Concordia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (867, 7, 67, 'Heath', 'Sigward', '09-901-1238', '84-(570)718-2422', '9 Grasskamp Avenue', 'Hưng Yên');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (868, 8, 68, 'Candi', 'Normanell', '90-581-5525', '63-(698)444-7103', '0 Thompson Trail', 'Marilao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (869, 9, 69, 'Dagmar', 'Cruse', '93-811-6621', '33-(707)377-5192', '22 Waxwing Junction', 'Troyes');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (870, 10, 70, 'Fara', 'Shipway', '00-986-3931', '86-(830)777-5224', '6 Northfield Pass', 'Qijiang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (871, 1, 71, 'Kay', 'Harhoff', '72-010-8299', '86-(283)889-1832', '9 Nobel Circle', 'Jingguan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (872, 2, 72, 'Chariot', 'Eyden', '31-179-4578', '7-(990)977-3252', '93478 Warner Parkway', 'Polyarnyye Zori');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (873, 3, 73, 'Max', 'Swindles', '83-820-6311', null, '2377 Carioca Street', 'Sucre');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (874, 4, 74, 'Gareth', 'Huxton', '10-393-9680', '976-(129)511-4933', '1 Scofield Plaza', 'Sharïngol');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (875, 5, 75, 'Nathaniel', 'Ausello', '50-034-6533', null, '0 Havey Hill', 'Słotowa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (876, 6, 76, 'Darcey', 'Capelle', '45-004-5880', '351-(848)563-1939', '8925 Golf Course Way', 'Famões');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (877, 7, 77, 'Julee', 'Hardie', '05-360-1525', '1-(904)508-8010', '629 Rigney Avenue', 'Port-Cartier');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (878, 8, 78, 'Ilyse', 'Baudy', '03-158-8823', '7-(673)191-2986', '3 Sunbrook Place', 'Novotitarovskaya');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (879, 9, 79, 'Elias', 'Bolles', '33-242-4516', '63-(984)682-4362', '595 Farmco Parkway', 'Taluksangay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (880, 10, 80, 'Joyous', 'Le Friec', '51-576-5811', null, '718 Shoshone Drive', 'Ouégoa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (881, 1, 81, 'Myron', 'Gowry', '31-716-3579', '62-(154)202-4976', '36 Cambridge Lane', 'Krajandadapmulyo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (882, 2, 82, 'Ulick', 'Hain', '07-499-8251', null, '7 Mcbride Avenue', 'Dunleer');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (883, 3, 83, 'Siusan', 'Pinching', '10-227-7450', null, '2 Bluestem Alley', 'Hövsan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (884, 4, 84, 'Kenton', 'Wallington', '34-621-9142', '31-(644)418-9207', '9732 Heath Avenue', 'Ede');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (885, 5, 85, 'Melly', 'Lyenyng', '17-245-5830', null, '0146 Scoville Crossing', 'Stockholm');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (886, 6, 86, 'Florencia', 'McIlreavy', '76-294-6162', '62-(696)105-4262', '38 Meadow Ridge Trail', 'Pamedaran');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (887, 7, 87, 'Lauren', 'Farebrother', '88-035-4243', '48-(909)275-9775', '0 Buhler Park', 'Lututów');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (888, 8, 88, 'Gerta', 'Mountstephen', '82-613-1444', null, '21285 Graedel Junction', 'Panacan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (889, 9, 89, 'Della', 'Franzelini', '20-997-6043', '66-(782)427-3379', '2 Monument Center', 'Rattanaburi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (890, 10, 90, 'Cully', 'Poley', '10-044-2671', null, '17 Calypso Avenue', 'San Juan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (891, 1, 91, 'Veronique', 'Rudeforth', '06-706-2968', '62-(644)365-2666', '0510 Moland Terrace', 'Banjar Baleagung');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (892, 2, 92, 'Annora', 'Glasspoole', '14-120-8106', '7-(308)654-3554', '23541 Center Place', 'Kazan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (893, 3, 93, 'Carin', 'Himsworth', '20-567-6696', null, '2531 Logan Circle', 'Liufang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (894, 4, 94, 'Stefania', 'Braithwaite', '21-826-6850', null, '2447 Susan Park', 'Macarse');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (895, 5, 95, 'Kippy', 'Tremollet', '54-468-5463', '47-(792)574-7555', '14 Charing Cross Road', 'Skien');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (896, 6, 96, 'Marybelle', 'Sagg', '10-354-6789', null, '25871 Dwight Center', 'Matiao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (897, 7, 97, 'Merrill', 'Fosbraey', '55-506-6604', '46-(377)776-2916', '5 Sloan Circle', 'Mölnlycke');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (898, 8, 98, 'Nessi', 'Petteford', '72-857-7109', '54-(950)247-8796', '5041 Nelson Center', 'Laboulaye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (899, 9, 99, 'Colene', 'Drain', '28-115-0887', null, '929 Summer Ridge Road', 'Lindavista');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (900, 10, 100, 'Kaela', 'Yekel', '70-268-8828', null, '36779 Truax Court', 'Zielonka');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (901, 1, 1, 'Levey', 'Uwins', '55-016-6227', null, '9 Fairview Avenue', 'Emiliano Zapata');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (902, 2, 2, 'Alexa', 'Craddock', '50-859-9442', '351-(640)722-9358', '7800 Daystar Crossing', 'Vale de Vila');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (903, 3, 3, 'Minor', 'Buard', '60-411-7844', null, '20322 Washington Point', 'Santa Lucia');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (904, 4, 4, 'Johanna', 'Domerc', '61-881-7887', null, '40043 Warrior Trail', 'Taoyuan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (905, 5, 5, 'Gualterio', 'Burgon', '61-467-8090', '81-(544)767-7557', '9704 Jenna Circle', 'Yoshida-kasugachō');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (906, 6, 6, 'Cori', 'Plimmer', '75-627-7940', null, '1914 Rockefeller Hill', 'Abreus');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (907, 7, 7, 'Ludvig', 'Swanger', '44-130-3339', '86-(843)774-2723', '83 Donald Place', 'Chengkan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (908, 8, 8, 'Franciskus', 'Pickvance', '01-353-8646', '46-(121)677-9523', '2 Petterle Street', 'Västerås');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (909, 9, 9, 'Dennie', 'Maudlin', '74-053-9082', '46-(227)246-0313', '11 Leroy Drive', 'Örnsköldsvik');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (910, 10, 10, 'Nisse', 'Roizin', '95-115-2403', '86-(372)928-9435', '6 Johnson Lane', 'Shangyuan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (911, 1, 11, 'Rolando', 'Sammes', '76-133-9767', '62-(551)816-5026', '3 Norway Maple Pass', 'Kojagete');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (912, 2, 12, 'Rosalie', 'Saterweyte', '76-459-6546', null, '57 Eastlawn Park', 'Artur Nogueira');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (913, 3, 13, 'Wald', 'Deakins', '74-057-5369', '1-(214)300-2190', '5489 Myrtle Circle', 'Dallas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (914, 4, 14, 'Koo', 'Luna', '73-056-8676', '967-(916)665-8032', '9394 Farragut Hill', 'Al Jarrāḩī');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (915, 5, 15, 'Rich', 'Juszczak', '84-117-7508', '351-(677)247-7820', '6069 Rutledge Circle', 'Torres Novas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (916, 6, 16, 'Ella', 'Bew', '34-886-1324', '381-(221)631-4886', '865 Almo Drive', 'Klenje');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (917, 7, 17, 'Gertie', 'McGarvey', '73-108-9062', '353-(862)946-3544', '84 Dixon Avenue', 'Dublin');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (918, 8, 18, 'Traver', 'Pury', '47-149-3983', '46-(541)963-6783', '6282 Lillian Place', 'Stockholm');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (919, 9, 19, 'Calypso', 'Possek', '70-389-8618', null, '848 Pond Center', 'Petrozavodsk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (920, 10, 20, 'Anderea', 'Moodycliffe', '67-245-7531', '351-(785)736-6548', '12626 Claremont Road', 'Rios Frios');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (921, 1, 21, 'Carlyn', 'Chattelaine', '92-307-4604', '1-(209)987-5182', '195 Fairfield Alley', 'Fresno');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (922, 2, 22, 'Maighdiln', 'Guilder', '52-472-6056', '63-(177)841-1598', '8524 Texas Court', 'Sabang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (923, 3, 23, 'Cammy', 'Meir', '12-462-8290', '34-(953)379-8153', '09 Warner Lane', 'Vigo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (924, 4, 24, 'Pierette', 'Evitt', '11-292-7123', '1-(801)111-4031', '0271 Chive Drive', 'Salt Lake City');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (925, 5, 25, 'Arturo', 'Killingbeck', '33-782-9968', '420-(694)582-8008', '78 Fair Oaks Terrace', 'Rychnov nad Kněžnou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (926, 6, 26, 'Farleigh', 'Haddrell', '26-374-0150', '86-(841)360-5040', '587 Atwood Crossing', 'Henghe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (927, 7, 27, 'Edik', 'Thonger', '60-205-3555', '7-(308)727-2777', '4 Valley Edge Drive', 'Abakan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (928, 8, 28, 'Quill', 'Olenov', '48-648-1209', '234-(774)232-7301', '3 Washington Way', 'Maru');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (929, 9, 29, 'Alexandro', 'Guerre', '68-640-5862', null, '7651 Barnett Place', 'Lampang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (930, 10, 30, 'Elysee', 'Troctor', '24-317-6405', '359-(205)413-0893', '86 Logan Hill', 'Strazhitsa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (931, 1, 31, 'Prinz', 'Jeggo', '21-446-6706', '351-(558)810-7475', '6 Havey Trail', 'Baião');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (932, 2, 32, 'Aindrea', 'Chapelhow', '46-323-3962', null, '98376 Swallow Alley', 'Penanggungan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (933, 3, 33, 'Timi', 'Gemlett', '78-643-7913', '355-(911)352-4558', '5790 Dottie Crossing', 'Dardhas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (934, 4, 34, 'Ardra', 'Strettell', '12-560-5184', '1-(695)997-6493', '15 Dahle Junction', 'Kelowna');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (935, 5, 35, 'Stace', 'Raccio', '18-915-0624', '351-(703)906-8257', '14292 Huxley Drive', 'Sernancelhe');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (936, 6, 36, 'Cicely', 'Pearsey', '69-049-1690', '62-(433)859-0532', '45 Hallows Alley', 'Cigaluh');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (937, 7, 37, 'Ellen', 'Martine', '95-311-4336', null, '68189 Vidon Place', 'Semanding');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (938, 8, 38, 'Theo', 'Stallon', '40-142-0621', null, '1983 Jana Drive', 'Tarsouat');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (939, 9, 39, 'Shirleen', 'Trunby', '69-373-3400', null, '7539 Nelson Center', 'Pragen Selatan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (940, 10, 40, 'Shell', 'Cabane', '71-541-4119', '86-(527)831-9493', '3 Donald Street', 'Shikou');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (941, 1, 41, 'Casper', 'Parlet', '17-892-9276', '62-(801)394-8770', '607 Helena Parkway', 'Sukonolo Krajan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (942, 2, 42, 'Lauryn', 'Arnaldy', '41-892-5856', '351-(369)732-2960', '1071 Goodland Circle', 'Famões');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (943, 3, 43, 'Elinore', 'Stening', '12-714-9969', '51-(260)674-3494', '1 Judy Drive', 'Tantamayo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (944, 4, 44, 'Ninnette', 'Glison', '58-155-3278', '351-(554)959-4023', '4 Becker Circle', 'Touguinha');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (945, 5, 45, 'Etti', 'Pont', '99-479-2245', '355-(770)716-8517', '73913 Lakeland Drive', 'Kurjan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (946, 6, 46, 'Stefa', 'Rumbellow', '69-061-7005', null, '19 Victoria Hill', 'Corticeiro de Baixo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (947, 7, 47, 'Raquela', 'Akerman', '85-163-5716', '63-(922)870-7847', '88 Columbus Park', 'Guiniculalay');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (948, 8, 48, 'Aarika', 'Cuthill', '51-776-5024', '970-(695)975-2165', '5533 Mandrake Junction', '‘Arab ar Rashāydah');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (949, 9, 49, 'Antony', 'Guiot', '93-078-3207', '63-(985)687-8213', '83606 Continental Crossing', 'Libertad');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (950, 10, 50, 'Dawn', 'Vasilmanov', '62-390-8808', '63-(629)172-1731', '138 Mccormick Lane', 'Tortosa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (951, 1, 51, 'Illa', 'Scantleberry', '65-714-7264', '86-(249)457-0017', '586 Burning Wood Drive', 'Kuangshan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (952, 2, 52, 'Norby', 'Seers', '06-201-8166', '62-(425)135-7116', '981 Spohn Street', 'Rejoso');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (953, 3, 53, 'Raine', 'Buttner', '11-059-6064', '86-(233)488-2264', '58888 Badeau Avenue', 'Huangchi');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (954, 4, 54, 'Carmelita', 'Burkinshaw', '18-873-4177', '86-(693)806-9928', '782 Starling Parkway', 'Xinfengjie');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (955, 5, 55, 'Ainslee', 'Armstrong', '01-525-2887', '7-(480)142-1791', '16 Mandrake Avenue', 'Karagandy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (956, 6, 56, 'Robenia', 'Jurisch', '27-914-2451', '55-(387)946-4535', '10381 Reindahl Park', 'Cascavel');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (957, 7, 57, 'Marcela', 'Carnachen', '37-160-5496', '62-(949)975-8057', '8 Lindbergh Crossing', 'Murtajih');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (958, 8, 58, 'Sergei', 'Savege', '01-421-1952', '972-(822)684-1694', '63 Elgar Terrace', 'Ḥurfeish');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (959, 9, 59, 'Callie', 'Burchess', '46-774-4783', '86-(277)621-1239', '31669 Corben Way', 'Dalu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (960, 10, 60, 'Theo', 'Kinkaid', '57-297-4559', '63-(963)325-0101', '548 Ridgeway Drive', 'Kaytitinga');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (961, 1, 61, 'Erin', 'Lissandrini', '42-077-5315', null, '919 Dapin Court', 'Khyzy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (962, 2, 62, 'Gale', 'Perchard', '48-996-8125', null, '6750 Sutherland Place', 'Kumanovo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (963, 3, 63, 'Skipper', 'Rugiero', '11-946-9950', '86-(787)607-9040', '838 Moulton Lane', 'Datong');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (964, 4, 64, 'Frieda', 'Rutigliano', '12-566-7108', '504-(915)166-0125', '98 Pine View Crossing', 'Azacualpa');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (965, 5, 65, 'Sara', 'Kobpa', '53-489-5663', '234-(120)930-1301', '63 High Crossing Parkway', 'Oyan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (966, 6, 66, 'Valentine', 'Casillis', '27-198-0454', '60-(208)343-9292', '62 Surrey Place', 'Kota Kinabalu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (967, 7, 67, 'Margalit', 'Whales', '20-621-0095', null, '77007 Westridge Lane', 'Uttar Char Fasson');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (968, 8, 68, 'Murry', 'McKearnen', '94-041-4455', null, '1 4th Lane', 'Yao’an');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (969, 9, 69, 'Shaw', 'Jennick', '28-546-9861', '33-(955)271-6108', '42963 Warner Lane', 'Cergy-Pontoise');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (970, 10, 70, 'Matteo', 'Goff', '23-298-4188', '55-(692)562-5920', '72326 Barnett Trail', 'Nova Era');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (971, 1, 71, 'Lark', 'Saunderson', '89-046-6127', '66-(223)472-3056', '094 Fieldstone Junction', 'Khao Khitchakut');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (972, 2, 72, 'Aldon', 'Simonetti', '73-874-3278', '7-(363)801-6267', '8 Farragut Point', 'Uchkeken');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (973, 3, 73, 'Sher', 'Keam', '01-742-3100', '1-(862)927-0615', '9750 Westend Lane', 'San Juan');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (974, 4, 74, 'Berthe', 'Rankling', '59-374-1125', null, '151 Merchant Hill', 'Itabaiana');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (975, 5, 75, 'Ansel', 'Croisdall', '13-570-0585', '7-(578)649-7082', '59 Northridge Alley', 'Sosnovo-Ozerskoye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (976, 6, 76, 'Irvin', 'Kiss', '34-949-7232', '48-(440)686-4548', '14250 La Follette Alley', 'Wijewo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (977, 7, 77, 'Nikolas', 'Druett', '63-212-3063', '58-(287)761-1704', '145 Bartelt Park', 'Clarines');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (978, 8, 78, 'Ahmed', 'Scathard', '99-833-8748', '7-(513)785-2808', '122 Macpherson Alley', 'Novoshakhtinsk');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (979, 9, 79, 'Rick', 'Hinrichsen', '72-886-2221', '54-(728)680-6476', '1 Bluestem Lane', 'Posadas');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (980, 10, 80, 'Candide', 'Hazlewood', '82-117-0031', null, '106 Hoffman Lane', 'Shifang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (981, 1, 81, 'Arlina', 'Wavish', '51-446-2887', null, '62 Steensland Road', 'Panaytayon');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (982, 2, 82, 'Whitman', 'Plak', '93-844-0194', '62-(114)365-5648', '7 Carey Hill', 'Ebak');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (983, 3, 83, 'Bethena', 'Shee', '35-830-6938', '380-(760)452-2066', '8 Cottonwood Crossing', 'Kuty');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (984, 4, 84, 'Brocky', 'Garrity', '12-366-3796', '86-(117)542-7220', '49366 Pine View Court', 'Xuebu');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (985, 5, 85, 'Gianna', 'Deluce', '49-747-3043', '86-(455)944-6023', '8 Kennedy Road', 'Quchomo');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (986, 6, 86, 'Sharia', 'Guille', '75-647-6522', '358-(555)954-6632', '5 Pennsylvania Parkway', 'Ranua');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (987, 7, 87, 'Rochelle', 'Rains', '93-289-5646', '62-(821)596-0524', '37428 Heffernan Lane', 'Cimanglid');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (988, 8, 88, 'Viviene', 'Abbots', '03-832-5576', '7-(786)707-6657', '80051 Banding Place', 'Krasnoye');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (989, 9, 89, 'Tiffanie', 'Cowl', '97-874-0200', '30-(697)883-3154', '57612 Division Drive', 'Néa Sánta');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (990, 10, 90, 'Nicolle', 'Ceely', '25-461-7245', '86-(724)845-1228', '82809 Duke Road', 'Shouchang');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (991, 1, 91, 'Sianna', 'Trimnell', '39-218-2837', '966-(872)877-6809', '1 East Terrace', 'Ţubarjal');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (992, 2, 92, 'Chloette', 'Sammonds', '72-845-5715', null, '04686 Texas Junction', 'Payao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (993, 3, 93, 'Editha', 'Keohane', '61-856-8623', '48-(521)740-3465', '8981 Crest Line Pass', 'Leśna Podlaska');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (994, 4, 94, 'Damien', 'Jacobowits', '13-296-1495', '86-(333)981-6942', '1 Loeprich Court', 'Xinqiao');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (995, 5, 95, 'Arlen', 'Larroway', '56-807-4528', null, '661 Morning Road', 'Yeniköy');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (996, 6, 96, 'Jeremiah', 'Spalding', '81-990-4397', null, '88971 Stephen Pass', 'Fresno');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (997, 7, 97, 'Mercie', 'Sergean', '90-634-3326', '63-(450)206-7203', '39778 Ridge Oak Pass', 'Conel');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (998, 8, 98, 'Sig', 'Mourgue', '36-813-7356', '62-(970)463-0943', '4554 Browning Parkway', 'Tunggaoen Timur');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (999, 9, 99, 'Georgetta', 'Linkin', '21-674-1192', '51-(829)761-1502', '8 Little Fleur Road', 'Pomahuaca');
insert into Student (StudentId, FacultyId, DepartmentId, Name, Surname, StudentNumber, PhoneNumber, Address, City) values (1000, 10, 100, 'Trueman', 'Elles', '59-354-2052', '82-(662)620-2006', '77816 Southridge Drive', 'Asan');
set identity_insert student off


set identity_insert StudentCourse on
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1,1,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2,1,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3,1,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4,1,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (5,1,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (6,2,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (7,2,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (8,2,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (9,2,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (10,2,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (11,2,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (12,2,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (13,3,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (14,3,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (15,3,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (16,3,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (17,3,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (18,3,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (19,3,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (20,4,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (21,4,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (22,4,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (23,4,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (24,4,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (25,5,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (26,5,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (27,5,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (28,5,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (29,5,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (30,6,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (31,6,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (32,6,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (33,6,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (34,6,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (35,7,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (36,7,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (37,7,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (38,7,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (39,7,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (40,7,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (41,7,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (42,7,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (43,8,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (44,8,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (45,8,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (46,8,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (47,8,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (48,8,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (49,8,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (50,8,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (51,9,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (52,9,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (53,9,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (54,10,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (55,10,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (56,10,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (57,11,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (58,12,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (59,12,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (60,12,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (61,12,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (62,13,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (63,13,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (64,13,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (65,13,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (66,14,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (67,14,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (68,14,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (69,14,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (70,14,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (71,14,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (72,14,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (73,14,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (74,15,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (75,15,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (76,15,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (77,15,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (78,15,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (79,15,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (80,15,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (81,15,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (82,16,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (83,16,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (84,16,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (85,16,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (86,16,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (87,16,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (88,16,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (89,16,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (90,17,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (91,17,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (92,17,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (93,17,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (94,17,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (95,17,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (96,18,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (97,18,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (98,18,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (99,18,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (100,18,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (101,18,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (102,19,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (103,19,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (104,19,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (105,19,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (106,19,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (107,19,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (108,19,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (109,19,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (110,19,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (111,20,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (112,20,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (113,20,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (114,20,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (115,20,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (116,20,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (117,20,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (118,20,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (119,20,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (120,21,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (121,21,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (122,21,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (123,21,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (124,21,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (125,21,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (126,21,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (127,21,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (128,22,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (129,22,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (130,22,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (131,22,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (132,22,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (133,22,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (134,22,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (135,22,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (136,23,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (137,23,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (138,23,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (139,23,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (140,23,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (141,23,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (142,23,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (143,23,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (144,24,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (145,24,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (146,25,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (147,25,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (148,26,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (149,26,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (150,26,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (151,26,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (152,26,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (153,27,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (154,27,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (155,27,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (156,27,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (157,27,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (158,28,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (159,28,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (160,28,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (161,28,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (162,29,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (163,29,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (164,29,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (165,29,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (166,30,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (167,30,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (168,30,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (169,30,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (170,30,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (171,30,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (172,30,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (173,31,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (174,31,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (175,31,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (176,31,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (177,31,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (178,31,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (179,31,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (180,32,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (181,33,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (182,34,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (183,35,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (184,35,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (185,35,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (186,35,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (187,35,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (188,35,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (189,35,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (190,35,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (191,35,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (192,36,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (193,36,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (194,36,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (195,36,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (196,36,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (197,36,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (198,36,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (199,36,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (200,36,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (201,37,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (202,37,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (203,37,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (204,38,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (205,38,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (206,38,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (207,39,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (208,39,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (209,40,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (210,40,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (211,41,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (212,41,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (213,42,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (214,42,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (215,42,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (216,42,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (217,42,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (218,43,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (219,43,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (220,43,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (221,43,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (222,43,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (223,44,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (224,44,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (225,44,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (226,44,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (227,44,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (228,45,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (229,45,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (230,45,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (231,45,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (232,45,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (233,45,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (234,45,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (235,45,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (236,46,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (237,46,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (238,46,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (239,46,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (240,46,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (241,46,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (242,46,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (243,47,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (244,47,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (245,47,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (246,47,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (247,47,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (248,47,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (249,47,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (250,48,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (251,48,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (252,48,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (253,48,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (254,48,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (255,48,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (256,48,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (257,49,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (258,50,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (259,51,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (260,51,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (261,51,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (262,51,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (263,52,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (264,52,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (265,52,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (266,52,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (267,53,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (268,53,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (269,53,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (270,53,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (271,54,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (272,54,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (273,54,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (274,55,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (275,55,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (276,55,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (277,56,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (278,56,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (279,56,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (280,56,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (281,56,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (282,56,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (283,57,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (284,57,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (285,57,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (286,57,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (287,57,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (288,57,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (289,58,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (290,58,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (291,58,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (292,58,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (293,58,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (294,58,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (295,59,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (296,59,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (297,59,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (298,59,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (299,60,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (300,60,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (301,60,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (302,60,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (303,61,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (304,61,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (305,61,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (306,61,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (307,61,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (308,61,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (309,61,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (310,61,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (311,62,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (312,62,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (313,62,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (314,62,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (315,62,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (316,62,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (317,62,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (318,62,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (319,63,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (320,63,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (321,64,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (322,64,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (323,65,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (324,65,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (325,66,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (326,66,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (327,66,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (328,66,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (329,66,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (330,66,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (331,66,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (332,66,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (333,66,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (334,67,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (335,67,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (336,67,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (337,67,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (338,67,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (339,67,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (340,67,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (341,67,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (342,67,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (343,68,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (344,68,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (345,68,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (346,68,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (347,69,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (348,69,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (349,69,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (350,69,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (351,70,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (352,70,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (353,70,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (354,70,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (355,70,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (356,70,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (357,70,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (358,71,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (359,71,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (360,71,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (361,71,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (362,71,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (363,71,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (364,71,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (365,72,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (366,72,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (367,72,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (368,72,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (369,72,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (370,73,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (371,73,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (372,73,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (373,73,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (374,73,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (375,73,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (376,73,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (377,73,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (378,73,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (379,74,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (380,74,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (381,74,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (382,74,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (383,74,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (384,74,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (385,74,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (386,74,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (387,74,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (388,75,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (389,75,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (390,75,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (391,75,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (392,75,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (393,75,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (394,75,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (395,76,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (396,76,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (397,76,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (398,76,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (399,76,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (400,76,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (401,76,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (402,77,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (403,77,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (404,77,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (405,77,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (406,77,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (407,77,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (408,77,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (409,78,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (410,79,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (411,80,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (412,80,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (413,80,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (414,80,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (415,80,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (416,81,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (417,81,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (418,81,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (419,81,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (420,81,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (421,82,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (422,82,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (423,82,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (424,82,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (425,82,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (426,83,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (427,83,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (428,83,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (429,84,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (430,84,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (431,84,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (432,85,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (433,85,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (434,85,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (435,85,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (436,85,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (437,85,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (438,86,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (439,86,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (440,86,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (441,86,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (442,86,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (443,86,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (444,87,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (445,88,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (446,89,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (447,89,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (448,89,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (449,89,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (450,89,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (451,89,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (452,89,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (453,89,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (454,90,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (455,90,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (456,90,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (457,90,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (458,90,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (459,90,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (460,90,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (461,90,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (462,91,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (463,91,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (464,91,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (465,91,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (466,91,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (467,91,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (468,91,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (469,91,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (470,92,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (471,92,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (472,93,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (473,93,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (474,94,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (475,95,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (476,96,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (477,96,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (478,96,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (479,96,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (480,97,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (481,97,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (482,97,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (483,97,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (484,98,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (485,98,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (486,98,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (487,98,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (488,98,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (489,98,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (490,98,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (491,99,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (492,99,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (493,99,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (494,99,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (495,99,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (496,99,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (497,99,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (498,100,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (499,100,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (500,100,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (501,100,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (502,100,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (503,100,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (504,101,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (505,101,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (506,101,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (507,101,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (508,101,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (509,101,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (510,102,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (511,102,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (512,102,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (513,102,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (514,102,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (515,102,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (516,102,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (517,102,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (518,102,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (519,103,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (520,103,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (521,103,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (522,103,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (523,103,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (524,103,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (525,103,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (526,103,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (527,103,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (528,104,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (529,104,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (530,104,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (531,105,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (532,105,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (533,105,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (534,106,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (535,106,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (536,107,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (537,107,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (538,108,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (539,108,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (540,108,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (541,108,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (542,108,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (543,109,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (544,109,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (545,109,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (546,109,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (547,109,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (548,110,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (549,110,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (550,110,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (551,110,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (552,111,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (553,111,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (554,111,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (555,111,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (556,111,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (557,111,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (558,111,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (559,112,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (560,112,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (561,112,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (562,112,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (563,112,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (564,112,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (565,112,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (566,113,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (567,114,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (568,115,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (569,115,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (570,115,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (571,115,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (572,115,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (573,115,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (574,115,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (575,115,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (576,115,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (577,116,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (578,116,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (579,116,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (580,116,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (581,116,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (582,116,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (583,116,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (584,116,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (585,116,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (586,117,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (587,117,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (588,117,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (589,118,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (590,118,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (591,118,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (592,119,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (593,119,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (594,119,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (595,119,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (596,119,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (597,119,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (598,120,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (599,120,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (600,120,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (601,120,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (602,120,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (603,120,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (604,121,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (605,121,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (606,121,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (607,121,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (608,121,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (609,121,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (610,122,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (611,122,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (612,122,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (613,122,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (614,122,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (615,123,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (616,123,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (617,123,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (618,123,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (619,123,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (620,124,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (621,124,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (622,124,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (623,124,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (624,124,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (625,124,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (626,124,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (627,124,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (628,125,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (629,125,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (630,125,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (631,125,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (632,125,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (633,125,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (634,126,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (635,126,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (636,126,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (637,126,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (638,126,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (639,126,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (640,127,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (641,127,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (642,127,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (643,127,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (644,127,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (645,127,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (646,127,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (647,127,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (648,127,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (649,128,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (650,128,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (651,128,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (652,128,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (653,128,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (654,128,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (655,128,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (656,128,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (657,128,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (658,129,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (659,129,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (660,129,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (661,129,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (662,130,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (663,130,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (664,130,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (665,130,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (666,131,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (667,131,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (668,132,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (669,132,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (670,133,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (671,133,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (672,133,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (673,133,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (674,133,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (675,134,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (676,134,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (677,134,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (678,134,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (679,134,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (680,135,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (681,135,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (682,135,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (683,135,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (684,135,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (685,136,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (686,136,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (687,136,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (688,136,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (689,136,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (690,136,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (691,136,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (692,136,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (693,136,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (694,137,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (695,137,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (696,137,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (697,137,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (698,137,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (699,137,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (700,137,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (701,137,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (702,137,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (703,138,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (704,138,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (705,138,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (706,138,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (707,138,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (708,138,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (709,138,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (710,139,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (711,139,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (712,139,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (713,139,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (714,139,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (715,139,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (716,139,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (717,140,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (718,141,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (719,142,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (720,143,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (721,143,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (722,143,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (723,143,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (724,143,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (725,143,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (726,143,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (727,143,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (728,143,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (729,144,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (730,144,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (731,144,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (732,144,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (733,144,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (734,144,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (735,144,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (736,144,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (737,144,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (738,145,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (739,145,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (740,145,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (741,146,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (742,146,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (743,146,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (744,147,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (745,147,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (746,147,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (747,147,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (748,147,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (749,147,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (750,148,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (751,148,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (752,148,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (753,148,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (754,148,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (755,148,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (756,149,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (757,149,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (758,149,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (759,149,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (760,149,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (761,150,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (762,150,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (763,150,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (764,150,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (765,150,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (766,151,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (767,151,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (768,151,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (769,151,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (770,151,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (771,151,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (772,151,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (773,151,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (774,152,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (775,152,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (776,152,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (777,152,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (778,152,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (779,152,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (780,152,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (781,152,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (782,153,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (783,153,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (784,154,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (785,154,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (786,155,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (787,156,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (788,157,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (789,158,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (790,158,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (791,158,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (792,158,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (793,159,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (794,159,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (795,159,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (796,159,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (797,160,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (798,160,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (799,160,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (800,161,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (801,161,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (802,161,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (803,162,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (804,162,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (805,162,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (806,163,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (807,163,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (808,163,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (809,163,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (810,163,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (811,163,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (812,164,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (813,164,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (814,164,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (815,164,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (816,164,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (817,164,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (818,165,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (819,165,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (820,165,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (821,165,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (822,165,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (823,165,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (824,165,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (825,165,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (826,165,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (827,166,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (828,166,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (829,166,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (830,166,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (831,166,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (832,166,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (833,166,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (834,166,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (835,166,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (836,167,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (837,167,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (838,167,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (839,167,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (840,167,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (841,167,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (842,167,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (843,167,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (844,168,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (845,168,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (846,168,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (847,168,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (848,168,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (849,168,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (850,168,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (851,168,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (852,169,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (853,169,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (854,170,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (855,170,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (856,171,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (857,171,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (858,172,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (859,172,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (860,172,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (861,172,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (862,172,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (863,173,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (864,173,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (865,173,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (866,173,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (867,173,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (868,174,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (869,174,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (870,174,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (871,174,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (872,174,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (873,175,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (874,175,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (875,175,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (876,175,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (877,176,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (878,176,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (879,176,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (880,176,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (881,177,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (882,177,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (883,177,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (884,177,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (885,178,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (886,178,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (887,178,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (888,178,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (889,179,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (890,179,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (891,179,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (892,179,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (893,179,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (894,179,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (895,179,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (896,180,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (897,180,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (898,180,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (899,180,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (900,180,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (901,180,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (902,180,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (903,181,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (904,181,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (905,181,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (906,181,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (907,181,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (908,182,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (909,182,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (910,182,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (911,182,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (912,182,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (913,183,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (914,183,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (915,183,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (916,183,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (917,183,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (918,183,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (919,183,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (920,183,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (921,183,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (922,184,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (923,184,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (924,184,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (925,184,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (926,184,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (927,184,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (928,184,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (929,184,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (930,184,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (931,185,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (932,185,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (933,185,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (934,185,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (935,185,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (936,185,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (937,185,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (938,185,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (939,185,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (940,186,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (941,186,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (942,186,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (943,187,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (944,187,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (945,187,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (946,188,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (947,189,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (948,190,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (949,190,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (950,190,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (951,190,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (952,190,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (953,191,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (954,191,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (955,191,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (956,191,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (957,191,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (958,192,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (959,192,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (960,192,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (961,192,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (962,192,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (963,193,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (964,193,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (965,193,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (966,193,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (967,193,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (968,193,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (969,193,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (970,193,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (971,194,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (972,194,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (973,194,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (974,194,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (975,194,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (976,194,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (977,195,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (978,195,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (979,195,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (980,195,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (981,195,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (982,195,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (983,196,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (984,197,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (985,198,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (986,199,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (987,199,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (988,199,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (989,199,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (990,199,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (991,199,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (992,199,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (993,199,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (994,200,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (995,200,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (996,200,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (997,200,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (998,200,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (999,200,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1000,200,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1001,200,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1002,201,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1003,201,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1004,202,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1005,202,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1006,203,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1007,203,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1008,204,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1009,204,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1010,204,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1011,204,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1012,204,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1013,204,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1014,205,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1015,205,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1016,205,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1017,205,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1018,205,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1019,205,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1020,206,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1021,206,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1022,206,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1023,206,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1024,207,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1025,207,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1026,207,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1027,207,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1028,208,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1029,208,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1030,208,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1031,208,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1032,209,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1033,209,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1034,209,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1035,209,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1036,209,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1037,209,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1038,209,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1039,210,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1040,210,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1041,210,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1042,210,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1043,210,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1044,210,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1045,210,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1046,211,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1047,211,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1048,212,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1049,212,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1050,213,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1051,213,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1052,213,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1053,213,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1054,213,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1055,213,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1056,213,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1057,213,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1058,213,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1059,214,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1060,214,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1061,214,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1062,214,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1063,214,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1064,214,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1065,214,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1066,214,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1067,214,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1068,215,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1069,215,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1070,215,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1071,215,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1072,215,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1073,215,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1074,215,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1075,215,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1076,215,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1077,216,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1078,216,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1079,216,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1080,217,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1081,217,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1082,217,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1083,218,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1084,218,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1085,218,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1086,219,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1087,219,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1088,220,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1089,220,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1090,221,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1091,221,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1092,221,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1093,221,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1094,221,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1095,222,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1096,222,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1097,222,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1098,222,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1099,222,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1100,223,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1101,223,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1102,223,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1103,223,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1104,223,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1105,224,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1106,224,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1107,224,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1108,224,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1109,224,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1110,224,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1111,224,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1112,224,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1113,225,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1114,225,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1115,225,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1116,225,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1117,225,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1118,225,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1119,225,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1120,225,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1121,226,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1122,226,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1123,226,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1124,226,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1125,226,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1126,226,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1127,226,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1128,226,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1129,227,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1130,227,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1131,227,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1132,227,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1133,227,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1134,227,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1135,227,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1136,228,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1137,228,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1138,228,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1139,228,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1140,228,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1141,228,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1142,228,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1143,229,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1144,230,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1145,231,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1146,232,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1147,232,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1148,232,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1149,232,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1150,233,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1151,233,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1152,233,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1153,233,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1154,234,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1155,234,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1156,234,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1157,234,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1158,235,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1159,235,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1160,235,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1161,236,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1162,236,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1163,236,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1164,237,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1165,237,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1166,237,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1167,237,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1168,237,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1169,237,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1170,238,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1171,238,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1172,238,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1173,238,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1174,238,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1175,238,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1176,239,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1177,239,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1178,239,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1179,239,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1180,239,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1181,239,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1182,240,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1183,240,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1184,240,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1185,240,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1186,240,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1187,241,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1188,241,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1189,241,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1190,241,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1191,241,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1192,242,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1193,242,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1194,242,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1195,242,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1196,242,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1197,242,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1198,242,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1199,242,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1200,243,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1201,243,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1202,243,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1203,243,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1204,243,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1205,243,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1206,243,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1207,243,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1208,244,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1209,244,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1210,245,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1211,246,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1212,247,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1213,247,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1214,247,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1215,247,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1216,248,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1217,248,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1218,248,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1219,248,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1220,249,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1221,249,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1222,249,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1223,249,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1224,249,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1225,249,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1226,249,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1227,250,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1228,250,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1229,250,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1230,250,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1231,250,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1232,250,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1233,250,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1234,251,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1235,251,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1236,251,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1237,251,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1238,251,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1239,251,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1240,251,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1241,252,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1242,252,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1243,252,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1244,252,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1245,252,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1246,252,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1247,253,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1248,253,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1249,253,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1250,253,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1251,253,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1252,253,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1253,254,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1254,254,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1255,254,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1256,254,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1257,254,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1258,254,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1259,254,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1260,254,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1261,254,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1262,255,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1263,255,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1264,255,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1265,255,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1266,255,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1267,255,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1268,255,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1269,255,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1270,255,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1271,256,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1272,256,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1273,256,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1274,256,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1275,256,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1276,256,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1277,256,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1278,257,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1279,257,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1280,257,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1281,257,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1282,257,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1283,257,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1284,257,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1285,258,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1286,258,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1287,259,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1288,259,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1289,260,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1290,260,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1291,260,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1292,260,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1293,260,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1294,261,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1295,261,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1296,261,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1297,261,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1298,261,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1299,262,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1300,262,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1301,262,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1302,263,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1303,263,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1304,263,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1305,264,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1306,264,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1307,264,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1308,265,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1309,265,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1310,265,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1311,266,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1312,266,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1313,266,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1314,266,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1315,266,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1316,266,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1317,266,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1318,267,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1319,267,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1320,267,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1321,267,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1322,267,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1323,267,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1324,267,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1325,268,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1326,269,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1327,270,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1328,271,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1329,271,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1330,271,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1331,271,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1332,271,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1333,271,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1334,271,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1335,271,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1336,272,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1337,272,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1338,272,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1339,272,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1340,272,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1341,272,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1342,272,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1343,272,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1344,273,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1345,273,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1346,273,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1347,273,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1348,273,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1349,273,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1350,273,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1351,273,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1352,274,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1353,274,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1354,274,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1355,275,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1356,275,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1357,275,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1358,276,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1359,277,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1360,278,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1361,279,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1362,279,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1363,279,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1364,279,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1365,280,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1366,280,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1367,280,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1368,280,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1369,281,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1370,281,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1371,281,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1372,281,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1373,281,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1374,281,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1375,281,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1376,281,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1377,282,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1378,282,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1379,282,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1380,282,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1381,282,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1382,282,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1383,282,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1384,282,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1385,283,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1386,283,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1387,283,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1388,283,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1389,283,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1390,283,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1391,283,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1392,283,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1393,284,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1394,284,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1395,284,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1396,284,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1397,284,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1398,284,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1399,285,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1400,285,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1401,285,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1402,285,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1403,285,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1404,285,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1405,286,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1406,286,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1407,286,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1408,286,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1409,286,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1410,286,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1411,286,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1412,286,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1413,286,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1414,287,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1415,287,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1416,287,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1417,287,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1418,287,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1419,287,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1420,287,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1421,287,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1422,287,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1423,288,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1424,288,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1425,288,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1426,288,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1427,289,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1428,289,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1429,289,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1430,289,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1431,290,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1432,290,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1433,291,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1434,291,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1435,292,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1436,292,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1437,292,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1438,292,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1439,292,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1440,293,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1441,293,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1442,293,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1443,293,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1444,293,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1445,294,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1446,294,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1447,294,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1448,294,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1449,295,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1450,295,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1451,295,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1452,295,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1453,296,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1454,296,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1455,296,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1456,296,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1457,296,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1458,296,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1459,296,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1460,297,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1461,297,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1462,297,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1463,297,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1464,297,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1465,297,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1466,297,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1467,298,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1468,298,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1469,298,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1470,298,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1471,298,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1472,298,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1473,298,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1474,299,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1475,300,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1476,300,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1477,300,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1478,300,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1479,300,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1480,300,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1481,300,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1482,300,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1483,300,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1484,301,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1485,301,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1486,301,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1487,301,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1488,301,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1489,301,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1490,301,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1491,301,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1492,301,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1493,302,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1494,302,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1495,302,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1496,303,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1497,303,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1498,303,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1499,304,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1500,304,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1501,304,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1502,305,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1503,305,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1504,305,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1505,305,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1506,305,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1507,305,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1508,306,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1509,306,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1510,306,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1511,306,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1512,306,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1513,306,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1514,307,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1515,307,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1516,307,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1517,307,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1518,307,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1519,307,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1520,308,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1521,308,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1522,308,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1523,308,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1524,308,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1525,309,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1526,309,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1527,309,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1528,309,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1529,309,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1530,310,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1531,310,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1532,310,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1533,310,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1534,310,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1535,310,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1536,310,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1537,310,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1538,311,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1539,311,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1540,311,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1541,311,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1542,311,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1543,311,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1544,311,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1545,311,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1546,312,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1547,312,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1548,312,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1549,312,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1550,312,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1551,312,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1552,312,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1553,313,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1554,313,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1555,313,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1556,313,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1557,313,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1558,313,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1559,313,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1560,314,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1561,315,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1562,316,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1563,317,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1564,317,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1565,317,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1566,317,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1567,318,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1568,318,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1569,318,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1570,318,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1571,319,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1572,319,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1573,319,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1574,320,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1575,320,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1576,320,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1577,321,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1578,321,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1579,321,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1580,321,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1581,321,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1582,321,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1583,322,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1584,322,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1585,322,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1586,322,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1587,322,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1588,322,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1589,323,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1590,323,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1591,323,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1592,323,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1593,323,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1594,323,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1595,323,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1596,323,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1597,323,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1598,324,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1599,324,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1600,324,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1601,324,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1602,324,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1603,324,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1604,324,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1605,324,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1606,324,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1607,325,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1608,325,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1609,325,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1610,325,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1611,325,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1612,325,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1613,325,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1614,325,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1615,326,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1616,326,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1617,326,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1618,326,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1619,326,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1620,326,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1621,326,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1622,326,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1623,327,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1624,327,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1625,327,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1626,327,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1627,327,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1628,327,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1629,327,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1630,327,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1631,328,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1632,328,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1633,329,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1634,329,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1635,329,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1636,329,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1637,329,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1638,329,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1639,329,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1640,329,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1641,329,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1642,330,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1643,330,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1644,330,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1645,330,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1646,330,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1647,330,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1648,330,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1649,330,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1650,330,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1651,331,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1652,331,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1653,331,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1654,331,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1655,331,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1656,331,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1657,331,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1658,331,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1659,331,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1660,332,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1661,332,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1662,332,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1663,332,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1664,333,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1665,333,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1666,333,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1667,333,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1668,334,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1669,335,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1670,335,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1671,335,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1672,335,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1673,336,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1674,336,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1675,336,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1676,336,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1677,337,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1678,337,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1679,337,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1680,338,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1681,338,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1682,338,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1683,339,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1684,339,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1685,339,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1686,340,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1687,340,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1688,340,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1689,340,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1690,340,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1691,340,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1692,341,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1693,341,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1694,341,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1695,341,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1696,341,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1697,341,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1698,342,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1699,342,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1700,342,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1701,342,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1702,342,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1703,342,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1704,342,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1705,342,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1706,342,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1707,343,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1708,343,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1709,343,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1710,343,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1711,343,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1712,343,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1713,343,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1714,343,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1715,343,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1716,344,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1717,344,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1718,344,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1719,344,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1720,344,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1721,344,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1722,344,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1723,344,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1724,345,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1725,345,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1726,346,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1727,346,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1728,346,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1729,346,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1730,346,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1731,347,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1732,347,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1733,347,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1734,347,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1735,348,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1736,348,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1737,348,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1738,348,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1739,348,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1740,348,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1741,348,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1742,349,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1743,349,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1744,349,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1745,349,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1746,349,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1747,349,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1748,349,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1749,350,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1750,350,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1751,350,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1752,350,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1753,350,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1754,350,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1755,350,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1756,351,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1757,351,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1758,351,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1759,351,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1760,351,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1761,351,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1762,352,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1763,352,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1764,352,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1765,352,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1766,352,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1767,352,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1768,353,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1769,353,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1770,353,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1771,353,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1772,353,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1773,353,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1774,353,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1775,353,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1776,353,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1777,354,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1778,354,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1779,354,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1780,354,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1781,354,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1782,354,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1783,354,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1784,354,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1785,354,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1786,355,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1787,355,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1788,355,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1789,356,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1790,356,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1791,356,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1792,357,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1793,357,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1794,357,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1795,358,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1796,358,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1797,359,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1798,359,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1799,360,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1800,360,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1801,360,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1802,360,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1803,360,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1804,361,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1805,361,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1806,361,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1807,361,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1808,361,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1809,362,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1810,362,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1811,362,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1812,362,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1813,362,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1814,363,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1815,363,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1816,363,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1817,363,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1818,363,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1819,363,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1820,363,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1821,363,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1822,364,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1823,364,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1824,364,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1825,364,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1826,364,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1827,364,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1828,364,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1829,364,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1830,365,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1831,365,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1832,365,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1833,365,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1834,365,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1835,365,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1836,365,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1837,366,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1838,366,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1839,366,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1840,366,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1841,366,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1842,366,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1843,366,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1844,367,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1845,368,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1846,369,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1847,370,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1848,370,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1849,370,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1850,370,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1851,370,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1852,370,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1853,370,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1854,370,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1855,371,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1856,371,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1857,371,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1858,371,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1859,371,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1860,371,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1861,371,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1862,371,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1863,372,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1864,372,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1865,372,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1866,372,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1867,372,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1868,372,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1869,372,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1870,372,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1871,373,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1872,373,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1873,373,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1874,374,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1875,374,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1876,374,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1877,375,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1878,375,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1879,375,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1880,375,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1881,375,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1882,375,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1883,376,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1884,376,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1885,376,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1886,376,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1887,376,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1888,376,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1889,377,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1890,377,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1891,377,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1892,377,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1893,378,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1894,378,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1895,378,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1896,378,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1897,379,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1898,379,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1899,379,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1900,379,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1901,379,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1902,379,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1903,379,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1904,379,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1905,380,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1906,380,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1907,380,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1908,380,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1909,380,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1910,380,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1911,380,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1912,380,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1913,381,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1914,381,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1915,382,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1916,382,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1917,383,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1918,383,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1919,383,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1920,383,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1921,383,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1922,383,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1923,383,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1924,383,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1925,383,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1926,384,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1927,384,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1928,384,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1929,384,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1930,384,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1931,384,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1932,384,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1933,384,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1934,384,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1935,385,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1936,385,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1937,385,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1938,385,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1939,386,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1940,386,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1941,386,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1942,386,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1943,387,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1944,387,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1945,387,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1946,387,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1947,388,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1948,388,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1949,389,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1950,389,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1951,390,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1952,390,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1953,390,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1954,390,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1955,390,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1956,391,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1957,391,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1958,391,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1959,391,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1960,391,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1961,392,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1962,392,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1963,392,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1964,392,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1965,392,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1966,393,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1967,393,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1968,393,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1969,393,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1970,393,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1971,393,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1972,393,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1973,393,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1974,393,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1975,394,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1976,394,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1977,394,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1978,394,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1979,394,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1980,394,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1981,394,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1982,394,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1983,394,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1984,395,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1985,395,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1986,395,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1987,395,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1988,395,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1989,395,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1990,395,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1991,396,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1992,396,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1993,396,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1994,396,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1995,396,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1996,396,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1997,396,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1998,397,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (1999,397,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2000,397,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2001,397,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2002,397,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2003,397,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2004,397,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2005,398,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2006,399,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2007,400,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2008,401,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2009,401,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2010,401,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2011,401,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2012,401,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2013,402,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2014,402,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2015,402,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2016,402,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2017,402,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2018,403,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2019,403,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2020,403,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2021,404,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2022,404,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2023,404,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2024,405,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2025,405,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2026,405,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2027,405,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2028,405,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2029,405,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2030,406,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2031,406,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2032,406,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2033,406,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2034,406,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2035,407,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2036,407,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2037,408,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2038,408,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2039,409,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2040,409,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2041,409,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2042,409,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2043,409,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2044,409,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2045,409,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2046,409,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2047,410,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2048,410,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2049,410,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2050,410,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2051,411,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2052,411,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2053,411,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2054,411,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2055,411,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2056,411,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2057,411,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2058,412,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2059,412,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2060,412,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2061,412,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2062,412,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2063,412,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2064,412,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2065,412,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2066,412,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2067,413,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2068,414,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2069,414,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2070,414,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2071,414,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2072,414,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2073,415,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2074,415,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2075,415,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2076,415,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2077,415,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2078,415,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2079,416,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2080,417,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2081,417,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2082,417,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2083,417,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2084,418,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2085,418,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2086,419,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2087,419,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2088,420,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2089,420,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2090,420,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2091,420,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2092,420,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2093,420,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2094,420,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2095,421,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2096,421,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2097,421,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2098,421,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2099,421,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2100,421,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2101,421,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2102,422,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2103,422,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2104,422,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2105,423,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2106,423,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2107,423,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2108,423,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2109,423,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2110,423,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2111,423,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2112,424,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2113,424,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2114,424,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2115,424,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2116,424,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2117,424,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2118,425,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2119,425,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2120,425,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2121,425,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2122,425,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2123,425,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2124,425,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2125,425,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2126,425,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2127,426,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2128,426,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2129,426,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2130,426,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2131,426,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2132,426,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2133,426,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2134,426,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2135,427,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2136,427,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2137,428,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2138,428,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2139,429,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2140,429,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2141,430,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2142,431,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2143,432,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2144,433,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2145,434,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2146,434,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2147,434,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2148,434,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2149,435,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2150,435,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2151,435,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2152,435,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2153,436,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2154,436,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2155,436,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2156,436,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2157,436,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2158,436,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2159,436,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2160,437,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2161,437,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2162,437,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2163,437,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2164,437,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2165,437,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2166,437,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2167,438,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2168,438,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2169,438,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2170,438,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2171,438,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2172,438,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2173,439,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2174,439,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2175,439,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2176,439,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2177,439,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2178,439,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2179,439,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2180,439,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2181,439,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2182,440,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2183,440,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2184,440,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2185,441,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2186,441,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2187,441,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2188,442,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2189,442,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2190,443,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2191,443,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2192,444,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2193,444,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2194,445,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2195,445,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2196,445,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2197,445,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2198,445,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2199,446,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2200,446,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2201,446,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2202,446,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2203,446,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2204,447,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2205,447,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2206,447,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2207,448,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2208,448,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2209,448,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2210,449,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2211,449,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2212,449,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2213,449,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2214,449,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2215,449,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2216,449,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2217,450,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2218,450,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2219,450,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2220,450,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2221,450,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2222,450,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2223,450,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2224,451,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2225,452,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2226,453,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2227,453,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2228,453,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2229,453,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2230,453,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2231,453,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2232,453,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2233,453,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2234,454,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2235,454,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2236,454,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2237,454,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2238,454,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2239,454,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2240,454,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2241,454,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2242,455,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2243,455,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2244,455,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2245,456,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2246,456,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2247,456,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2248,457,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2249,457,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2250,457,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2251,458,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2252,458,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2253,458,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2254,459,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2255,459,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2256,459,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2257,459,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2258,459,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2259,459,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2260,460,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2261,460,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2262,460,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2263,460,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2264,460,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2265,460,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2266,461,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2267,461,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2268,461,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2269,461,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2270,462,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2271,462,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2272,462,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2273,462,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2274,463,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2275,463,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2276,463,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2277,463,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2278,463,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2279,463,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2280,463,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2281,464,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2282,464,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2283,464,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2284,464,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2285,464,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2286,464,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2287,464,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2288,465,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2289,465,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2290,465,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2291,465,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2292,465,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2293,465,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2294,466,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2295,466,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2296,466,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2297,466,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2298,466,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2299,466,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2300,467,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2301,467,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2302,467,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2303,467,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2304,467,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2305,467,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2306,468,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2307,468,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2308,468,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2309,468,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2310,468,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2311,468,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2312,468,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2313,468,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2314,468,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2315,469,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2316,469,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2317,469,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2318,470,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2319,470,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2320,470,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2321,471,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2322,471,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2323,471,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2324,471,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2325,471,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2326,472,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2327,472,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2328,472,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2329,472,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2330,472,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2331,472,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2332,472,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2333,473,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2334,474,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2335,474,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2336,474,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2337,474,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2338,474,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2339,474,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2340,474,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2341,474,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2342,474,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2343,475,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2344,475,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2345,475,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2346,476,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2347,476,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2348,476,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2349,477,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2350,477,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2351,477,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2352,477,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2353,477,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2354,477,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2355,478,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2356,478,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2357,478,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2358,478,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2359,478,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2360,479,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2361,479,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2362,479,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2363,479,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2364,479,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2365,480,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2366,480,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2367,480,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2368,480,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2369,480,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2370,480,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2371,480,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2372,480,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2373,481,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2374,481,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2375,482,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2376,482,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2377,483,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2378,484,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2379,484,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2380,484,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2381,484,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2382,485,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2383,485,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2384,485,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2385,485,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2386,486,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2387,486,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2388,487,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2389,487,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2390,488,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2391,488,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2392,488,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2393,488,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2394,488,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2395,488,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2396,489,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2397,489,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2398,489,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2399,489,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2400,489,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2401,489,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2402,490,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2403,490,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2404,490,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2405,490,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2406,490,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2407,490,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2408,490,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2409,490,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2410,490,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2411,491,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2412,491,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2413,491,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2414,491,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2415,491,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2416,491,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2417,491,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2418,492,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2419,492,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2420,493,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2421,493,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2422,493,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2423,493,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2424,493,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2425,494,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2426,494,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2427,494,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2428,494,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2429,494,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2430,495,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2431,495,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2432,495,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2433,496,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2434,496,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2435,496,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2436,497,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2437,497,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2438,497,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2439,497,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2440,497,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2441,497,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2442,497,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2443,498,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2444,498,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2445,498,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2446,498,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2447,498,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2448,499,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2449,499,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2450,499,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2451,499,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2452,499,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2453,499,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2454,499,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2455,499,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2456,500,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2457,500,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2458,500,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2459,501,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2460,501,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2461,501,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2462,502,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2463,503,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2464,504,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2465,504,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2466,504,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2467,504,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2468,505,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2469,505,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2470,505,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2471,505,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2472,505,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2473,505,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2474,505,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2475,505,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2476,506,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2477,506,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2478,506,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2479,506,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2480,506,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2481,506,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2482,506,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2483,506,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2484,507,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2485,507,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2486,507,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2487,507,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2488,507,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2489,507,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2490,508,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2491,508,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2492,508,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2493,508,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2494,508,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2495,508,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2496,508,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2497,508,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2498,508,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2499,509,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2500,509,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2501,509,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2502,509,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2503,509,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2504,509,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2505,509,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2506,509,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2507,510,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2508,510,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2509,510,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2510,510,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2511,510,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2512,510,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2513,510,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2514,510,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2515,511,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2516,511,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2517,512,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2518,512,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2519,512,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2520,512,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2521,512,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2522,513,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2523,513,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2524,513,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2525,513,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2526,513,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2527,514,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2528,514,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2529,514,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2530,514,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2531,515,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2532,515,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2533,515,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2534,515,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2535,515,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2536,515,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2537,515,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2538,516,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2539,516,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2540,516,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2541,516,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2542,516,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2543,516,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2544,516,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2545,517,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2546,518,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2547,519,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2548,519,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2549,519,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2550,519,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2551,519,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2552,519,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2553,519,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2554,519,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2555,519,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2556,520,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2557,520,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2558,520,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2559,520,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2560,520,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2561,520,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2562,520,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2563,520,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2564,520,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2565,521,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2566,521,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2567,521,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2568,522,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2569,522,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2570,523,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2571,523,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2572,524,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2573,524,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2574,524,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2575,524,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2576,524,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2577,525,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2578,525,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2579,525,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2580,525,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2581,525,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2582,525,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2583,525,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2584,525,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2585,526,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2586,526,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2587,526,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2588,526,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2589,526,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2590,526,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2591,526,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2592,526,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2593,527,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2594,527,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2595,527,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2596,527,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2597,527,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2598,527,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2599,527,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2600,528,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2601,528,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2602,528,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2603,528,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2604,528,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2605,528,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2606,528,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2607,529,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2608,530,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2609,530,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2610,530,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2611,530,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2612,531,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2613,531,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2614,531,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2615,531,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2616,532,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2617,532,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2618,532,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2619,533,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2620,533,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2621,533,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2622,533,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2623,533,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2624,533,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2625,534,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2626,534,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2627,534,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2628,534,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2629,534,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2630,534,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2631,535,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2632,535,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2633,535,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2634,535,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2635,536,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2636,536,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2637,536,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2638,536,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2639,536,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2640,536,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2641,536,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2642,536,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2643,537,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2644,537,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2645,538,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2646,538,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2647,538,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2648,538,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2649,538,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2650,538,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2651,538,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2652,538,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2653,538,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2654,539,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2655,539,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2656,539,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2657,539,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2658,539,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2659,539,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2660,539,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2661,539,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2662,539,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2663,540,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2664,540,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2665,540,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2666,540,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2667,541,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2668,541,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2669,541,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2670,541,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2671,541,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2672,541,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2673,541,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2674,542,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2675,542,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2676,542,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2677,542,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2678,542,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2679,543,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2680,543,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2681,543,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2682,543,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2683,543,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2684,544,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2685,544,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2686,544,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2687,544,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2688,544,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2689,544,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2690,544,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2691,544,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2692,544,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2693,545,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2694,545,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2695,545,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2696,545,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2697,545,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2698,545,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2699,545,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2700,546,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2701,546,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2702,546,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2703,546,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2704,546,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2705,546,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2706,546,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2707,547,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2708,548,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2709,548,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2710,548,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2711,548,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2712,548,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2713,549,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2714,549,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2715,549,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2716,550,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2717,550,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2718,550,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2719,550,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2720,550,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2721,550,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2722,551,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2723,551,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2724,551,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2725,551,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2726,551,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2727,551,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2728,552,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2729,553,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2730,553,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2731,553,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2732,553,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2733,553,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2734,553,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2735,553,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2736,553,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2737,554,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2738,554,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2739,554,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2740,554,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2741,554,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2742,554,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2743,554,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2744,554,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2745,555,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2746,555,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2747,556,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2748,557,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2749,557,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2750,557,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2751,557,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2752,558,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2753,558,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2754,558,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2755,558,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2756,558,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2757,558,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2758,558,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2759,559,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2760,559,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2761,559,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2762,559,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2763,559,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2764,559,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2765,560,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2766,560,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2767,560,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2768,560,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2769,560,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2770,560,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2771,560,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2772,560,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2773,560,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2774,561,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2775,561,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2776,561,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2777,562,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2778,562,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2779,562,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2780,563,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2781,563,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2782,563,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2783,564,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2784,564,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2785,565,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2786,565,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2787,566,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2788,566,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2789,567,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2790,567,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2791,567,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2792,567,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2793,568,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2794,568,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2795,568,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2796,568,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2797,568,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2798,568,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2799,568,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2800,569,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2801,569,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2802,569,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2803,569,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2804,569,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2805,569,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2806,569,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2807,570,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2808,571,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2809,571,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2810,571,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2811,571,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2812,571,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2813,571,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2814,571,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2815,571,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2816,571,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2817,572,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2818,572,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2819,572,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2820,573,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2821,573,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2822,573,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2823,573,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2824,573,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2825,573,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2826,574,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2827,574,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2828,574,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2829,574,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2830,574,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2831,575,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2832,575,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2833,575,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2834,575,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2835,575,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2836,575,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2837,575,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2838,575,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2839,576,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2840,576,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2841,576,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2842,576,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2843,576,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2844,576,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2845,576,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2846,576,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2847,577,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2848,577,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2849,577,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2850,577,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2851,577,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2852,577,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2853,578,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2854,579,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2855,579,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2856,579,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2857,579,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2858,580,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2859,580,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2860,581,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2861,581,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2862,581,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2863,581,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2864,581,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2865,581,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2866,582,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2867,582,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2868,582,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2869,582,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2870,582,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2871,582,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2872,582,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2873,582,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2874,582,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2875,583,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2876,583,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2877,583,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2878,583,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2879,583,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2880,583,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2881,583,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2882,584,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2883,584,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2884,584,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2885,584,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2886,584,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2887,584,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2888,584,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2889,585,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2890,585,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2891,586,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2892,586,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2893,586,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2894,586,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2895,586,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2896,586,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2897,586,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2898,586,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2899,586,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2900,587,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2901,587,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2902,587,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2903,588,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2904,588,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2905,588,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2906,588,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2907,588,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2908,588,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2909,589,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2910,589,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2911,589,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2912,589,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2913,589,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2914,589,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2915,590,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2916,590,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2917,590,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2918,590,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2919,590,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2920,591,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2921,591,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2922,591,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2923,591,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2924,591,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2925,592,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2926,592,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2927,592,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2928,592,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2929,592,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2930,592,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2931,592,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2932,592,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2933,593,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2934,593,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2935,593,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2936,593,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2937,593,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2938,593,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2939,593,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2940,593,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2941,594,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2942,594,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2943,595,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2944,595,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2945,596,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2946,597,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2947,598,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2948,598,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2949,598,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2950,598,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2951,599,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2952,599,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2953,599,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2954,600,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2955,600,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2956,600,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2957,600,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2958,600,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2959,600,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2960,601,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2961,601,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2962,601,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2963,601,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2964,601,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2965,601,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2966,601,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2967,601,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2968,601,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2969,602,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2970,602,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2971,602,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2972,602,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2973,602,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2974,602,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2975,602,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2976,602,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2977,602,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2978,603,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2979,603,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2980,603,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2981,603,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2982,603,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2983,603,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2984,603,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2985,603,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2986,604,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2987,604,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2988,605,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2989,605,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2990,605,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2991,605,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2992,605,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2993,606,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2994,606,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2995,606,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2996,606,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2997,606,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2998,607,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (2999,607,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3000,607,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3001,607,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3002,608,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3003,608,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3004,608,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3005,608,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3006,608,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3007,608,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3008,608,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3009,609,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3010,609,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3011,609,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3012,609,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3013,609,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3014,609,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3015,609,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3016,610,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3017,610,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3018,610,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3019,610,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3020,610,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3021,611,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3022,611,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3023,611,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3024,611,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3025,611,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3026,611,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3027,611,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3028,611,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3029,611,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3030,612,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3031,612,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3032,612,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3033,612,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3034,612,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3035,612,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3036,612,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3037,612,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3038,612,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3039,613,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3040,613,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3041,613,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3042,614,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3043,614,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3044,614,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3045,615,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3046,616,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3047,617,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3048,617,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3049,617,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3050,617,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3051,617,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3052,618,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3053,618,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3054,618,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3055,618,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3056,618,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3057,619,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3058,619,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3059,619,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3060,619,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3061,619,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3062,619,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3063,619,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3064,619,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3065,620,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3066,620,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3067,620,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3068,620,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3069,620,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3070,620,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3071,621,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3072,621,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3073,621,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3074,621,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3075,621,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3076,621,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3077,622,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3078,623,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3079,623,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3080,623,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3081,623,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3082,623,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3083,623,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3084,623,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3085,623,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3086,624,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3087,624,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3088,624,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3089,624,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3090,624,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3091,624,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3092,624,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3093,624,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3094,625,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3095,625,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3096,626,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3097,626,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3098,627,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3099,627,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3100,627,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3101,627,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3102,627,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3103,627,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3104,628,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3105,628,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3106,628,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3107,628,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3108,629,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3109,629,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3110,629,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3111,629,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3112,630,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3113,630,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3114,630,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3115,630,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3116,630,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3117,630,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3118,630,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3119,631,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3120,631,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3121,632,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3122,632,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3123,632,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3124,632,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3125,632,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3126,632,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3127,632,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3128,632,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3129,632,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3130,633,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3131,633,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3132,633,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3133,633,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3134,633,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3135,633,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3136,633,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3137,633,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3138,633,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3139,634,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3140,634,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3141,634,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3142,634,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3143,634,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3144,634,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3145,634,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3146,634,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3147,634,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3148,635,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3149,635,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3150,635,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3151,636,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3152,636,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3153,637,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3154,637,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3155,637,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3156,637,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3157,637,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3158,638,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3159,638,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3160,638,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3161,638,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3162,638,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3163,639,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3164,639,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3165,639,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3166,639,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3167,639,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3168,640,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3169,640,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3170,640,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3171,640,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3172,640,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3173,640,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3174,640,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3175,640,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3176,641,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3177,641,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3178,641,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3179,641,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3180,641,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3181,641,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3182,641,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3183,641,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3184,642,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3185,642,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3186,642,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3187,642,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3188,642,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3189,642,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3190,642,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3191,643,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3192,644,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3193,645,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3194,645,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3195,645,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3196,645,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3197,646,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3198,646,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3199,646,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3200,646,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3201,647,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3202,647,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3203,647,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3204,648,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3205,648,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3206,648,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3207,648,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3208,648,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3209,648,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3210,649,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3211,649,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3212,649,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3213,649,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3214,649,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3215,649,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3216,650,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3217,650,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3218,650,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3219,650,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3220,650,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3221,651,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3222,651,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3223,651,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3224,651,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3225,651,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3226,651,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3227,651,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3228,651,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3229,652,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3230,653,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3231,653,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3232,653,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3233,653,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3234,654,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3235,654,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3236,654,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3237,654,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3238,654,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3239,654,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3240,654,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3241,655,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3242,655,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3243,655,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3244,655,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3245,655,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3246,655,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3247,656,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3248,656,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3249,656,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3250,656,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3251,656,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3252,656,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3253,657,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3254,657,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3255,657,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3256,657,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3257,657,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3258,657,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3259,657,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3260,657,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3261,657,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3262,658,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3263,658,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3264,658,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3265,658,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3266,658,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3267,658,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3268,658,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3269,659,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3270,659,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3271,659,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3272,659,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3273,659,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3274,659,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3275,659,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3276,660,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3277,660,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3278,661,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3279,661,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3280,661,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3281,661,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3282,661,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3283,662,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3284,662,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3285,662,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3286,663,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3287,663,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3288,663,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3289,664,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3290,664,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3291,664,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3292,664,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3293,664,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3294,664,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3295,664,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3296,665,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3297,666,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3298,666,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3299,666,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3300,666,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3301,666,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3302,666,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3303,666,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3304,666,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3305,667,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3306,667,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3307,667,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3308,668,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3309,669,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3310,669,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3311,669,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3312,669,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3313,670,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3314,670,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3315,670,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3316,670,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3317,670,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3318,670,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3319,670,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3320,670,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3321,671,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3322,671,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3323,671,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3324,671,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3325,671,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3326,671,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3327,672,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3328,672,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3329,672,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3330,672,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3331,672,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3332,672,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3333,672,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3334,672,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3335,672,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3336,673,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3337,673,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3338,673,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3339,673,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3340,674,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3341,674,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3342,674,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3343,674,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3344,675,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3345,675,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3346,676,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3347,676,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3348,677,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3349,677,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3350,677,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3351,677,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3352,677,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3353,678,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3354,678,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3355,678,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3356,678,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3357,679,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3358,679,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3359,679,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3360,679,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3361,680,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3362,680,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3363,680,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3364,680,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3365,680,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3366,680,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3367,680,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3368,681,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3369,682,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3370,682,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3371,682,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3372,682,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3373,682,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3374,682,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3375,682,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3376,682,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3377,682,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3378,683,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3379,683,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3380,683,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3381,684,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3382,684,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3383,684,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3384,685,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3385,685,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3386,685,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3387,686,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3388,686,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3389,686,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3390,686,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3391,686,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3392,686,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3393,687,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3394,687,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3395,687,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3396,687,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3397,687,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3398,688,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3399,688,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3400,688,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3401,688,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3402,688,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3403,689,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3404,689,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3405,689,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3406,689,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3407,689,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3408,689,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3409,689,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3410,689,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3411,690,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3412,690,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3413,690,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3414,690,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3415,690,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3416,690,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3417,690,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3418,691,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3419,691,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3420,691,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3421,691,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3422,691,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3423,691,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3424,691,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3425,692,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3426,693,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3427,694,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3428,694,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3429,694,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3430,694,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3431,695,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3432,695,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3433,695,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3434,695,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3435,696,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3436,696,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3437,696,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3438,697,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3439,697,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3440,697,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3441,697,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3442,697,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3443,697,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3444,698,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3445,698,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3446,698,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3447,698,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3448,698,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3449,698,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3450,698,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3451,698,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3452,698,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3453,699,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3454,699,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3455,699,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3456,699,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3457,699,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3458,699,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3459,699,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3460,699,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3461,699,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3462,700,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3463,700,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3464,700,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3465,700,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3466,700,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3467,700,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3468,700,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3469,700,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3470,701,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3471,701,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3472,702,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3473,702,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3474,703,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3475,703,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3476,703,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3477,703,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3478,703,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3479,703,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3480,703,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3481,703,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3482,703,423)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3483,704,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3484,704,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3485,704,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3486,704,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3487,705,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3488,705,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3489,705,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3490,705,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3491,706,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3492,706,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3493,706,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3494,706,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3495,706,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3496,706,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3497,706,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3498,707,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3499,707,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3500,707,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3501,707,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3502,707,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3503,707,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3504,707,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3505,708,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3506,708,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3507,708,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3508,708,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3509,708,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3510,709,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3511,709,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3512,709,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3513,709,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3514,709,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3515,710,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3516,710,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3517,710,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3518,710,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3519,710,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3520,710,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3521,710,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3522,710,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3523,710,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3524,711,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3525,711,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3526,711,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3527,712,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3528,713,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3529,714,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3530,714,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3531,714,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3532,714,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3533,714,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3534,715,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3535,715,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3536,715,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3537,715,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3538,715,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3539,716,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3540,716,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3541,716,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3542,717,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3543,717,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3544,717,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3545,718,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3546,719,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3547,720,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3548,720,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3549,720,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3550,720,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3551,720,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3552,720,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3553,720,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3554,720,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3555,721,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3556,721,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3557,721,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3558,721,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3559,721,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3560,721,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3561,721,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3562,721,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3563,722,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3564,722,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3565,723,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3566,723,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3567,723,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3568,723,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3569,723,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3570,724,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3571,724,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3572,724,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3573,724,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3574,725,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3575,725,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3576,725,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3577,725,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3578,725,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3579,725,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3580,725,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3581,726,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3582,726,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3583,726,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3584,726,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3585,726,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3586,726,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3587,727,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3588,727,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3589,727,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3590,727,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3591,727,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3592,727,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3593,728,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3594,728,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3595,728,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3596,728,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3597,728,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3598,728,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3599,728,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3600,728,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3601,728,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3602,729,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3603,729,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3604,729,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3605,729,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3606,729,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3607,729,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3608,729,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3609,729,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3610,729,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3611,730,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3612,730,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3613,730,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3614,731,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3615,731,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3616,732,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3617,732,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3618,732,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3619,732,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3620,732,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3621,733,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3622,733,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3623,733,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3624,733,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3625,733,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3626,733,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3627,733,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3628,733,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3629,734,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3630,734,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3631,734,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3632,734,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3633,734,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3634,734,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3635,734,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3636,734,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3637,735,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3638,735,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3639,735,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3640,735,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3641,735,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3642,735,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3643,735,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3644,735,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3645,736,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3646,736,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3647,736,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3648,736,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3649,736,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3650,736,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3651,736,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3652,737,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3653,738,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3654,738,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3655,738,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3656,738,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3657,738,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3658,738,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3659,738,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3660,738,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3661,738,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3662,739,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3663,739,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3664,739,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3665,739,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3666,739,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3667,739,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3668,739,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3669,739,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3670,739,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3671,740,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3672,740,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3673,740,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3674,741,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3675,741,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3676,741,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3677,741,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3678,741,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3679,741,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3680,742,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3681,742,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3682,742,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3683,742,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3684,743,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3685,743,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3686,743,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3687,743,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3688,743,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3689,743,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3690,743,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3691,743,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3692,744,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3693,744,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3694,744,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3695,744,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3696,744,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3697,744,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3698,744,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3699,744,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3700,745,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3701,745,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3702,746,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3703,746,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3704,746,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3705,746,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3706,746,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3707,746,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3708,746,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3709,746,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3710,746,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3711,747,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3712,747,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3713,747,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3714,747,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3715,748,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3716,748,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3717,749,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3718,749,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3719,749,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3720,749,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3721,749,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3722,750,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3723,750,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3724,750,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3725,750,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3726,750,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3727,750,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3728,750,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3729,750,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3730,750,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3731,751,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3732,751,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3733,751,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3734,751,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3735,751,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3736,751,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3737,751,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3738,752,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3739,752,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3740,752,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3741,752,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3742,752,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3743,752,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3744,752,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3745,753,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3746,754,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3747,754,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3748,754,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3749,754,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3750,754,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3751,755,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3752,755,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3753,755,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3754,756,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3755,756,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3756,756,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3757,756,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3758,756,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3759,756,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3760,757,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3761,757,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3762,757,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3763,757,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3764,757,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3765,758,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3766,758,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3767,758,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3768,758,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3769,758,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3770,758,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3771,758,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3772,758,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3773,759,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3774,759,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3775,760,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3776,760,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3777,761,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3778,762,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3779,763,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3780,763,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3781,763,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3782,763,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3783,764,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3784,764,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3785,764,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3786,764,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3787,765,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3788,765,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3789,765,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3790,765,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3791,765,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3792,765,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3793,765,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3794,766,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3795,766,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3796,766,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3797,766,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3798,766,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3799,766,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3800,766,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3801,767,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3802,767,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3803,767,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3804,767,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3805,767,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3806,767,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3807,768,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3808,768,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3809,768,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3810,768,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3811,768,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3812,768,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3813,768,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3814,768,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3815,768,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3816,769,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3817,769,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3818,769,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3819,769,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3820,769,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3821,769,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3822,769,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3823,769,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3824,770,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3825,770,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3826,771,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3827,771,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3828,772,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3829,772,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3830,773,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3831,773,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3832,773,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3833,773,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3834,773,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3835,774,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3836,774,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3837,774,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3838,774,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3839,775,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3840,775,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3841,775,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3842,775,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3843,776,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3844,776,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3845,776,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3846,776,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3847,776,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3848,776,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3849,776,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3850,777,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3851,778,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3852,778,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3853,778,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3854,778,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3855,778,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3856,778,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3857,778,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3858,778,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3859,778,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3860,779,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3861,779,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3862,779,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3863,779,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3864,779,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3865,779,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3866,779,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3867,779,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3868,779,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3869,780,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3870,780,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3871,780,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3872,781,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3873,782,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3874,782,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3875,782,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3876,782,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3877,782,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3878,783,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3879,783,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3880,783,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3881,783,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3882,783,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3883,783,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3884,783,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3885,783,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3886,784,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3887,784,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3888,784,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3889,784,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3890,784,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3891,784,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3892,785,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3893,786,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3894,787,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3895,787,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3896,787,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3897,787,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3898,788,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3899,788,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3900,788,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3901,788,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3902,789,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3903,789,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3904,790,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3905,790,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3906,790,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3907,790,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3908,790,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3909,790,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3910,791,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3911,791,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3912,791,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3913,791,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3914,791,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3915,791,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3916,792,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3917,792,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3918,792,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3919,792,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3920,793,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3921,793,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3922,793,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3923,793,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3924,793,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3925,793,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3926,793,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3927,794,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3928,794,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3929,795,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3930,795,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3931,796,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3932,796,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3933,796,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3934,796,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3935,796,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3936,796,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3937,796,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3938,796,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3939,796,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3940,797,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3941,797,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3942,797,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3943,798,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3944,798,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3945,798,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3946,798,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3947,798,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3948,798,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3949,798,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3950,799,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3951,799,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3952,799,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3953,799,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3954,799,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3955,799,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3956,799,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3957,800,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3958,800,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3959,800,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3960,800,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3961,800,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3962,801,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3963,801,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3964,801,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3965,801,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3966,801,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3967,802,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3968,802,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3969,802,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3970,802,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3971,802,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3972,802,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3973,802,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3974,802,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3975,803,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3976,803,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3977,803,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3978,803,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3979,803,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3980,803,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3981,803,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3982,804,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3983,805,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3984,806,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3985,806,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3986,806,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3987,806,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3988,807,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3989,807,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3990,807,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3991,807,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3992,808,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3993,808,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3994,808,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3995,809,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3996,809,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3997,809,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3998,810,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (3999,810,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4000,810,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4001,811,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4002,811,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4003,811,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4004,811,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4005,811,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4006,811,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4007,812,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4008,812,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4009,812,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4010,812,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4011,812,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4012,812,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4013,812,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4014,812,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4015,812,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4016,813,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4017,813,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4018,813,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4019,813,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4020,813,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4021,813,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4022,813,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4023,813,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4024,814,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4025,814,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4026,815,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4027,815,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4028,816,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4029,816,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4030,817,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4031,818,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4032,818,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4033,818,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4034,818,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4035,819,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4036,819,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4037,819,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4038,819,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4039,820,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4040,820,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4041,820,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4042,820,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4043,820,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4044,820,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4045,820,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4046,821,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4047,821,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4048,821,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4049,821,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4050,821,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4051,821,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4052,822,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4053,822,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4054,822,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4055,822,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4056,822,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4057,822,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4058,822,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4059,822,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4060,822,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4061,823,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4062,823,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4063,823,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4064,824,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4065,824,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4066,825,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4067,825,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4068,825,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4069,825,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4070,825,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4071,826,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4072,826,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4073,826,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4074,826,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4075,826,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4076,827,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4077,827,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4078,827,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4079,828,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4080,828,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4081,828,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4082,828,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4083,828,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4084,828,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4085,828,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4086,829,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4087,830,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4088,830,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4089,830,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4090,830,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4091,830,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4092,830,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4093,830,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4094,830,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4095,831,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4096,831,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4097,831,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4098,832,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4099,832,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4100,832,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4101,832,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4102,832,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4103,832,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4104,833,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4105,833,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4106,833,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4107,833,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4108,834,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4109,834,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4110,834,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4111,834,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4112,834,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4113,834,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4114,834,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4115,834,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4116,835,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4117,835,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4118,835,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4119,835,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4120,835,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4121,835,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4122,836,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4123,836,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4124,836,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4125,836,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4126,836,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4127,836,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4128,836,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4129,836,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4130,836,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4131,837,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4132,837,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4133,837,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4134,837,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4135,838,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4136,838,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4137,838,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4138,838,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4139,838,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4140,839,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4141,839,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4142,839,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4143,839,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4144,839,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4145,839,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4146,839,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4147,839,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4148,839,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4149,840,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4150,840,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4151,840,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4152,840,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4153,840,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4154,840,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4155,840,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4156,841,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4157,841,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4158,841,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4159,842,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4160,842,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4161,842,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4162,843,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4163,843,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4164,843,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4165,843,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4166,843,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4167,843,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4168,844,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4169,844,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4170,844,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4171,844,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4172,844,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4173,845,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4174,845,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4175,846,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4176,847,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4177,847,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4178,847,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4179,848,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4180,848,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4181,848,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4182,848,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4183,848,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4184,848,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4185,849,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4186,849,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4187,849,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4188,849,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4189,849,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4190,849,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4191,849,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4192,849,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4193,849,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4194,850,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4195,850,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4196,850,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4197,850,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4198,850,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4199,850,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4200,850,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4201,850,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4202,850,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4203,851,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4204,851,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4205,851,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4206,851,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4207,851,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4208,851,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4209,851,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4210,852,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4211,852,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4212,853,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4213,853,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4214,853,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4215,853,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4216,853,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4217,854,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4218,854,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4219,854,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4220,855,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4221,855,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4222,855,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4223,855,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4224,855,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4225,855,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4226,855,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4227,856,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4228,856,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4229,856,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4230,856,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4231,856,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4232,857,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4233,857,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4234,857,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4235,857,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4236,857,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4237,857,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4238,857,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4239,857,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4240,858,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4241,858,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4242,858,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4243,859,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4244,860,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4245,860,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4246,860,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4247,860,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4248,861,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4249,861,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4250,861,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4251,861,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4252,861,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4253,861,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4254,861,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4255,861,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4256,862,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4257,862,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4258,862,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4259,862,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4260,862,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4261,862,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4262,862,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4263,862,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4264,863,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4265,863,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4266,863,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4267,863,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4268,863,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4269,863,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4270,864,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4271,864,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4272,864,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4273,864,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4274,864,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4275,864,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4276,864,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4277,864,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4278,865,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4279,865,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4280,865,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4281,865,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4282,865,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4283,865,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4284,865,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4285,865,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4286,866,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4287,866,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4288,867,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4289,867,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4290,867,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4291,867,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4292,867,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4293,868,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4294,868,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4295,868,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4296,868,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4297,868,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4298,869,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4299,869,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4300,869,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4301,869,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4302,870,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4303,870,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4304,870,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4305,870,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4306,870,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4307,870,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4308,870,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4309,871,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4310,872,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4311,872,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4312,872,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4313,872,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4314,872,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4315,872,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4316,872,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4317,872,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4318,872,422)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4319,873,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4320,873,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4321,874,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4322,874,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4323,874,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4324,874,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4325,874,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4326,875,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4327,875,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4328,875,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4329,875,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4330,875,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4331,875,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4332,875,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4333,875,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4334,876,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4335,876,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4336,876,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4337,876,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4338,876,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4339,876,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4340,876,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4341,877,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4342,878,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4343,878,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4344,878,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4345,878,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4346,879,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4347,879,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4348,879,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4349,880,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4350,880,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4351,880,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4352,880,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4353,880,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4354,880,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4355,881,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4356,881,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4357,881,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4358,881,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4359,882,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4360,882,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4361,882,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4362,882,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4363,882,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4364,882,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4365,882,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4366,882,902)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4367,883,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4368,883,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4369,883,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4370,883,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4371,883,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4372,883,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4373,883,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4374,883,903)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4375,884,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4376,884,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4377,885,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4378,885,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4379,885,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4380,885,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4381,885,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4382,885,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4383,885,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4384,885,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4385,885,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4386,886,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4387,886,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4388,886,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4389,886,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4390,887,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4391,887,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4392,887,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4393,887,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4394,887,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4395,887,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4396,887,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4397,888,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4398,888,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4399,888,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4400,888,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4401,888,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4402,889,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4403,889,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4404,889,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4405,889,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4406,889,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4407,889,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4408,889,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4409,889,909)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4410,889,429)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4411,890,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4412,890,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4413,890,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4414,890,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4415,890,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4416,890,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4417,890,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4418,891,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4419,892,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4420,892,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4421,892,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4422,892,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4423,892,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4424,893,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4425,893,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4426,893,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4427,893,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4428,893,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4429,893,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4430,894,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4431,895,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4432,895,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4433,895,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4434,895,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4435,895,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4436,895,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4437,895,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4438,895,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4439,896,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4440,896,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4441,897,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4442,898,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4443,898,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4444,898,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4445,898,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4446,899,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4447,899,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4448,899,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4449,899,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4450,899,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4451,899,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4452,899,389)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4453,900,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4454,900,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4455,900,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4456,900,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4457,900,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4458,900,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4459,901,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4460,901,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4461,901,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4462,901,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4463,901,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4464,901,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4465,901,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4466,901,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4467,901,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4468,902,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4469,902,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4470,902,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4471,903,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4472,903,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4473,904,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4474,904,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4475,904,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4476,904,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4477,904,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4478,905,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4479,905,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4480,905,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4481,905,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4482,906,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4483,906,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4484,906,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4485,906,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4486,906,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4487,906,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4488,906,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4489,907,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4490,908,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4491,908,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4492,908,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4493,908,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4494,908,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4495,908,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4496,908,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4497,908,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4498,908,428)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4499,909,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4500,909,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4501,909,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4502,910,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4503,910,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4504,910,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4505,910,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4506,910,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4507,911,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4508,911,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4509,911,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4510,911,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4511,911,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4512,911,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4513,911,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4514,911,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4515,912,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4516,912,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4517,912,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4518,912,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4519,912,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4520,912,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4521,913,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4522,914,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4523,914,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4524,914,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4525,914,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4526,915,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4527,915,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4528,916,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4529,916,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4530,916,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4531,916,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4532,916,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4533,916,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4534,917,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4535,917,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4536,917,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4537,917,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4538,917,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4539,917,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4540,917,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4541,917,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4542,917,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4543,918,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4544,918,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4545,918,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4546,918,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4547,918,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4548,918,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4549,918,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4550,919,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4551,919,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4552,920,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4553,920,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4554,920,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4555,920,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4556,920,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4557,920,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4558,920,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4559,920,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4560,920,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4561,921,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4562,921,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4563,921,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4564,922,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4565,922,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4566,922,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4567,922,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4568,922,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4569,922,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4570,922,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4571,923,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4572,923,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4573,923,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4574,923,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4575,923,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4576,924,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4577,924,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4578,924,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4579,924,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4580,924,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4581,924,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4582,924,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4583,924,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4584,925,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4585,925,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4586,925,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4587,926,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4588,927,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4589,927,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4590,927,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4591,927,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4592,928,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4593,928,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4594,928,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4595,928,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4596,929,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4597,929,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4598,929,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4599,929,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4600,929,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4601,929,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4602,930,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4603,930,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4604,930,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4605,930,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4606,930,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4607,930,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4608,930,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4609,930,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4610,930,421)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4611,931,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4612,931,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4613,932,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4614,932,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4615,933,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4616,933,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4617,933,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4618,933,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4619,934,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4620,934,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4621,934,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4622,934,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4623,934,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4624,934,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4625,935,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4626,935,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4627,935,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4628,936,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4629,936,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4630,936,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4631,937,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4632,937,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4633,938,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4634,938,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4635,938,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4636,938,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4637,938,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4638,938,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4639,938,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4640,938,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4641,939,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4642,939,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4643,939,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4644,939,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4645,939,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4646,939,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4647,940,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4648,941,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4649,941,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4650,942,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4651,942,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4652,942,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4653,942,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4654,942,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4655,942,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4656,943,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4657,943,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4658,943,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4659,943,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4660,944,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4661,944,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4662,944,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4663,944,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4664,944,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4665,944,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4666,944,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4667,945,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4668,945,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4669,946,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4670,946,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4671,946,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4672,946,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4673,946,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4674,946,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4675,946,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4676,946,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4677,946,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4678,947,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4679,947,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4680,947,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4681,947,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4682,947,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4683,947,867)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4684,947,387)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4685,947,907)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4686,947,427)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4687,948,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4688,948,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4689,949,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4690,949,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4691,949,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4692,949,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4693,949,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4694,950,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4695,950,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4696,950,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4697,950,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4698,950,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4699,950,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4700,950,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4701,950,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4702,951,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4703,951,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4704,951,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4705,951,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4706,951,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4707,951,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4708,951,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4709,952,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4710,953,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4711,953,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4712,953,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4713,953,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4714,954,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4715,954,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4716,954,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4717,954,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4718,955,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4719,955,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4720,955,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4721,956,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4722,956,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4723,956,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4724,956,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4725,956,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4726,956,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4727,957,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4728,957,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4729,957,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4730,957,817)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4731,957,337)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4732,958,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4733,958,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4734,958,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4735,958,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4736,958,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4737,958,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4738,958,388)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4739,958,908)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4740,959,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4741,959,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4742,960,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4743,961,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4744,961,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4745,961,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4746,961,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4747,962,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4748,962,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4749,962,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4750,962,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4751,962,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4752,962,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4753,962,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4754,963,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4755,963,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4756,963,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4757,963,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4758,963,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4759,963,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4760,963,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4761,964,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4762,964,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4763,964,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4764,964,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4765,964,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4766,964,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4767,965,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4768,965,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4769,965,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4770,965,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4771,965,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4772,965,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4773,965,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4774,965,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4775,965,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4776,966,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4777,966,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4778,966,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4779,966,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4780,966,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4781,966,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4782,966,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4783,967,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4784,967,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4785,968,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4786,968,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4787,968,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4788,968,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4789,968,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4790,969,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4791,969,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4792,969,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4793,970,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4794,970,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4795,970,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4796,970,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4797,970,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4798,970,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4799,970,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4800,971,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4801,971,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4802,971,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4803,971,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4804,971,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4805,971,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4806,971,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4807,972,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4808,973,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4809,974,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4810,974,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4811,974,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4812,974,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4813,974,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4814,974,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4815,974,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4816,974,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4817,975,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4818,975,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4819,975,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4820,976,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4821,976,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4822,976,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4823,977,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4824,978,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4825,978,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4826,978,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4827,978,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4828,979,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4829,979,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4830,979,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4831,979,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4832,980,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4833,980,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4834,980,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4835,980,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4836,980,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4837,980,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4838,980,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4839,980,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4840,981,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4841,981,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4842,981,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4843,981,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4844,981,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4845,981,861)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4846,981,381)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4847,981,901)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4848,982,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4849,982,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4850,982,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4851,982,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4852,982,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4853,982,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4854,983,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4855,983,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4856,983,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4857,983,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4858,983,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4859,983,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4860,984,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4861,984,774)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4862,984,294)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4863,984,814)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4864,984,334)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4865,984,864)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4866,984,384)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4867,984,904)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4868,984,424)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4869,985,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4870,985,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4871,985,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4872,985,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4873,986,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4874,986,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4875,986,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4876,986,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4877,987,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4878,987,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4879,988,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4880,988,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4881,989,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4882,989,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4883,989,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4884,989,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4885,989,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4886,990,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4887,990,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4888,990,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4889,990,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4890,990,331)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4891,991,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4892,991,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4893,991,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4894,991,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4895,992,242)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4896,992,772)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4897,992,292)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4898,992,812)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4899,992,332)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4900,992,862)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4901,992,382)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4902,993,243)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4903,993,773)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4904,993,293)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4905,993,813)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4906,993,333)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4907,993,863)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4908,993,383)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4909,994,244)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4910,995,245)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4911,995,775)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4912,995,295)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4913,995,815)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4914,995,335)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4915,995,865)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4916,995,385)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4917,995,905)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4918,995,425)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4919,996,246)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4920,996,776)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4921,996,296)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4922,996,816)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4923,996,336)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4924,996,866)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4925,996,386)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4926,996,906)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4927,996,426)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4928,997,247)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4929,997,777)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4930,997,297)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4931,998,248)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4932,998,778)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4933,998,298)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4934,998,818)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4935,998,338)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4936,998,868)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4937,999,249)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4938,999,779)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4939,999,299)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4940,999,819)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4941,999,339)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4942,999,869)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4943,1000,241)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4944,1000,771)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4945,1000,291)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4946,1000,811)
insert into StudentCourse (StudentCourseId, StudentId, CourseId) values (4947,1000,331)
set identity_insert StudentCourse off
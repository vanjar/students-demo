﻿using Autofac;

namespace StudentsApi.Infrastructure
{
    public class InfrastrutcureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ReferenceDataCache>().SingleInstance();
        }
    }
}

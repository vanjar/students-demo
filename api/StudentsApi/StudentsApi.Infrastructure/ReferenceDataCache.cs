﻿using StudentsApi.DataAccess.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StudentsApi.DataAccess;

namespace StudentsApi.Infrastructure
{
    
    public class ReferenceDataCache
    {
        private readonly DatabaseService _databaseService;
        private Dictionary<int, CourseDto> _courseByIdCache;
        private Dictionary<int, List<CourseDto>> _courseByFacultyIdCache;
        private Dictionary<int, DepartmentDto> _departmentByIdCache;
        private Dictionary<int, List<DepartmentDto>> _departmentByFacultyIdCache;
        private Dictionary<int, FacultyDto> _facultyByIdCache;

        public ReferenceDataCache(DatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public async Task InitializeCacheAsync(CancellationToken ct)
        {
            var courses = await _databaseService.GetCoursesAsync(ct).ConfigureAwait(false);
            _courseByIdCache = courses.ToDictionary(x => x.CourseId);
            _courseByFacultyIdCache = courses.GroupBy(c => c.FacultyId).ToDictionary(g => g.Key, g => g.ToList());

            var departments = await _databaseService.GetDepartmentsAsync(ct).ConfigureAwait(false);
            _departmentByIdCache = departments.ToDictionary(d => d.DepartmentId);
            _departmentByFacultyIdCache = departments.GroupBy(d => d.FacultyId)
                .ToDictionary(g => g.Key, g => g.ToList());

            var faculties = await _databaseService.GetFacultiesAsync(ct).ConfigureAwait(false);
            _facultyByIdCache = faculties.ToDictionary(f => f.FacultyId);
        }


        public CourseDto GetCourse(int courseId)
        {
            return _courseByIdCache.ContainsKey(courseId) ? _courseByIdCache[courseId] : null;
        }

        
        public List<CourseDto> GetCoursesByFacultyId(int facultyId)
        {
            return _courseByFacultyIdCache.ContainsKey(facultyId) ? _courseByFacultyIdCache[facultyId] :null;
        }

        public DepartmentDto GetDepartment(int departmentdId)
        {
            return _departmentByIdCache.ContainsKey(departmentdId) ? _departmentByIdCache[departmentdId] : null;
        }

        public List<DepartmentDto> GetDepartmentsByFacultyId(int facultyId)
        {
            return _departmentByIdCache.ContainsKey(facultyId) ? _departmentByFacultyIdCache[facultyId] : null;
        }

        public FacultyDto GetFaculty(int facultyId)
        {
            return _facultyByIdCache.ContainsKey(facultyId) ? _facultyByIdCache[facultyId] : null;
        }

        public List<FacultyDto> GetFaculties()
        {
            return _facultyByIdCache.Values.ToList();
        }
    }
}

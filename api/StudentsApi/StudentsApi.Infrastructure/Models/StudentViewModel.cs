﻿using System.Collections.Generic;

namespace StudentsApi.Infrastructure.Models
{
    public class StudentViewModel
    {
        public int StudentId { get; }

        public int FacultyId { get; }
        public string FacultyName { get;  }

        public string DepartmentName { get; }

        public int DepartmentId { get; }

        public string Name { get; }

        public string Surname { get; }

        public string StudentNumber { get; }

        public string Address { get; }

        public string City { get; }

        public string PhoneNumber { get; }

        public List<CourseViewModel> Courses { get; }
        

        public StudentViewModel(int studentId, int facultyId, string facultyName, string departmentName, 
            int departmentId, string name, string surname, string studentNumber, string address, string city,
            string phoneNumber, List<CourseViewModel> courses)
        {
            StudentId = studentId;
            FacultyId = facultyId;
            FacultyName = facultyName;
            DepartmentName = departmentName;
            DepartmentId = departmentId;
            Name = name;
            Surname = surname;
            StudentNumber = studentNumber;
            Address = address;
            City = city;
            PhoneNumber = phoneNumber;
            Courses = courses;
        }

    }
}
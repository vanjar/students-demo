﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsApi.Infrastructure.Models
{
    public class FacultyViewModel
    {
        public int FacultyId { get; }
        public string Name { get;  }

        public FacultyViewModel(int facultyId, string name)
        {
            FacultyId = facultyId;
            Name = name;
        }
    }
}

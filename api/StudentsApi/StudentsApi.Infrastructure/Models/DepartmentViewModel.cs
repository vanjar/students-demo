﻿namespace StudentsApi.Infrastructure.Models
{
    public class DepartmentViewModel
    {
        public int DepartmentId { get;  }
        public string Name { get; }

        public DepartmentViewModel(int departmentId, string name)
        {
            DepartmentId = departmentId;
            Name = name;
        }
    }
}
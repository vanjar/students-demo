﻿namespace StudentsApi.Infrastructure.Models
{
    public class CourseViewModel
    {
        public int CourseId { get;  }
        public string Name { get; }
        public CourseViewModel(int courseId, string name)
        {
            CourseId = courseId;
            Name = name;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsApi.Infrastructure.Models
{
    public class StudentSearchStudentViewModel
    {
        public int StudentId { get; }
        public string Name { get; }
        public string Surname { get; }
        public string StudentNo { get; }
        public string Faculty { get; }
        public string Department { get; }

        public StudentSearchStudentViewModel(int studentId, string name, string surname, string studentNo, string faculty, string department)
        {
            StudentId = studentId;
            Name = name;
            Surname = surname;
            StudentNo = studentNo;
            Faculty = faculty;
            Department = department;
        }
    }

    public class StudentSearchViewModel
    {
        public int Total { get; }
        public int PageSize { get; }
        public int Page { get; }
        public int Pages { get; }
        public List<StudentSearchStudentViewModel> Students { get; }

        public StudentSearchViewModel(int total, int pageSize, int page, int pages, List<StudentSearchStudentViewModel> students)
        {
            Total = total;
            PageSize = pageSize;
            Page = page;
            Pages = pages;
            Students = students;
        }
    }
}
